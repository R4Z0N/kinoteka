<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'year',
        'rating',
        'trailer',
        'stream',
        'description',
        'genre',
        'actor',
        'director',
        'published'
    ];

    /**
     * Get the comments for the blog post.
     */
    public function episodes()
    {
        return $this->hasMany('App\Episode');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopeUser($query)
    {
        return $query->where('user_id', auth()->id());
    }

    public function scopeMovies($query)
    {
        return $query->where('type', 'movie');
    }

    public function scopeSeries($query)
    {
    	return $query->where('type', 'series');
    }

    public function scopePublished($query)
    {
    	return $query->where('published', true);
    }

    public function scopeNotPublished($query)
    {
        return $query->where('published', false);
    }

    public function scopeHaveStream($query)
    {
        return $query->where('stream', '!=', '')
                     ->WhereNotNull('stream');
    }

    public function scopeHaveNotStream($query)
    {
        return $query->where('stream', '=', '')
                     ->orWhereNull('stream');
    }

    public function scopeHaveTrailer($query)
    {
        return $query->where('trailer', '!=', '')
                     ->whereNotNull('trailer');
    }
    
    public function scopeHaveNotTrailer($query)
    {
        return $query->where('trailer', '=', '')
                     ->orWhereNotNull('trailer');
    }
}
