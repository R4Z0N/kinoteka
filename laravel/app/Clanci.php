<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clanci extends Model
{
	protected $table = 'clanci';

	protected $fillable = [
		'naslov',
		'slug',
		'opis',
		'embed',
		'imdb',
		'slika',
		'objavljen',
		'user_id',
		'kategorije_id'
	];

	public function kategorije(){
		return $this->belongsTo('App\Kategorije');
	}

	public function omiljeno() {
		return $this->hasMany('App\Omiljeno');
	}

	public function oznake() {
		return $this->belongsToMany('App\Oznake')->withTimestamps();
	}

	public function subtitles() {
		return $this->belongsToMany('App\Subtitles')->withTimestamps();
	}

	public function user() {
		return $this->belongsTo('App\User')->where('id', '=', $this->user_id)->first();
	}


	public function random() {
		return \App\Clanci::orderBy('objavljen', '=', true)->latest('pregleda', '=', 'desc')->get()->take(12)->random(6);
	}

	public function stream()
	{
		return $this->belongsTo('App\Clanci', 'data', 'embed');
	}

}
