<?php

namespace App\Console\Commands;

use App\Clanci;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class Gledalica extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gledalica:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sinhronizacija filmova sa Gledalica.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        function omdbAPI($name, $year)
        {
            $client =  new \GuzzleHttp\Client(['http_errors' => false, 'verify' => false]);
            $url = 'https://www.omdbapi.com/?apikey=e93b1ffa&y='.$year.'&t='.urlencode(mb_convert_encoding($name, "UTF-8", "HTML-ENTITIES"));
            $url = $client->request('GET', $url, ['http_errors' => false]);
            if ($url->getStatusCode() == 200) 
            {
                if ($json = json_decode($url->getBody(), true)) {
                    if ($json["Response"] == 'False') return false;
//                else var_dump($json);
                    return $json;
                } else return false;
            }
            else return false;
        }
        $GuzzleHttp =  new \GuzzleHttp\Client(['http_errors' => false, 'verify' => false]);
        $page = 'browse-HD-tablet-mobilni-videos-1-date.html';
        $pageNumber = 0;
        while ($page) {
            $pageNumber++;
            $url = 'https://www.gledalica.com/'.$page;
            $url = $GuzzleHttp->request('GET', $url);
            $string = $url->getBody();
            $pattern = '/<div class=\"video_i\">\n<a href=\"(?P<url>.*?)\">/ms';
            if (preg_match_all($pattern, $string, $matches))
            {
                echo "\n==============================================";
                echo "\n<<<<<<<<<<<<<<<< Strana: $pageNumber >>>>>>>>>>>>>>>>>>>";
                //print_r($matches[1]);
                foreach ($matches[1] as $url) 
                {
                    $url = $GuzzleHttp->request('GET', $url);
    //                echo $url->getBody();

                    $subject = $url->getBody();
                    $patternInfo = '/<title>(?P<name>.*?) \((?P<year>.*?)\) online sa prevodom<\/title>/';
                    if(preg_match($patternInfo, $subject,$info))
                    {
                        echo "\nFilm: ".$info['name']." (".$info['year'].")";
    //                    print_r($info);
                        $patternIframe = '/<iframe src=\"(?P<openload>.*?)\" scrolling=\"no\" frameborder=\"0\"/';
                        if(preg_match($patternIframe, $subject,$embed))
                        {
                            echo "\n".$embed['openload'];
                            if(DB::table('stream_indexes')->where('source', '=' , $embed['openload'])->count() == 0) 
                                {
                                    $omdbApi = omdbAPI($info['name'], $info['year']);
                                    if ($omdbApi) // Provjerava informacije sa omdb-a
                                    {
                                        $uploadOpenload = "https://api.openload.co/1/remotedl/add?login=79ba3aa09509e975&key=3QQ4AFz_&url=".$embed['openload']."&folder=4490282";
                                        $uploadOpenload = $GuzzleHttp->request('GET', $uploadOpenload);
                                        $uploadOpenload = json_decode($uploadOpenload->getBody());

                                        if ($uploadOpenload->msg == 'OK')
                                        {
                                            $statusOpenload = "https://api.openload.co/1/remotedl/status?login=79ba3aa09509e975&key=3QQ4AFz_&id=".$uploadOpenload->result->id;
                                            $statusOpenload = $GuzzleHttp->request('GET', $statusOpenload);
                                            $statusOpenload = json_decode($statusOpenload->getBody());
                                            //print_r($statusOpenload);
                                            $idOpenload = $uploadOpenload->result->id;
                                            $embedOpenload = '<iframe src="https://openload.co/embed/'.$statusOpenload->result->$idOpenload->extid.'/" scrolling="no" frameborder="0" width="700" height="430" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';

                                            echo "\n".$embedOpenload;
                                            DB::table('stream_indexes')->insert([
                                                'name' => $info['name'].' ('.$info['year'].')',
                                                'imdbID' => $omdbApi['imdbID'],
                                                'source' => $embed['openload'],
                                                'data' => $embedOpenload
                                            ]);

                                            $clanak = Clanci::where('imdb', $omdbApi['imdbID'])->limit(1)->pluck('id');
                                            if ($clanak->count()) 
                                            {
                                                echo "\nPOSTOJI U BAZI ID:".$clanak;
                                            //print_r($clanak);
                                                $clanci = Clanci::findOrFail($clanak);
                                                if (empty($clanci->embed)) 
                                                {
                                                    $clanci->embed = $embedOpenload;
                                                    $clanci->embed_q = '720p';
                                                }
                                                elseif (empty($clanci->embed_2))
                                                {
                                                    $clanci->embed_2 = $embedOpenload;
                                                    $clanci->embed_q_2 = '720p';
                                                }
                                                elseif (empty($clanci->embed_3))
                                                {
                                                    $clanci->embed_3 = $embedOpenload;
                                                    $clanci->embed_q_3 = '720p';
                                                }
                                            }
                                            else 
                                            {
                                                echo "\nFilm NE POSTOJI U BAZI";
                                                $clanci = new Clanci;
                                                $clanci->kategorije_id = 1;
                                                $clanci->user_id = 1356;
                                                $clanci->imdb = $omdbApi['imdbID'];
                                                $clanci->naslov = $omdbApi['Title'];
                                                $clanci->slug = Str::slug($omdbApi['Title']);
                                                $clanci->opis = $omdbApi['Plot'];
                                                $clanci->godina = $omdbApi['Year'];
                                                $clanci->embed = $embedOpenload;
                                                $clanci->embed_q = '720p';
                                                $clanci->objavljen = 1;

                                                if($slika = $omdbApi['Poster']) 
                                                {
                                                    $location = '../public/img/filmovi/'.$clanci->slug .'.jpg';
                                                    if ($slika && $slika != 'N/A') copy($slika, $location);
                                                    if ($slika && $slika != 'N/A') $clanci->slika = $clanci->slug .'.jpg';
                                                }
                                            }

                                            $clanci->Director = $omdbApi['Director'];
                                            $clanci->Writer = $omdbApi['Writer'];
                                            $clanci->Actors = $omdbApi['Actors'];
                                            $clanci->imdbRating = $omdbApi['imdbRating'];
                                            $clanci->imdbVotes = $omdbApi['imdbVotes'];
                                            $oznake = array();
                                            if (preg_match('/Action/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 1);
                                            if (preg_match('/Animation/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 2);
                                            if (preg_match('/Adventure/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 3);
                                            if (preg_match('/Biography/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 4);
                                            if (preg_match('/Documentary/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 5);
                                            if (preg_match('/Drama/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 6);
                                            if (preg_match('/Fantasy/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 7);
                                            if (preg_match('/Horror/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 8);
                                            if (preg_match('/History/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 9);
                                            if (preg_match('/Comedy/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 10);
                                            if (preg_match('/Crime/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 11);
                                            if (preg_match('/Mystery/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 12);
                                            if (preg_match('/Musical/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 13);
                                            if (preg_match('/Sci-Fi/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 14);
                                            if (preg_match('/War/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 15);
                                            if (preg_match('/Romance/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 16);
                                            if (preg_match('/Sport/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 17);
                                            if (preg_match('/Thriller/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 18);
                                            if (preg_match('/Western/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 19);
                                            if (preg_match('/Family/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 20);

                                        // Provjera da li clanak postoji
                                            if ($clanak->count()) 
                                            {
                                                $clanci->update();
                                            }

                                        // Ako film ne postoji pokrece se komanda za kreiranje novog clanka
                                            else 
                                            {
                                                $clanci->save();
                                            // Update kategorija
                                                $clanci->oznake()->attach($oznake);
                                            }
                                        }
                                    }  
                                    else
                                    {
                                        echo "\nOMDB GRESKA";
                                    }
                                }
                            }
                            echo "\n==============================================";
                        }
                    }
                } 
                else 
                {
                    echo "greska";
                }
                $patternNext = '#<a href="(?P<url>.{40,80}?)">sledeći &raquo;</a></div>\n#';
                if (preg_match($patternNext, $string, $matches)) 
                {
                //  print_r($matches);
                    $page = $matches['url'];
                } else $page = NULL;
            }
        }
    }
