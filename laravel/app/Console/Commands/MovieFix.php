<?php

namespace App\Console\Commands;

use App\Clanci;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class MovieFix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movie:fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Komanda koja resava probleme sa filmovima.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        function iframeCheck($embed)
        {
            if(preg_match('/(openload)/', $embed))
            {
                $pattern = '/<iframe src=\"https:\/\/openload.co\/embed\/(?P<id>.*?)\//';
                preg_match($pattern, $embed, $openload);
                $json_info = file_get_contents('https://api.openload.co/1/file/info?file='.$openload['id']);
                $json_data_info = json_decode($json_info, true);
                if($json_data_info['result'][$openload['id']]['status'] == 404 or $json_data_info['result'][$openload['id']]['status'] == 500)
                {
                    echo "izvor OPENLOAD // MRTAV"; 
                    return false;
                    //DB::table('stream_indexes')->where('id', $index->id)->delete();
                } else {
                    echo "izvor OPENLOAD // UREDAN"; 
                    return true;
                }
            }
            elseif(preg_match('/(vidoza)/', $embed))
            {
                $pattern = '/SRC="([^"]+)"/';
                preg_match($pattern, $embed, $vidoza);
                if(file_get_contents($vidoza[1]) == 'File was deleted')
                {
                    echo "izvor VIDOZA // MRTAV";
                    return false;
                }
                else
                {
                    echo "izvor VIDOZA // UREDAN"; 
                    return true;
                }
            }
            else
            {
                echo "izvor NEPOZNAT"; 
                return true;
            }
        }


        function omdbAPI($name, $year)
        {
            $client =  new \GuzzleHttp\Client(['http_errors' => false, 'verify' => false]);
            $url = 'https://www.omdbapi.com/?apikey=e93b1ffa&y='.$year.'&t='.urlencode(mb_convert_encoding($name, "UTF-8", "HTML-ENTITIES"));
            $url = $client->request('GET', $url, ['http_errors' => false]);
            if ($url->getStatusCode() == 200) 
            {
                if ($json = json_decode($url->getBody(), true)) {
                    if ($json["Response"] == 'False') return false;
//                else var_dump($json);
                    return $json;
                } else return false;
            }
            else return false;
        }

        $movies = Clanci::where('objavljen', true)->orderBy('id','desc')->get();
        foreach ($movies as $movie) {
            echo "\n$movie->id. $movie->naslov ($movie->godina)";
            if (!empty($movie->embed)) {
                echo "\n1. ";
                if(!iframeCheck($movie->embed))
                {
                    $movie->embed = NULL;
                    $movie->embed_q = NULL;
                }
            } elseif(!empty($movie->embed_2) && iframeCheck($movie->embed_2)) {
                $movie->embed = $movie->embed_2;
                $movie->embed_q = $movie->embed_q_2;
                $movie->embed_2 = NULL;
                $movie->embed_q_2 = NULL;
            } 
            if (!empty($movie->embed_2)) {
                echo "\n2. ";
                if(!iframeCheck($movie->embed_2))
                {
                    $movie->embed_2 = NULL;
                    $movie->embed_q_2 = NULL;
                }
            } elseif(!empty($movie->embed_3) && iframeCheck($movie->embed_3)) {
                $movie->embed_2 = $movie->embed_3;
                $movie->embed_q_2 = $movie->embed_q_3;
                $movie->embed_3 = NULL;
                $movie->embed_q_3 = NULL;
            } 
            if (!empty($movie->embed_3)) {
                echo "\n3. ";
                if(!iframeCheck($movie->embed_3))
                {
                    $movie->embed_3 = NULL;
                    $movie->embed_q_3 = NULL;
                }
            }
            // Update imdb podataka
            if(empty($movie->imdb))
            {
                if($omdbAPI = omdbapi($movie->naslov,$movie->godina))
                {
                    $movie->imdb = $omdbAPI['imdbID'];
                    echo "\nIMDB ID dodjeljen: $movie->imdb";
                }
            }
            // Upis u bazu novih informacija
            if($movie->update()) echo "\n<<<<< Film je uspjesno obnovljen >>>>>>";
            echo "\n=======================================";
//            sleep(1);
        }
    }
}
