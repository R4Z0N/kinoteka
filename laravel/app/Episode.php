<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    
    public function scopeHaveStream($query)
    {
        return $query->where('stream', '!=', '')
                     ->WhereNotNull('stream');
    }

    public function scopeHaveNotStream($query)
    {
        return $query->where('stream', '=', '')
                     ->orWhereNull('stream');
    }
}
