<?php

namespace App\Http\Controllers;

use App\Podesavanja;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

public function index()
{
    $podesavanja = Podesavanja::orderBy('id')->first();
    return view('admin.home', compact('podesavanja'));
}

public function update(Request $request, $id)
{
    $podesavanja = Podesavanja::findOrFail($id);
    $podesavanja->update($request->all());
    return redirect('admin');
}

}
