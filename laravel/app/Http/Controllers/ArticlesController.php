<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Episode;
use App\Podesavanja;
use App\User;
use App\Kategorije;
use App\Oznake;

class ArticlesController extends Controller
{   
    /*
    | -----------------------------------------
    |   serija
    | ----------------------------------------- 
    */
    public function serija(Article $article, $se = '01', $ep = '01')
    {
        $kategorije = Kategorije::orderBy('pozicija')->get();   
        $podesavanja = Podesavanja::orderBy('id')->first();
        $users = User::orderBy('id')->get();
        $oznake = Oznake::orderBy('pozicija')->get();
        
        $se = (int)str_replace('s', '', $se);
        $ep = (int)str_replace('e', '', $ep);

        $article->seasons = Episode::
            where('article_id', $article->id)->select('season', 'episode', 'name')->get();

        $episode = Episode::
            where('article_id', $article->id)->where('season', $se)->where('episode', $ep)->first();

        // return [$article,$episode,$se,$ep];

        $seasons = Episode::
            where('article_id', $article->id)->select('season')->distinct()->get();

        return view('sajt.playlist', compact('users', 'podesavanja', 'kategorije', 'oznake',
            'article',
            'seasons',
            'episode',
            'se',
            'ep'
        ));   
    }
}
