<?php

namespace App\Http\Controllers;

use App\Clanci;
use App\Kategorije;
use App\Oznake;
use App\Subtitles;

use Illuminate\Support\Str;
use App\Http\Requests\ClanciRequest;
use App\Http\Requests\ClanciIzmeniRequest;

use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClanciController extends Controller
{

public function index() {
  $clanci = Clanci::get();
  $clanci->all = count(Clanci::get());
  $clanci->online = count(Clanci::where('objavljen', '=', true)->get());
  $clanci->padding = count(Clanci::where('objavljen', '=', false)->get());
  $clanci->danger = count(Clanci::where('embed', '=', '')->get());
  $kategorije = Kategorije::orderBy('pozicija')->get();
  $oznake = Oznake::orderBy('id')->get();
  return view('admin.clanci.index', compact('clanci', 'oznake', 'kategorije'));
}


public function nosubtitle() {
  // odaberi sve id koji imaju prevod
  $Subtitle = DB::table('clanci_subtitles')->where('subtitles_id', '=', '1')->pluck('clanci_id');
  // odaberi sve clanke koji nemaju odredjen id
  $clanci = Clanci::orderBy('objavljen', 'desc')->orderBy('pregleda', 'acs')->where('objavljen', '=', true)->where('embed','<>','')->whereNotIn('id', $Subtitle)->whereNotIn('kategorije_id', [3,4,6])->latest('user_id')->latest('user_id')->get();
  $clanci->all = count(Clanci::get());
  $clanci->online = count(Clanci::where('objavljen', '=', true)->get());
  $clanci->padding = count(Clanci::where('objavljen', '=', false)->get());
  $clanci->danger = count(Clanci::where('embed', '=', '')->get());
  $kategorije = Kategorije::orderBy('pozicija')->get();
  $oznake = Oznake::orderBy('id')->get();
  return view('admin.clanci.index', compact('clanci', 'oznake', 'kategorije'));
}


public function create() {
  $kategorije = Kategorije::orderBy('pozicija')->get();
  $oznake = Oznake::orderBy('id')->get();
  $subtitles = Subtitles::orderBy('id')->get();
  return view('admin.clanci.create', compact('oznake', 'kategorije', 'subtitles'));
}

public function store(ClanciRequest $request)
{
  $clanci = auth()->user()->clanci()->create($request->all());
  $clanci->naslov = strip_tags($request->naslov);
  $clanci->slug = Str::slug($request->naslov);
  $clanci->godina = strip_tags($request->godina);
  $clanci->tags = strip_tags($request->tags);
  $clanci->Director = strip_tags($request->Director);
  $clanci->Writer = strip_tags($request->Writer);
  $clanci->Actors = strip_tags($request->Actors);
  $clanci->trailer_1 = $request->trailer_1;
  $clanci->trailer_2 = $request->trailer_2;
  $clanci->trailer_3 = $request->trailer_3;
  $clanci->embed_2 = $request->embed_2;
  $clanci->embed_3 = $request->embed_3;
  if($request->has('embed')) { $clanci->embed_q = strip_tags($request->embed_q); }
  if($request->has('embed_2')) { $clanci->embed_q_2 = strip_tags($request->embed_q_2); }
  if($request->has('embed_3')) { $clanci->embed_q_3 = strip_tags($request->embed_q_3); }
  $clanci->imdbRating = strip_tags($request->imdbRating);
  $clanci->imdbVotes = strip_tags($request->imdbVotes);

  if($request->hasFile('slika')) {
    $file = $request->file('slika');
    $clanci->slika = $clanci->slug .'.'. $request->file('slika')->getClientOriginalExtension();
    $file->move('../public/img/filmovi/', $clanci->slika);
  } elseif (empty($clanci->slika)) {
  $slika = $request->Poster;
  $location = '../public/img/filmovi/'.$clanci->slug .'.jpg';
  if ($slika && $slika != 'N/A') copy($slika, $location);
  if ($slika && $slika != 'N/A') $clanci->slika = $clanci->slug .'.jpg';
}
  $clanci->oznake()->attach($request->oznake);
  $clanci->subtitles()->attach($request->subtitles);
  auth()->user()->clanci()->save($clanci);
  return redirect()->back()->with('message', "Uspjesno ste dodali film. <a target='_blank' href='/watch/$clanci->id/$clanci->slug'>$clanci->naslov</a>");
}

public function update(ClanciIzmeniRequest $request, $id)
{
  $clanci = Clanci::findOrFail($id);
  if($request->has('naslov')) {
    $clanci->naslov = strip_tags($request->naslov);
    $clanci->slug = Str::slug($request->naslov);
  }
  $clanci->godina = strip_tags($request->godina);
  $clanci->tags = strip_tags($request->tags);
  $clanci->Director = strip_tags($request->Director);
  $clanci->Writer = strip_tags($request->Writer);
  $clanci->Actors = strip_tags($request->Actors);
  $clanci->trailer_1 = $request->trailer_1;
  $clanci->trailer_2 = $request->trailer_2;
  $clanci->trailer_3 = $request->trailer_3;
  $clanci->embed_2 = $request->embed_2;
  $clanci->embed_3 = $request->embed_3;
  if($request->has('embed')) { $clanci->embed_q = strip_tags($request->embed_q); } else $clanci->embed_q= NULL;
  $clanci->embed_q_2 = strip_tags($request->embed_q_2); 
  $clanci->embed_q_3 = strip_tags($request->embed_q_3); 
  if($clanci->embed_3) { $clanci->embed_q_3 = strip_tags($request->embed_q_3); }
  $clanci->imdbRating = strip_tags($request->imdbRating);
  $clanci->imdbVotes = strip_tags($request->imdbVotes);

  /* Ugasen uslov da opis mora biti uposan :D
  if($request->has('opis')) { $clanci->opis = $request->opis; }
  */
  $clanci->opis = $request->opis;
  if($request->has('imdb')) { $clanci->imdb = $request->imdb; }
  /* Ugasen uslov da embed mora biti upisan :D
  if($request->has('embed')) { $clanci->embed = $request->embed; }
  */
  $clanci->embed = $request->embed;
  if($request->has('objavljen')) { $clanci->objavljen = $request->objavljen; }
  if($request->has('kategorije_id')) { $clanci->kategorije_id = $request->kategorije_id; }

  if($request->hasFile('slika')) {
    $file = $request->file('slika');
    $clanci->slika = $clanci->slug .'.'. $request->file('slika')->getClientOriginalExtension();
    $file->move('../public/img/filmovi/', $clanci->slika);
  } elseif (empty($clanci->slika)) {
  $slika = $request->Poster;
  $location = '../public/img/filmovi/'.$clanci->slug .'.jpg';
  if ($slika && $slika != 'N/A') copy($slika, $location);
  if ($slika && $slika != 'N/A') $clanci->slika = $clanci->slug .'.jpg';
}

  if(!$request->has('oznake')) {
    $clanci->oznake()->detach($request->oznake);
  } else {
    $clanci->oznake()->sync($request->oznake);
  }
  if(!$request->has('subtitles')) {
    $clanci->subtitles()->detach($request->subtitles);
  } else {
    $clanci->subtitles()->sync($request->subtitles);
  }
  $clanci->update();
  return redirect('/watch/'.$clanci->id.'/'.$clanci->slug)->with('message', 'Uspesno ste izmjenili klip.');
}


public function edit($id)
{
   $serial = Clanci::where('id', '=', $id)->firstOrFail();
   $kategorije = Kategorije::orderBy('pozicija')->get();
   $oznake = Oznake::orderBy('id')->get();
   $subtitles = Subtitles::orderBy('id')->get();
  return view('admin.clanci.edit', compact('serial', 'oznake', 'kategorije', 'subtitles'));
}


public function destroy($id)
{
  $clanci = Clanci::findOrFail($id);
  $clanci->delete();
  return back();
}

}