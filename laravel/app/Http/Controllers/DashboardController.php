<?php

namespace App\Http\Controllers;

use App\Podesavanja;
use App\Clanci;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

public function index()
{
    $podesavanja = Podesavanja::orderBy('id')->first();
    $novidoza = Clanci::where('embed', 'NOT LIKE' , '%vidoza%')->latest('wview', '=', 'desc')->paginate(10);
    return view('admin.Dashboard', compact('podesavanja', 'novidoza'));
}

public function update(Request $request, $id)
{
    $podesavanja = Podesavanja::findOrFail($id);
    $podesavanja->update($request->all());
    return redirect('admin');
}

}
