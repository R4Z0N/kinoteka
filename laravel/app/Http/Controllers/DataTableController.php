<?php

namespace App\Http\Controllers;

use App\Clanci;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;



class DataTableController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function nosubtitle()
    {
        return Datatables::of(Clanci::query())->make(true);
    }
}