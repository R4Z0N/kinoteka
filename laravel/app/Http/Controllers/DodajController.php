<?php

namespace App\Http\Controllers;

use App\Clanci;
use App\Kategorije;
use App\Podesavanja;
use App\Oznake;
use App\Subtitles;

use Illuminate\Support\Str;
use App\Http\Requests\DodajRequest;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DodajController extends Controller
{

    public function index() {
        $podesavanja = Podesavanja::orderBy('id')->first();
        $clanci = Clanci::latest('id')->paginate(20);
        $kategorije = Kategorije::orderBy('pozicija')->get();
        $oznake = Oznake::orderBy('pozicija')->get();
        return view('sajt.dodaj.show', compact('clanci', 'kategorije', 'podesavanja', 'oznake'));
    }

    public function store(DodajRequest $request)
    {
        $clanci = new Clanci($request->all());
        $clanci->naslov = strip_tags($request->naslov);
        $clanci->imdb = strip_tags($request->imdb);
        $clanci->slug = Str::slug($request->naslov);
        auth()->user()->clanci()->save($clanci);
        return redirect('dodaj')->with('message', 'Uspješno ste dodali film.');
    }

}