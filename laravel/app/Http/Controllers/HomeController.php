<?php

namespace App\Http\Controllers;

use Validator;

use App\Sitemap;
use App\Podesavanja;
use App\User;
use App\Clanci;
use App\Kategorije;
use App\Oznake;
use App\Subtitles;
use App\Tikets;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

  public function index()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $users = User::orderBy('id')->get();
    $clanci = Clanci::where('objavljen', '=', true)->where('embed', '!=', '')->whereNotIn('kategorije_id', [4, 5])->latest('updated_at')->paginate(36);
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    return view('sajt.home', compact('podesavanja', 'users', 'clanci', 'kategorije', 'oznake'));
  }

  public function donacije()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $users = User::orderBy('id')->get();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    return view('sajt.donacije', compact('podesavanja', 'users', 'kategorije', 'oznake'));
  }

  public function popularno()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $users = User::orderBy('id')->get();
    $clanci = Clanci::orderBy('objavljen', '=', true)->where('embed', '!=', '')->latest('pregleda', '=', 'desc')->paginate(36);
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    return view('sajt.popularno', compact('podesavanja', 'users', 'clanci', 'kategorije', 'oznake'));
  }

  public function best_rated()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $users = User::orderBy('id')->get();
    $clanci = Clanci::orderBy('objavljen', '=', true)->where('embed', '!=', '')->latest('imdbRating', '=', 'desc')->paginate(36);
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    $sort_name = 'Najbolje ocjenjeni filmovi';
    $sort_description = 'Ovdje mozete pogledati najbolje ocjenjene filmove';
    return view('sajt.sort', compact('podesavanja', 'users', 'clanci', 'kategorije', 'oznake', 'sort_name', 'sort_description'));
  }

  public function clanak($slug)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $clanci = Clanci::where('slug', '=', $slug)->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $kategorija = Kategorije::where('slug', '=', $slug)->first();
    $oznake = Oznake::orderBy('pozicija')->get();
    if($clanci && $clanci->objavljen == true) {
      $users = User::where('id', '=', $clanci->user_id)->first();
      $clanci->timestamps = false;
      $clanci->pregleda += 1;
      $clanci->wview += 1;
      $clanci->update();
      return view('sajt.clanci.show', compact('users', 'podesavanja', 'clanci', 'kategorije', 'oznake'));
    }
    elseif($kategorija) {
      $clanci = Clanci::where('kategorije_id', '=', $kategorija->id)->where('objavljen', '=', true)->latest('id')->paginate(36);
      return view('sajt.kategorije.show', compact('podesavanja', 'kategorije', 'kategorija', 'clanci', 'oznake'));
    }
    else { abort(404); }
  }
  
  public function watch($id)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $clanci = Clanci::where('id', '=', $id)->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $kategorija = Kategorije::where('id', '=', $id)->first();
    $oznake = Oznake::orderBy('pozicija')->get();
    if($clanci && ($clanci->objavljen == true or @auth()->user()->admin)) {
      $users = User::where('id', '=', $clanci->user_id)->first();
      $clanci->timestamps = false;
      $clanci->pregleda += 1;
      $clanci->wview += 1;
      $clanci->update();
      return view('sajt.clanci.show', compact('users', 'podesavanja', 'clanci', 'kategorije', 'oznake'));
    } else { abort(404); }
  }

  public function playlist($id)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $clanci = DB::table('film_seri')->where('id', '=', $id)->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $kategorija = Kategorije::where('id', '=', $id)->first();
    $oznake = Oznake::orderBy('pozicija')->get();
    if($clanci && ($clanci->objavljen == true or @auth()->user()->admin)) {
      $users = User::where('id', '=', $clanci->user_id)->first();
      return view('sajt.playlist', compact('users', 'podesavanja', 'clanci', 'kategorije', 'oznake'));
    } else { abort(404); }
  }


  public function oznake($slug)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznaka = Oznake::where('slug', '=', $slug)->first();
    $oznake = Oznake::orderBy('pozicija')->get();
    if($oznaka) {
      $clanci = $oznaka->clanci()->where('objavljen', '=', true)->latest('id')->paginate(36);
      return view('sajt.oznake.show', compact('podesavanja', 'kategorije', 'clanci', 'oznaka', 'oznake'));
    }
    else { abort(404); }
  }

  public function Subtitles($slug){
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $Subtitle = Subtitles::where('naslov', '=', $slug)->first();
    $oznake = Oznake::orderBy('pozicija')->get();
    if($Subtitle) {
      $clanci = $Subtitle->clanci()->where('objavljen', '=', true)->latest('id')->paginate(36);
      return view('sajt.subtitles.show', compact('podesavanja', 'kategorije', 'clanci', 'Subtitle', 'oznake'));
    }
    else { abort(404); }
  } 

  public function pretraga(Request $request)
  {
    $pretraga = $request->pretraga;
    $preporuka = NULL;
    $imdbID = $request->imdbID;
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('id')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    $oznaka = Oznake::where('naslov', 'LIKE' , '%'. $pretraga .'%')->first();
    if($oznaka) {
      $clanci = $oznaka->clanci()->orderBy('clanci_id')->orderBy('objavljen', '=', true)->get();
    } else {
      $clanci =  Clanci::where('naslov', 'LIKE' , '%'.$pretraga.'%')->orwhere('tags', 'LIKE' , '%'. $pretraga .'%')->orwhere('godina', 'LIKE', '%'.$pretraga.'%')->orwhere('imdb', 'LIKE', '%'.$pretraga.'%')->orderBy('objavljen', '=', true);
      if($imdbID) $clanci = $clanci->orwhere('imdb', $imdbID);
      $clanci = $clanci->get();
    }
    if (count($clanci) == 0) {
      $preporuka = Clanci::where('objavljen', '=', true)->where('embed', '!=', '')->where('imdbRating', '>=', '7')->latest('updated_at')->limit(12)->get();
    }
    return view('sajt.pretraga.show', compact('pretraga', 'preporuka', 'podesavanja', 'kategorije', 'clanci', 'oznake'));
  }

  public function godina($godina)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('id')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    $clanci =  Clanci::where('godina', $godina)->orderBy('objavljen', '=', true)->get();
    if($clanci) {
      return view('sajt.godina.show', compact('godina', 'podesavanja', 'kategorije', 'clanci', 'oznake'));
    }
    else { abort(404); }
  }

  public function prijavi($id, Request $request) {

    $validator = Validator::make($request->all(), [
      'problem' => 'required',
      'email' => 'required|min:3|max:300',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->withErrors($validator)->withInput();
    }


    \Mail::send('auth.emails.prijavi',
      array(
        'email' => $request->get('email'),
        'problem' => $request->get('problem'),
        'adresa' => $request->get('adresa')
      ), function($message) use($request)
      {
        $message->from($request->get('email'));
        $message->to('kinoteka.biz@gmail.com', 'Kinoteka.biz')->subject('Kinoteka.biz - Prijavljen problem');
      });
    \Mail::send('auth.emails.prijavi',
      array(
        'email' => $request->get('email'),
        'problem' => $request->get('problem'),
        'adresa' => $request->get('adresa')
      ), function($message) use($request)
      {
        $message->from($request->get('email'));
        $message->to('kinotekafilmovi@gmail.com')->subject('Kinoteka.biz - Prijavljen problem');
      });
    return redirect()->back()->with('message', 'Uspješno ste prijavili problem.');
  }

  public function sitemap(Sitemap $sitemap)
  {
    $map = $sitemap->getsitemap();
    return response($map)->header('Content-type', 'text/xml');
  }

}
