<?php

namespace App\Http\Controllers;

use App\Sitemap;
use App\Podesavanja;
use App\User;
use App\Clanci;
use App\Kategorije;
use App\Oznake;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

  public function index()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $users = User::orderBy('id')->get();
    $clanci = Clanci::where('objavljen', '=', true)->whereNotIn('kategorije_id', [4, 5])->latest('id')->paginate(18);
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    return view('sajt.home', compact('podesavanja', 'users', 'clanci', 'kategorije', 'oznake'));
  }

  public function donacije()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $users = User::orderBy('id')->get();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    return view('sajt.donacije', compact('podesavanja', 'users', 'kategorije', 'oznake'));
  }

  public function popularno()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $users = User::orderBy('id')->get();
    $clanci = Clanci::orderBy('objavljen', '=', true)->latest('pregleda', '=', 'desc')->paginate(18);
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    return view('sajt.popularno', compact('podesavanja', 'users', 'clanci', 'kategorije', 'oznake'));
  }

  public function clanak($slug)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $clanci = Clanci::where('slug', '=', $slug)->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $kategorija = Kategorije::where('slug', '=', $slug)->first();
    $oznake = Oznake::orderBy('pozicija')->get();
    if($clanci) {
      if($clanci->objavljen == true) {
      $users = User::where('id', '=', $clanci->user_id)->first();
      $clanci->timestamps = false;
      $clanci->pregleda += 1;
      $clanci->update();
        return view('sajt.clanci.show', compact('users', 'podesavanja', 'clanci', 'kategorije', 'oznake'));
      }
    }
    if($kategorija) {
      $clanci = Clanci::where('kategorije_id', '=', $kategorija->id)->where('objavljen', '=', true)->latest('id')->paginate(18);
      return view('sajt.kategorije.show', compact('podesavanja', 'kategorije', 'kategorija', 'clanci', 'oznake'));
    }
    else { abort(404); }
  }

  public function oznake($slug)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznaka = Oznake::where('slug', '=', $slug)->first();
    $oznake = Oznake::orderBy('pozicija')->get();
    if($oznaka) {
      $clanci = $oznaka->clanci()->where('objavljen', '=', true)->latest('id')->paginate(18);
      return view('sajt.oznake.show', compact('podesavanja', 'kategorije', 'clanci', 'oznaka', 'oznake'));
    }
    else { abort(404); }
  }

  public function pretraga(Request $request)
  {
    $pretraga = $request->pretraga;
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('id')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    $oznaka = Oznake::where('naslov', 'LIKE' , '%'. $pretraga .'%')->first();
    if($oznaka) {
      $clanci = $oznaka->clanci()->orderBy('clanci_id')->orderBy('objavljen', '=', true)->get();
    } else {
      $clanci =  Clanci::where('naslov', 'LIKE' , '%'. $pretraga .'%')->orderBy('objavljen', '=', true)->get();
    }
    return view('sajt.pretraga.show', compact('pretraga', 'podesavanja', 'kategorije', 'clanci', 'oznake'));
  }

  public function godina($godina)
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('id')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    $clanci =  Clanci::where('naslov', 'LIKE' , '%'. $godina .'%')->orderBy('objavljen', '=', true)->get();
    if($clanci) {
      return view('sajt.godina.show', compact('godina', 'podesavanja', 'kategorije', 'clanci', 'oznake'));
    }
    else { abort(404); }
  }

  public function prijavi(Request $request) {

    \Mail::send('auth.emails.prijavi',
        array(
            'email' => $request->get('email'),
            'problem' => $request->get('problem'),
            'adresa' => $request->get('adresa')
        ), function($message)
        {
          $message->from('noreply@kinoteka.biz');
          $message->to('kinoteka.biz@gmail.com', 'Kinoteka.biz')->subject('Kinoteka.biz - Prijavljen problem');
        });
    return redirect('/')->with('message', 'Uspješno ste prijavili problem.');
  }

  public function sitemap(Sitemap $sitemap)
  {
    $map = $sitemap->getsitemap();
    return response($map)->header('Content-type', 'text/xml');
  }

}
