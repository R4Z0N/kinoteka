<?php

namespace App\Http\Controllers;

use App\Kategorije;
use Illuminate\Support\Str;
use App\Http\Requests\KategorijeRequest;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class KategorijeController extends Controller
{

public function index() {
      $kategorije = Kategorije::orderBy('pozicija')->paginate(10);
      return view('admin.kategorije.index', compact('kategorije'));
}

public function store(KategorijeRequest $request) {
      $kategorije = new Kategorije($request->all());
      $kategorije->naslov = strip_tags($request->naslov);
      $kategorije->slug = Str::slug($request->naslov);
      auth()->user()->kategorije()->save($kategorije);
      return redirect('admin/kategorije');
}

public function update(Request $request, $id) {
      $kategorije = Kategorije::findOrFail($id);
      if($request->has('pozicija')) {
      $kategorije->pozicija = $request->pozicija;
      }
      if($request->has('naslov')) {
      $kategorije->naslov = strip_tags($request->naslov);
      $kategorije->slug = Str::slug($request->naslov);
      }
      if($request->has('opis')) {
      $kategorije->opis = $request->opis;
      }
      if($request->has('boja')) {
      $kategorije->boja = $request->boja;
      }
      $kategorije->update();
      return redirect('admin/kategorije');
}

public function destroy($id) {
      $kategorije = Kategorije::findOrFail($id);
      $kategorije->delete();
      return redirect('admin/kategorije');
}

}
