<?php

namespace App\Http\Controllers;


use App\User;
use App\Clanci;
use App\Kategorije;
use App\Podesavanja;
use App\Oznake;

use Illuminate\Support\Str;
use App\Http\Requests\KorisniciRequest;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class KorisniciController extends Controller
{


    public function index()
    {
        $users = User::orderBy('id')->paginate(20);
        $podesavanja = Podesavanja::orderBy('id')->first();
        return view('admin.korisnici.index', compact('users', 'podesavanja'));
    }

    public function store(KorisniciRequest $request) {
        $users = new User($request->all());
        $users->name = $request->name;
        $users->slug = Str::slug($request->name);
        $users->email = $request->email;
        $users->password = bcrypt($request->password);
        $users->save();
        return redirect('admin/korisnici');
    }

    public function destroy($id)
    {
        $users = User::findOrFail($id);
        $users->delete();
        return redirect()->back();
    }

    public function show($slug)
    {
        $podesavanja = Podesavanja::orderBy('id')->first();
        $users = User::where('slug', '=', $slug)->firstOrFail();
        $clanci = Clanci::where('user_id','=', $users->id)->latest('id')->paginate(16);
        $kategorije = Kategorije::orderBy('pozicija')->get();
        $oznake = Oznake::orderBy('pozicija')->get();
        return view('sajt.korisnici.show', compact('users', 'clanci', 'podesavanja', 'kategorije', 'oznake'));
    }

    public function edit($slug) {
      $podesavanja = Podesavanja::orderBy('id')->first();
      $users = User::where('slug', '=', $slug)->firstOrFail();
      $kategorije = Kategorije::orderBy('id')->get();
      $oznake = Oznake::orderBy('pozicija')->get();
      if(auth()->user()->id == $users->id or auth()->user()->admin) {
          return view('sajt.korisnici.edit', compact('users', 'podesavanja', 'kategorije', 'oznake'));
        } else {
          abort(404);
        }
     }

  public function update(KorisniciRequest $request, $id)
  {
      $users = User::find($id);
      if ($request->has('name')) {
      if(auth()->user()->admin) {
          $users->name = $request->name;
          $users->slug = Str::slug($request->name);
      }else {
          return redirect()->back()->with('izbrisi', 'Korisničko ime nije moguće mijenjati');
      }
      }
      if($request->has('email')) { $users->email = $request->email; }
      if($request->has('password')) { $users->password = bcrypt($request->password); }
      if(auth()->user()->admin) {
          if ($request->has('admin')) {
              $users->admin = $request->admin;
          }
      }
      if($request->hasFile('avatar')) {
          $file = $request->file('avatar');
          $users->avatar = $users->name .'.'. $request->file('avatar')->getClientOriginalExtension();
          $file->move('../public/img/avatar/', $users->avatar);
      }
      $users->update();
      if(auth()->user()->admin) {
          return redirect('admin/korisnici');
      } else {
          return redirect('korisnik/'. $users->slug .'/edit')->with('message', 'Uspješno ste izmjenili nalog');
      }
  }

}
