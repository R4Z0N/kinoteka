<?php

namespace App\Http\Controllers;

use App\Podesavanja;
use App\Clanci;
use App\Kategorije;
use App\Oznake;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OmiljenoController extends Controller
{

  public function index()
  {
    $podesavanja = Podesavanja::orderBy('id')->first();
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('pozicija')->get();
    $clanci = Clanci::orderBy('objavljen', '=', true)->latest('created_at')->get();
    return view('sajt.omiljeno.show', compact('podesavanja', 'users', 'omiljeno', 'clanci', 'kategorije', 'oznake'));
  }

  public function show()
  {
  return redirect()->back();
  }

  public function store(Request $request)
{
    $omiljeno = auth()->user()->omiljeno()->create($request->all());
    if($omiljeno) {
    return redirect()->back()->with('dodaj', 'Uspješno ste dodali film u omiljene.');
    } else { return redirect()->back(); }
}

  public function destroy($id)
{
    $omiljeno = auth()->user()->omiljeno()->where('clanci_id', '=', $id)->firstOrFail();
    if($omiljeno) {
        $omiljeno->delete();
        return redirect()->back()->with('izbrisi', 'Uspješno ste izbrisali film iz omiljenih.');
    } else { return redirect()->back(); }
}

}
