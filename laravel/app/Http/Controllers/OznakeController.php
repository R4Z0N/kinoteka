<?php

namespace App\Http\Controllers;

use App\Oznake;
use Illuminate\Support\Str;
use App\Http\Requests\OznakeRequest;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class OznakeController extends Controller
{

public function index() {
      $oznake = Oznake::orderBy('pozicija')->paginate(20);
      return view('admin.oznake.index', compact('oznake'));
}

public function store(OznakeRequest $request) {
      $oznake = new Oznake($request->all());
      $oznake->naslov = strip_tags($request->naslov);
      $oznake->slug = Str::slug($request->naslov);
      auth()->user()->oznake()->save($oznake);
      return redirect('admin/oznake');
}

public function update(Request $request, $id) {
      $oznake = Oznake::findOrFail($id);
      if($request->has('pozicija')) {
      $oznake->pozicija = $request->pozicija;
      }
      if($request->has('naslov')) {
      $oznake->naslov = strip_tags($request->naslov);
      $oznake->slug = Str::slug($request->naslov);
      }
      if($request->has('opis')) {
      $oznake->opis = $request->opis;
      }
      if($request->has('boja')) {
      $oznake->boja = $request->boja;
      }
      $oznake->update();
      return redirect('admin/oznake');
}

public function destroy($id) {
      $oznake = Oznake::findOrFail($id);
      $oznake->delete();
      return redirect('admin/oznake');
}

}
