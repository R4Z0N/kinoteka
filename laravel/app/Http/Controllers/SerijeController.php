<?php

namespace App\Http\Controllers;

use App\Clanci;
use App\Kategorije;
use App\Oznake;
use App\Subtitles;
use App\Series;

use Illuminate\Support\Str;
use App\Http\Requests\SerijeRequest;
use App\Http\Requests\ClanciIzmeniRequest;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;

class SerijeController extends Controller
{

  public function index() {
    $series = Series::latest('id')->get();
    $series->all = count(Series::get());
    $series->online = count(Series::where('published', '=', true)->get());
    $series->padding = count(Series::where('published', '=', false)->get());
    $series->danger = 0;
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('id')->get();
    return view('admin.serije.index', compact('series', 'oznake', 'kategorije'));
  }


  public function create() {
    $kategorije = Kategorije::orderBy('pozicija')->get();
    $oznake = Oznake::orderBy('id')->get();
    return view('admin.serije.create', compact('oznake', 'kategorije'));
  }

  public function store(SerijeRequest $request)
  {
    $serial = new Series;
    $serial->name = $request->name;
    $serial->imdbID = $request->imdbID;
    $serial->imdbRating = $request->imdbRating;
    $serial->imdbVotes = $request->imdbVotes;
    $serial->year = $request->year;
    $serial->category_id = $request->category_id;
    $serial->tags = $request->tags;
    $serial->plot = $request->plot;
    $serial->slug = Str::slug($request->name);
    $serial->actors = $request->actors;
    $serial->writer = $request->writer;
    $serial->trailer = $request->trailer;
    $serial->director = $request->director;
    $serial->published = $request->published;
    $serial->save();
    return redirect()->back()->with('message', "Uspjesno ste dodali seriju.");
  }

  public function update(ClanciIzmeniRequest $request, $id)
  {
   $serial = Clanci::latest('id')->paginate(20);
   $kategorije = Kategorije::orderBy('pozicija')->get();
   $oznake = Oznake::orderBy('id')->get();
   return view('admin.serije.index', compact('serial', 'oznake', 'kategorije'));
 }


public function edit($id)
{
   $serial = Series::where('id', '=', $id)->firstOrFail();
   dd((new DateTime)->format(DateTime::RFC850));

   $kategorije = Kategorije::orderBy('pozicija')->get();
   $oznake = Oznake::orderBy('id')->get();
   $subtitles = Subtitles::orderBy('id')->get();
  return view('admin.serije.edit', compact('serial', 'oznake', 'kategorije', 'subtitles'));
}


public function destroy($id)
{
  $serial = Series::findOrFail($id);
  $serial->delete();
  return back();
}


}

