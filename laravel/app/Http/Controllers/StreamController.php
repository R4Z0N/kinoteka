<?php

namespace App\Http\Controllers;

use App\Clanci;
use App\Kategorije;
use App\Podesavanja;
use App\Oznake;
use App\Subtitles;
use App\Torrents;
use App\Stream;

use Illuminate\Support\Str;
use App\Http\Requests\DodajRequest;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StreamController extends Controller
{

  public function index() 
  {
    $test = '';
    $streams = Stream::get();
    $ukupno = count($streams);
    $preostalo = $ukupno;
    $objavljenih = 0;
    foreach ($streams as $stream) 
    {
      $clanak = Clanci::where('embed', $stream->data)->orWhere('embed_2', $stream->data)->orWhere('embed_3', $stream->data);
      if(!$clanak->count())
      {
        $test .= '[
          "'.$stream->id.'",
          "'.$stream->name.'",
          "'.$stream->imdbID.'",
          "'.$stream->source.'",
          \''.htmlentities($stream->data).'\',
          "<a href=\"'.url('admin/clanci/create?name='.$stream->name.'&embed='.htmlentities($stream->data)).'\" class=\"btn btn-social-icon\" title=\"Dodaj\"><i class=\"fa fa-share-square-o\"></i></a><form  action=\"admin/stream-index/$stream->id\" method=\"POST\" style=\"float: left;\"><input type=\"hidden\" name=\"_method\" value=\"DELETE\"><input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\"><button onclick=\"return confirm("Da li ste sigurni da želite da izbrišete?\");\" class=\"btn\" title=\"@lang(\"admin.izbrisi\")\" type=\"submit\"><i class=\"fa fa-trash-o\"></i></button></form>"
        ],';
        $preostalo--;
      }
      else
        $objavljenih++;
    }
    return view('admin.stream.index', compact('test','streams', 'ukupno', 'objavljenih', 'preostalo'));
  }

  public function destroy($id)
  {
    DB::table('stream_indexes')->where('id', $id);
    return back();
  }
}