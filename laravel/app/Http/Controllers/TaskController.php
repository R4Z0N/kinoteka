<?php

namespace App\Http\Controllers;

use App\Clanci;
use App\Kategorije;
use App\oznake;
use App\Subtitles;

use Illuminate\Support\Str;
use App\Http\Requests\ClanciRequest;
use App\Http\Requests\ClanciIzmeniRequest;

use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller 
{
  public function popcorn()
  {

    set_time_limit(0);
    function uzmiSadrzaj($stranica)
    {   
      $client = new \GuzzleHttp\Client();
      $sadrzaj = file_get_contents($stranica);
      return $sadrzaj;
    }

    function omdbAPI($name, $year)
    {
      $client = new \GuzzleHttp\Client();
      $url = 'https://www.omdbapi.com/?apikey=e93b1ffa&y='.$year.'&t='.urlencode(mb_convert_encoding($name, "UTF-8", "HTML-ENTITIES"));
      $url = $client->request('GET', $url);

      $json = json_decode($url->getbody(), true); //This will convert it to an array
      if ($json["Response"] == 'False') echo 'nema filma na omdbapi';
//     else var_dump($json);
      return $json;
    }

    function infofigure($url)
    {
      $string = file_get_contents($url);
      $pattern = '/<a href=\"(?P<popcorn_url>.*?)\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"(?P<name>.*?) \((?P<year>.*?)\) sa prevodom\">/';
      if (preg_match_all($pattern, $string, $matches))
      {
        return $matches;
      }
      else
      {
        echo "<br>else grana infofigure<br>";
        return false;
      }
    }

    function infomovie($url)
    {
      $client = new \GuzzleHttp\Client();
      $string = $client->request('GET', $url)->getbody();
      $pattern = '/<iframe src=\"(?P<openload>.*?)\" .*<h1>(?P<name>.*?) \((?P<year>.*?)\) <small>sa prevodom<\/small>/ms';
      if(preg_match($pattern, $string, $matches))
      {
        return $matches;
      }
      else
      {
        echo "<br>else grana infomovie<br>";
        return false;
      }
      $json = json_decode($url->getbody(), true); //This will convert it to an array
    }

    $popcorn = uzmiSadrzaj('http://www.popcorn.rs/filmovi');
    if(!session('popcorn_page') OR session('popcorn_page')>230)session(['popcorn_page' => 1]);
    else session(['popcorn_page' => session('popcorn_page')+1]);
    $page = session('popcorn_page');
    for ($i=$page; $i<=$page; $i++)
    {
      echo "<br> $i. strana<br>";
      $movies = infofigure("http://www.popcorn.rs/filmovi?page=$i");
      for ($p=0; $p<18; $p++)
      {
        if ($movies['name'][$p] && $movies['year'][$p])
        {
          echo "<li>Ime filma: ".$movies['name'][$p]." // Godina: ".$movies['year'][$p]." // URL => ".$movies['popcorn_url'][$p]."</li>";
          $clanci = Clanci::where('naslov', '=', $movies['name'][$p])->where('godina', '=', $movies['year'][$p])->where('embed', '<>', '')->get();
          if(!$clanci->count()) 
          {

            $movie = infomovie("http://www.popcorn.rs".$movies['popcorn_url'][$p]);

            if(DB::table('stream_indexes')->where('source', '=' , $movie['openload'])->count() == 0)
            {
              echo "<b>NEMA OVOG FILMA </b>";


              $url = "https://api.openload.co/1/remotedl/add?login=79ba3aa09509e975&key=3QQ4AFz_&url=".$movie['openload']."&folder=3772420";
              $json = file_get_contents($url);
              $json_data = json_decode($json, true);
              var_dump($json_data);
              echo "<br>";

              if ($json_data['msg'] == 'OK')
              {
                $url_info = "https://api.openload.co/1/remotedl/status?login=79ba3aa09509e975&key=3QQ4AFz_&id=".$json_data['result']['id'];
                $json_info = file_get_contents($url_info);
                $json_data_info = json_decode($json_info, true);
                $embed = '<iframe src="https://openload.co/embed/'.$json_data_info['result'][$json_data['result']['id']]['extid'].'/" scrolling="no" frameborder="0" width="700" height="430" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>';
              }

              $omdbApi = omdbAPI($movie['name'],$movie['year']);
              if ($omdbApi["Response"] == 'True' && $json_data['msg'] == 'OK')
              {
                $movie_id = Clanci::where('naslov', '=', $omdbApi['Title'])->where('godina', '=', $omdbApi['Year'])->pluck('id');
                DB::table('stream_indexes')->insert(
                  ['name' => $movie['name'].' ('.$movie['year'].')', 'imdbID' => $omdbApi['imdbID'], 'source' => $movie['openload'], 'data' => $embed]
                  );
                if(!$movie_id->count())
                {
                  $clanci = new Clanci;
                  $clanci->kategorije_id = 1;
                  $clanci->user_id = 1356;
                  $clanci->naslov = strip_tags($omdbApi['Title']);
                  $clanci->slug = Str::slug($omdbApi['Title']);
                  $clanci->opis = strip_tags($omdbApi['Plot']);
                  $clanci->imdb = strip_tags($omdbApi['imdbID']);
                  $clanci->godina = strip_tags($omdbApi['Year']);
                  $clanci->Director = strip_tags($omdbApi['Director']);
                  $clanci->Writer = strip_tags($omdbApi['Writer']);
                  $clanci->Actors = strip_tags($omdbApi['Actors']);
                  $clanci->embed = $embed;
                  $clanci->embed_q = '720p';
                  $clanci->imdbRating = strip_tags($omdbApi['imdbRating']);
                  $clanci->imdbVotes = strip_tags($omdbApi['imdbVotes']);
                  $clanci->objavljen = 1;
                  if($slika = $omdbApi['Poster']) {
                    $location = '../public/img/filmovi/'.$clanci->slug .'.jpg';
                    if ($slika && $slika != 'N/A') copy($slika, $location);
                    if ($slika && $slika != 'N/A') $clanci->slika = $clanci->slug .'.jpg';
                  }
                  $oznake = array();
                  if (preg_match('/Action/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 1);
                  if (preg_match('/Animation/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 2);
                  if (preg_match('/Adventure/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 3);
                  if (preg_match('/Biography/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 4);
                  if (preg_match('/Documentary/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 5);
                  if (preg_match('/Drama/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 6);
                  if (preg_match('/Fantasy/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 7);
                  if (preg_match('/Horror/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 8);
                  if (preg_match('/History/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 9);
                  if (preg_match('/Comedy/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 10);
                  if (preg_match('/Crime/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 11);
                  if (preg_match('/Mystery/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 12);
                  if (preg_match('/Musical/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 13);
                  if (preg_match('/Sci-Fi/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 14);
                  if (preg_match('/War/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 15);
                  if (preg_match('/Romance/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 16);
                  if (preg_match('/Sport/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 17);
                  if (preg_match('/Thriller/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 18);
                  if (preg_match('/Western/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 19);
                  if (preg_match('/Family/',$omdbApi['Genre'])) $oznake = array_prepend($oznake, 20);
                  $clanci->save();
                  $clanci->oznake()->attach($oznake);
                }
                else 
                {
                  $clanci = Clanci::findOrFail($movie_id);
                  $clanci->embed = $embed;
                  $clanci->embed_q = '720p';
                  $clanci->update();
                }
              }
              elseif($json_data['msg'] == 'OK') 
              {
                DB::table('stream_indexes')->insert(
                  ['name' => $movie['name'].' ('.$movie['year'].')', 'source' => $movie['openload'], 'data' => $embed]
                  );
              }
            } else echo "Ovaj film je indeksiran";
          } else echo "Ovaj film je objavljen";
          echo "<hr>";
        } 

      }

    }

  }



  public function stream()
  {
    function openload($clanak,$embed)
    {
      echo "<br>Ovo je OPENLOAD";
      $pattern = '/<iframe src=\"https:\/\/openload.co\/embed\/(?P<id>.*?)\//';
      preg_match($pattern, $clanak->$embed, $openload);
      echo "<br> ID FILMA JE: ".$openload['id']."<br>";
      $json_info = file_get_contents('https://api.openload.co/1/file/info?file='.$openload['id']);
      $json_data_info = json_decode($json_info, true);
      if($json_data_info['result'][$openload['id']]['status'] == 404)
      {
        echo "OVAJ LINK JE MRTAV"; 
        $movie = Clanci::findOrFail($clanak->id);
        $movie->$embed = NULL;
        $movie->update();
      } else {
        echo $json_data_info['result'][$openload['id']]['status'].' // FILM JE UREDU'; 
      }
    }

    function openload_i($index)
    {
      echo "<br>Ovo je OPENLOAD";
      $pattern = '/<iframe src=\"https:\/\/openload.co\/embed\/(?P<id>.*?)\//';
      preg_match($pattern, $index->data, $openload);
      echo "<br> ID FILMA JE: ".$openload['id']."<br>";
      $json_info = file_get_contents('https://api.openload.co/1/file/info?file='.$openload['id']);
      $json_data_info = json_decode($json_info, true);
      if($json_data_info['result'][$openload['id']]['status'] == 404 or $json_data_info['result'][$openload['id']]['status'] == 500)
      {
        echo "OVAJ LINK JE MRTAV"; 
        DB::table('stream_indexes')->where('id', $index->id)->delete();
      } else {
        echo $json_data_info['result'][$openload['id']]['status'].' // FILM JE UREDU'; 
      }
    }

    function vidoza($clanak,$embed)
    {
      echo "<br>Ovo je VIDOZA";
      $pattern = '/SRC="([^"]+)"/';
      preg_match($pattern, $clanak->$embed, $vidoza);
      if(file_get_contents($vidoza[1]) == 'File was deleted')
      {
        echo "<br> OVAJ LINK JE MRTAV";
        $movie = Clanci::findOrFail($clanak->id);
        $movie->$embed = NULL;
        $movie->update();
      }
      else
      {
        echo "<br> UREDNO";
      }
    }

    if(!session('page_stream') OR session('page_stream')>550) session(['page_stream' => 1]);
    else session(['page_stream' => session('page_stream')+1]);
    $page = session('page_stream');

    echo $page."<br>";

    $clanci = Clanci::skip($page*10)->take(10)->get();

    foreach ($clanci as $clanak) {
      echo $clanak->id.'. '.$clanak->naslov."<br>".htmlentities($clanak->embed);
      if(preg_match('/(openload)/', $clanak->embed))
      {
        openload($clanak,'embed');
      }
      if(preg_match('/(vidoza)/', $clanak->embed))
      {
        vidoza($clanak,'embed');
      }
      echo "<br>".htmlentities($clanak->embed_2);
      if(preg_match('/(openload)/', $clanak->embed_2))
      {
        openload($clanak,'embed_2');
      }
      if(preg_match('/(vidoza)/', $clanak->embed_2))
      {
        vidoza($clanak,'embed_2');
      }

      echo "<br>".htmlentities($clanak->embed_3);
      if(preg_match('/(openload)/', $clanak->embed_3))
      {
        openload($clanak,'embed_3');
      }
      if(preg_match('/(vidoza)/', $clanak->embed_3))
      {
        vidoza($clanak,'embed_3');
      }	  
      echo "<hr>";
    }

    $indexi = DB::table('stream_indexes')->skip($page*10)->take(10)->get();
    echo "<hr>INDEKSIRANI STREAMOVI <hr>";
    foreach ($indexi as $index) {
      echo $index->id.'. '.$index->name."<br>".htmlentities($index->data);
      if(preg_match('/(openload)/', $index->data))
      {
        openload_i($index);
      }
      echo "<hr>";
    }
  }



public function destroy($id)
{
  $clanci = Clanci::findOrFail($id);
  $clanci->delete();
  return back();
}

}