<?php

namespace App\Http\Controllers;

use App\Clanci;
use App\Kategorije;
use App\Podesavanja;
use App\Oznake;
use App\Subtitles;
use App\Torrents;

use Illuminate\Support\Str;
use App\Http\Requests\DodajRequest;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TorrentController extends Controller
{

    public function index() {
        $torrents = Torrents::get();
        $ukupno = count($torrents);
        return view('admin.torrent.index', compact('torrents', 'ukupno'));
    }

}