<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClanciIzmeniRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'naslov' => 'required|min:3|max:70',
            'slika' => 'mimes:jpeg,png,gif|max:512',
            'imdb' => 'required|alpha_num',
//            'embed' => 'required',
        ];
    }
}
