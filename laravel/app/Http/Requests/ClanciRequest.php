<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ClanciRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'naslov' => 'required|min:2|max:100|unique:clanci',
            'slika' => 'mimes:jpeg,png,gif|max:512',
            'imdb' => 'required|alpha_num',
//            'embed' => 'required',
        ];
    }
}
