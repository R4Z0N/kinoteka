<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DodajRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'naslov' => 'required|min:3|max:70|unique:clanci',
            'embed' => 'required',
        ];
    }
}
