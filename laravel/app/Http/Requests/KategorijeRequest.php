<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class KategorijeRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'naslov' => 'required|min:3|max:60',
            'opis' => 'min:5|max:100',
            'pozicija' => 'alpha_num',
        ];
    }
}
