<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class KorisniciRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
          'name' => 'min:3|max:15|alpha_num|unique:users',
          'email' => 'email|min:3|max:50|unique:users',
          'password' => 'min:6',
          'avatar' => 'mimes:jpeg,png,gif|max:512',
        ];
    }
}
