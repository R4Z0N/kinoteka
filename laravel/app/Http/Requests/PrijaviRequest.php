<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PrijaviRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|min:3|max:70|unique:clanci',
            'problem' => 'required',
        ];
    }
}
