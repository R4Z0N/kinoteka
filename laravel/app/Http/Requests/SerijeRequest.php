<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SerijeRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'imdbID' => 'required|unique:serials',
        ];
    }
    public function messages()
    {
        return [
            'imdbID.unique' => 'Ova serija vec postoji u bazi.',
        ];
    }
}
