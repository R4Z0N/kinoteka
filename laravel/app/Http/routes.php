<?php


Route::group(['middleware' => 'admin'], function() {
Route::get('admin', 'DashboardController@index');
Route::resource('admin/podesavanja', 'AdminController', array('only' => array('index', 'update')));
Route::resource('admin/clanci', 'ClanciController', array('except' => array('show')));
Route::get('admin/clanci/nosubtitle', 'ClanciController@nosubtitle');
Route::get('admin/datatable/nosubtitle', 'DataTableController@nosubtitle');


Route::resource('admin/serije', 'SerijeController', array('except' => array('show')));
Route::resource('admin/torrent', 'TorrentController', array('only' => array('index', )));
Route::resource('admin/stream-index', 'StreamController', array('except' => array('create', 'show')));
Route::resource('admin/kategorije', 'KategorijeController', array('except' => array('create', 'show')));
Route::resource('admin/oznake', 'OznakeController', array('except' => array('create', 'show')));
Route::resource('admin/korisnici', 'KorisniciController', array('except' => array('create', 'show')));
});
// Samo logovani
Route::group(['middleware' => 'auth'], function() {
Route::resource('korisnik', 'KorisniciController', array('except' => array('index', 'create', 'store', 'destroy')));
Route::resource('dodaj', 'DodajController', array('only' => array('index', 'store')));
Route::resource('omiljeno', 'OmiljenoController', array('except' => array('create', 'update')));
});

// Prikaz prve stranice
Route::get('/', 'HomeController@index');

// Zadaci
Route::get('task/popcorn', 'TaskController@popcorn');
Route::get('task/stream', 'TaskController@stream');

Route::auth();

Route::get('popularno', 'HomeController@popularno');
Route::get('best_rated', 'HomeController@best_rated');
Route::get('donacije', 'HomeController@donacije');
Route::get('sitemap.xml', 'HomeController@sitemap');

Route::post('pretraga', 'HomeController@pretraga');
Route::post('{id}/prijavi', 'HomeController@prijavi'); // Treba izbrisati

Route::post('/watch/{id}/{slug?}/prijavi', 'HomeController@prijavi');

Route::get('godina/{godina}', 'HomeController@godina');
Route::get('zanr/{slug}', 'HomeController@oznake');
Route::get('subtitle/{slug}', 'HomeController@subtitles');
Route::get('{slug}', 'HomeController@clanak');
Route::get('/watch/{id}/{slug?}', 'HomeController@watch');
Route::get('/list/{id}/{slug?}', 'HomeController@playlist');


Route::group(['prefix' => 'serija/{article}'], function () {
    // Route to access article info (optional parametar is trailer)
    // example: /article-slug/ or /article-slug/trailer
    Route::get('/',                         'ArticlesController@serija');

    // Route only for the series
    // access to article info, season info and episode stream link
    Route::get('s{sezona?}/e{epizoda?}',    'ArticlesController@serija');
});

Route::get('{id}/{slug}', 'HomeController@watch');