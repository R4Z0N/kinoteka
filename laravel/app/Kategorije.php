<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategorije extends Model
{
  protected $table = 'kategorije';

  protected $fillable = [
  	'pozicija',
  	'naslov',
  	'slug',
  	'opis',
  	'boja',
  	'user_id'
	];

	public function clanci() {
		return $this->hasMany('App\Clanci')->where('objavljen', '=', true)->get();
	}
}
