<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Omiljeno extends Model
{
  protected $table = 'omiljeno';

	protected $fillable = [
		'user_id',
		'clanci_id',
 ];

 public function clanci(){
    return $this->belongsTo('App\Clanci');
 }


    public function user() {
        return $this->belongsTo('App\User')->where('id', '=', $this->user_id)->first();
    }

}
