<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oznake extends Model
{
  protected $table = 'oznake';

	protected $fillable = [
        'pozicija',
        'naslov',
        'slug',
        'opis',
        'boja',
        'user_id'
 ];

 public function clanci(){
    return $this->belongsToMany('App\Clanci');
 }


}
