<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Podesavanja extends Model
{
    protected $table = 'podesavanja';

  	protected $fillable = [
  		'naslov',
  		'podnaslov',
  		'keywords',
      'description',
  		'bing',
  		'google',
  		'twitter',
      'facebook',
  		'googleplus',
  		'favicon',
  	 	'slika',
   ];



}
