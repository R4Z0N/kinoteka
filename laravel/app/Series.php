<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    protected $table = 'serials';


	public function kategorije(){
		return $this->belongsTo('App\Kategorije');
	}

	public function omiljeno() {
		return $this->hasMany('App\Omiljeno');
	}

	public function oznake() {
		return $this->belongsToMany('App\Oznake')->withTimestamps();
	}

	public function subtitles() {
		return $this->belongsToMany('App\Subtitles')->withTimestamps();
	}

	public function user() {
		return $this->belongsTo('App\User')->where('id', '=', $this->user_id)->first();
	}
}
