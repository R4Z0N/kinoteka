<?php

namespace App;


class Sitemap
{

  public function getsitemap()
  {
    $sitemap = $this->buildsitemap();
    return $sitemap;
  }

  protected function buildsitemap()
    {
      $url = url('/');
      $xml = [];
      $xml[] = '<?xml version="1.0" encoding="UTF-8"?>';
      $xml[] = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
      $xml[] = '<url>';
      $xml[] = "<loc>$url</loc>";
      $xml[] = '<changefreq>daily</changefreq>';
      $xml[] = '<priority>1.0</priority>';
      $xml[] = '</url>';
      foreach ($this->getcategoryinfo() as $category) {
        $xml[] = '<url>';
        $xml[] = "<loc>$url/$category->slug</loc>";
        $xml[] = "<changefreq>hourly</changefreq>";
        $xml[] = "<priority>0.9</priority>";
        $xml[] = '</url>';
      }
      foreach ($this->getpostsinfo() as $film) {
        $xml[] = '<url>';
        $xml[] = "<loc>$url/watch/$film->id/$film->slug</loc>";
        $xml[] = "<lastmod>".$film->created_at->format('Y-m-d')."</lastmod>";
        $xml[] = "<changefreq>daily</changefreq>";
        $xml[] = "<priority>0.8</priority>";
        $xml[] = '</url>';
      }
      $xml[] = '</urlset>';
      return join("\n", $xml);
    }

  protected function getpostsinfo()
  {
    return Clanci::orderBy('updated_at', 'desc')->orderBy('objavljen', '=', true)->get();
  }
  protected function getcategoryinfo()
  {
    return Kategorije::orderBy('pozicija')->get();
  }

}