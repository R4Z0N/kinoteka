<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Stream extends Model
{
	protected $table = 'stream_indexes';

	protected $fillable = [
		'name',
		'source',
		'imdbID',
		'data'
	];

}