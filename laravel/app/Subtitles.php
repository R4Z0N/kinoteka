<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtitles extends Model
{
  protected $table = 'subtitles';

	protected $fillable = [
        'naslov',
        'flag'
 ];

 public function clanci(){
    return $this->belongsToMany('App\Clanci');
 }


}
