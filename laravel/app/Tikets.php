<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tikets extends Model
{
  protected $table = 'tikets';

	protected $fillable = [
        'name',
        'message'
 ];

 public function clanci(){
    return $this->belongsToMany('App\Clanci');
 }