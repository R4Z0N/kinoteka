<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Torrents extends Model
{
  protected $table = 'torrents';

	protected $fillable = [
        'name',
        'year',
        'source',
        'imdbID',
        'imdbRating',
        'Genre',
        'data'
 ];

 public function clanci(){
    return $this->hasOne('App\Clanci', 'imdb', 'imdbID');
 }
 }