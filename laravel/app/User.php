<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'users';

    protected $fillable = [
        'name', 'slug', 'email', 'password', 'avatar'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getEmailAttribute($value) {
        return strtolower($value);
    }

    public function setEmailAttribute($value) {
        $this->attributes['email'] = strtolower($value);
    }

    public function kategorije() {
    return $this->hasMany('App\Kategorije');
    }
 
    public function clanci() {
    return $this->hasMany('App\Clanci');
    }

    public function omiljeno() {
        return $this->hasMany('App\Omiljeno');
    }

    public function oznake(){
        return $this->hasMany('App\Oznake');
    }

    public function subtitles(){
        return $this->hasMany('App\Subtitles');
    }

    public function tikets(){
        return $this->hasMany('App\Tikets');
    }

    public function dodao_filmova()
    {
        return $this->hasMany('App\Clanci')->where('user_id', $this->id)->count();
    }

    public function omiljeni_filmovi()
    {
        return $this->hasMany('App\Omiljeno')->where('user_id', $this->id)->count();
    }

}
