<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Zoran',
            'email' => 'zoran96rs@gmail.comm',
            'password' => bcrypt('popcorn'),
        ]);
        DB::table('podesavanja')->insert([
            'naslov' => 'Kinoteka.biz',
        ]);
    }
}
