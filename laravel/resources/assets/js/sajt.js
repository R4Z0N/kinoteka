$(function () { $('[data-toggle="tooltip"]').tooltip() });

$('.play').addClass('animated flip');
$('.animacija').addClass('animated fadeInDown');

var ias = jQuery.ias({
    container:  '#content',
    item:       '.col-xs-6',
    pagination: '.pagination',
    next:       '.pagination li.active + li a',
});

ias.extension(new IASSpinnerExtension({
    src: 'http://www.kinoteka.biz/img/loading.gif', // optionally
    html: '<div class="container ias-spinner" style="text-align: left;"><img src="{src}"/></div>',
}));

tinymce.init({
    selector: "textarea",
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify",
    menubar:false,
    statusbar: false,
    height: 200
});

$(document).ready(function() {
    if(window.location.href.indexOf("#otvori") > -1) {
        $("#otvori").fadeToggle();
    }
    $(".otvori").click(function() {
        $("#otvori").fadeToggle();
    });
    $("body").mouseup(function(e) {
        var subject = $("#otvori");
        if(e.target.id != subject.attr('id') && !subject.has(e.target).length) { subject.fadeOut(); }
    });
});

$(function() {
    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    $('.modal').on('show.bs.modal', reposition);
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
});
