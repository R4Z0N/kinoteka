<?php

return [

    // Prva stranica
    'dobrodosli' => 'Dobrodošli na Kinoteka.biz // Registruj se kako bi izbjegao iskakajuce reklame :D',
    'dobrodosli_opis' => 'Postani naš član, budi informisan, podrži zajednicu, počni sam da postavljaš filmove, budi nagradjen za svoj rad, Kinoteka.biz',

    'popcorn' => 'Kinoteka.biz',
    // Linkovi
    'sajt' => 'Posetite veb mesto',
    'administracija' => 'Administracija',

    // Podesavanja
    'podesavanja' => 'Podešavanja',
    'podesavanja_opis' => 'Podesite informacije vezane za vaš sajt kao što je naslov, ključne riječi...',

    'opsta' => 'Opšta',

    'naslov' => 'Naslov',
    'podnaslov' => 'Podnaslov',
    'description' => 'Opis',
    'opisvebmjesta' => 'Opis veb mjesta',
    'keywords' => 'Ključne riječi',
    'favicon' => 'Favicon URL',

    'webmaster' => 'Webmaster Alati',
    'bing' => 'Bing Webmaster Tools',
    'google' => 'Google Webmaster Tools',

    'social' => 'Social',
    'googleplus' => 'Google+ URL',
    'twitter' => 'Twitter Username',
    'facebook' => 'Facebook Page',
    'slika' => 'Slika URL',

    // Korisnici
    'korisnici' => 'Korisnici',
    'korisnici_opis' => 'Ovde možete da izmenite podatke od vašeg profila',
    'dodaj_korisnika' => 'Dodaj korisnika',
    'korisnicko_ime' => 'Korisničko ime',
    'izmeni_avatar' => 'Izmeni avatar',
    'email_adresa' => 'Email adresa',
    'funkcija' => 'Funkcija',
    'admin' => 'Admin',
    'korisnik' => 'Korisnik',
    'slug' => 'Slug',
    'lozinka' => 'Lozinka',
    'nova_lozinka' => 'Nova lozinka',
    'pogledaj_profil' => 'Pogledaj profil',
    'izmeni_profil' => 'Izmeni profil',
    'odjava' => 'Odjavi se',

    // Omiljeno
    'omiljeno' => 'Omiljeni filmovi',
    'omiljeno_opis' => 'Lista vaših besplatnih omiljenih filmova sa prevodom koje možete po želji gledati',
    'dodaj_omiljeno' => 'Dodaj u omiljene',
    'izbrisi_omiljeno' => 'Izbriši iz omiljenih',

    'ocena' => 'Ocena filma',

    // Kategorije
    'kategorije' => 'Kategorije',
    'kategorije_opis' => 'Upravljajte vašim kategorijama, dodajte nove ili izbrišite i izmenite postojeće',
    'dodaj_kategoriju' => 'Dodaj kategoriju',
    'pozicija' => 'Pozicija',
    'opis' => 'Opis',
    'boja' => 'Boja',

    // Oznake
    'oznake' => 'Oznake',
    'oznake_opis' => 'Upravljajte vašim oznakama, dodajte nove ili izbrišite i izmenite postojeće',
    'dodaj_oznaku' => 'Dodaj oznaku',

    // Funkcije
    'izbrisi' => 'Izbriši',
    'izmeni' => 'Izmeni',
    'pogledaj' => 'Pogledaj',
    'sacuvaj' => 'Sačuvaj',
    'dodaj' => 'Dodaj',
    'dodaj_opis' => 'Popunite prazna polja da bi dodali novi film',

    // Navigacija
    'registracija' => 'Registracija',
    'prijava' => 'Prijava',
    
    // Serije
    'serije' => 'Serije',

    // Filmovi
    'clanci' => 'Filmovi',
    'clanci_opis' => 'Upravljajte vašim filmovima, dodajte nove ili izbrišite i izmenite postojeće',
    'dodaj_clanak' => 'Dodaj film',
    'imdb' => 'IMDb kod',
    'embed' => 'Embed kod',
    'kategorija' => 'Kategorija',
    'objavljen' => 'Objavljen',
    'neobjavljen' => 'Neobjavljen',
    'izmeni_opis' => 'Izmenite podatke za film koji ste odabrali',
    'najnovije' => 'Najnovije',
    'popularno' => 'Popularno',
    'pocetna' => 'Početna',

    'random' => 'Najbolji filmovi',

    'popularni' => 'Popularni filmovi',
    'popularni_opis' => 'Ovde možete pratiti najgledanije besplatne filmove sa prevodom na Kinoteka.biz',
    // Pogledaj profil

    'statistika' => 'Statistika',
    'na_cekanje' => 'Filmovi na čekanje',

    // Pretraga
    'pretraga' => 'Pretraga',
    'pretraga_opis' => 'Pronađeni su filmovi koji ste uneli za pretragu',

    // Godina
    'godina' => 'Godina',

    'dodajte_film' => 'Dodajte film',
    'dodajte_film_opis' => 'Popunite prazna polja kako biste dodali željeni film, posle pregleda administratora biće objavljen.',
    'opis_filma' => 'Opis filma',
    'dodao_filmova'  => 'Dodao filmova',
    'dodati_filmova'  => 'Dodati filmovi',

    'google_share' => 'Podeli na Google+',
    'twitter_share' => 'Podeli na Twitter',
    'facebook_share' => 'Podeli na Facebook',

    // Problem
    'prijavi_problem' => 'Prijavite problem',
    'prijavi_opis' => 'Opišite problem',
    'posalji' => 'Pošalji',

    'kontakt' => 'Kontakt',

    'donacije' => 'Donacije',
    'donacije_opis' => 'Lista korisnika koji su donirali, na svakom filmu imate donaciju ispod te možete i vi učiniti isto i time doprinjeti unapređivanju Kinoteka.biz i obezbeđivanju novih nagrada.',

];
