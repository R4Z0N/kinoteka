@extends('admin.templates.admin')

@section('title', 'Kontrolna tabla')


@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Kontrolna tabla
    <small>Pregled stanja</small>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="callout callout-warning">
    <h4>Obavestenje!</h4>

    <p><b>Grafikoni</b> nisu jos uvjek podeseni, mozda u skorije vreme i bude sta od ovoga u funkciji :D.</p>
  </div>
  <div class="row">


    <div class="col-md-6">
      <!-- Top 10 filmova ove sedmice koji nisu na vidozi -->        <div class="box">
      <div class="box-header">
        <h3 class="box-title">Top 10 filmova ove sedmice koji nisu na vidozi</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body no-padding">
        <table class="table">
          <tr>
            <th style="width: 10px">ID</th>
            <th>Naslov</th>
            <th>Status</th>
            <th style="width: 40px">Pregleda</th>
            <th style="width: 40px">Opcije</th>
          </tr>
          @foreach ($novidoza as $movie)
          <tr >
            <td>{{ $movie->id }}</td>
            <td><a style="color: black;" href="/watch/{{ $movie->id }}/{{ $movie->slug }}" title="{{ $movie->naslov }}">{{ $movie->naslov }} ({{ $movie->godina }})</a></td>
            <td>
              <div class="progress progress-xs progress-striped active">
                <div class="progress-bar progress-bar-success" style="width: {{ $movie->wview }}%"></div>
              </div>
            </td>
            <td><span class="badge bg-green">{{ $movie->wview }}/100</span></td>
            <td style="padding: 0; text-align: center;"><a style="color: black;" href="{{ url('admin/clanci/'. $movie->id .'/edit') }}" class="btn btn-social-icon" style="float: left;" title="@lang('admin.izmeni')"><i class="fa fa-edit"></i></a></td>
          </tr>
          @endforeach
        </table>
		<center>
		{!! $novidoza->links() !!}
		</center>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- DONUT CHART -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Donut Chart</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body chart-responsive">
        <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

  </div>
  <!-- /.col (LEFT) -->
  <div class="col-md-6">
    <!-- LINE CHART -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Line Chart</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body chart-responsive">
        <div class="chart" id="line-chart" style="height: 300px;"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <!-- BAR CHART -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Bar Chart</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body chart-responsive">
        <div class="chart" id="bar-chart" style="height: 300px;"></div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->

  </div>
  <!-- /.col (RIGHT) -->
</div>
<!-- /.row -->

</section>

@stop

@section('content_js_footer')
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/plugins/morris/morris.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    "use strict";

    // LINE CHART
    var line = new Morris.Line({
      element: 'line-chart',
      resize: true,
      data: [
      {y: '2011 Q1', item1: 2666},
      {y: '2011 Q2', item1: 2778},
      {y: '2011 Q3', item1: 4912},
      {y: '2011 Q4', item1: 3767},
      {y: '2012 Q1', item1: 6810},
      {y: '2012 Q2', item1: 5670},
      {y: '2012 Q3', item1: 4820},
      {y: '2012 Q4', item1: 15073},
      {y: '2013 Q1', item1: 10687},
      {y: '2013 Q2', item1: 8432}
      ],
      xkey: 'y',
      ykeys: ['item1'],
      labels: ['Item 1'],
      lineColors: ['#3c8dbc'],
      hideHover: 'auto'
    });

    //DONUT CHART
    var donut = new Morris.Donut({
      element: 'sales-chart',
      resize: true,
      colors: ["#3c8dbc", "#f56954", "#00a65a"],
      data: [
      {label: "Download Sales", value: 12},
      {label: "In-Store Sales", value: 30},
      {label: "Mail-Order Sales", value: 20}
      ],
      hideHover: 'auto'
    });
    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
      {y: '2006', a: 100, b: 90},
      {y: '2007', a: 75, b: 65},
      {y: '2008', a: 50, b: 40},
      {y: '2009', a: 75, b: 65},
      {y: '2010', a: 50, b: 40},
      {y: '2011', a: 75, b: 65},
      {y: '2012', a: 100, b: 90}
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['CPU', 'DISK'],
      hideHover: 'auto'
    });
  });
</script>
@stop