@extends('admin.templates.admin')

@section('title', trans('admin.clanci'))
@section('content_css_head')
<!-- Select2 -->
<link rel="stylesheet" href="/plugins/select2/select2.min.css">

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>


@stop

@section('content')



    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Filmovi
        <small>Izmjena filma</small>
      </h1>
    </section>

    <section class="content">

<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Popuni</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
    <form action="{{ url('admin/clanci') }}" method="POST" files="true" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('POST') }}
        <input type="hidden" id="imdbRating" name="imdbRating">
        <input type="hidden" id="imdbVotes" name="imdbVotes">
        <input type="hidden" id="Poster" name="Poster">
        <div class="row" >
            <div class="alert fadeIn" style="margin: 0px 15px 10px 15px; padding: 10px; display: none;">
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" id="imdb" name="imdb" class="form-control" placeholder="@lang('admin.imdb')" value="{{app('request')->input('imdbID')}}">
                    <div id="update" class="glyphicon glyphicon-refresh " style="position: absolute; cursor: pointer; z-index: 0; width: 15px; height: 18px; color: rgb(170, 170, 170); top: 10px; right: 25px; display: block;"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" id="naslov" name="naslov" class="form-control" placeholder="@lang('admin.naslov')" value="{{app('request')->input('name')}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" id="year" name="godina" class="form-control" placeholder="Godina">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="kategorije_id" class="form-control">
                        @foreach($kategorije as $kategorija)
                            <option value="{{ $kategorija->id }}">{{ $kategorija->naslov }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="text" name="tags" class="form-control" placeholder="Tagovi (Ovdje obicno idu drugi nazivi za isti film)">
        </div>
        <div class="form-group">
            <textarea id="Plot" name="opis" class="form-control"></textarea>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="Director" name="Director" class="form-control" placeholder="Direktor">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="Writer" name="Writer" class="form-control" placeholder="Pisci">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="Actors" name="Actors" class="form-control" placeholder="Glumci">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" name="trailer_1" class="form-control" placeholder="Treiler #1 (Embed kod)">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" name="trailer_2" class="form-control" placeholder="Treiler #2 (Embed kod)">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" name="trailer_3" class="form-control" placeholder="Treiler #3 (Embed kod)">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="text" name="embed" class="form-control" placeholder="@lang('admin.embed')" value="{{app('request')->input('embed')}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="embed_q" class="form-control">
                        <option value="">Izaberi kvalitet</option>
                        <option value="1080p">Full HD (1080p)</option>
                        <option value="720p">HD (760p)</option>
                        <option value="SD">SD</option>
                        <option value="CAM">CAM</option>
                        <option value="3D">3D</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="text" name="embed_2" class="form-control" placeholder="@lang('admin.embed') #2">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="embed_q_2" class="form-control">
                        <option value="">Izaberi kvalitet</option>
                        <option value="1080p">Full HD (1080p)</option>
                        <option value="720p">HD (760p)</option>
                        <option value="SD">SD</option>
                        <option value="CAM">CAM</option>
                        <option value="3D">3D</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="text" name="embed_3" class="form-control" placeholder="@lang('admin.embed') #3">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="embed_q_3" class="form-control">
                        <option value="">Izaberi kvalitet</option>
                        <option value="1080p">Full HD (1080p)</option>
                        <option value="720p">HD (760p)</option>
                        <option value="SD">SD</option>
                        <option value="CAM">CAM</option>
                        <option value="3D">3D</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Žandrovi</label>
                    <select id="Genre" name="oznake[]" class="oznake form-control select2" multiple="multiple">
                        @foreach($oznake as $oznaka)
                            <option value="{{ $oznaka->id }}">{{ $oznaka->naslov }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Prevodi</label>
                    <select name="subtitles[]" class="subtitles oznake form-control select2" multiple="multiple">
                        @foreach($subtitles as $subtitle)
                            <option value="{{ $subtitle->id }}">{{ $subtitle->naslov }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>@lang('admin.objavljen')</label>
                    <select name="objavljen" class="form-control">
                        <option value="1">Da</option>
                        <option value="0">Ne</option>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>@lang('admin.slika')</label>
                    <input type="file" name="slika" accept="image/*" class="form-control">
                </div>
            </div>
        </div>

        <div class="form-group  pull-right">
            <button class="btn btn-success" title="@lang('admin.dodaj')" type="submit">@lang('admin.dodaj')</button>
        </div>
    </form>

           </div>
           <!-- /.box-body -->
         </div>

</section>
@stop


@section('content_js_footer_top')
<link rel="stylesheet"          href="/css/jquery-ui.css">
<script>

        $(function(){
            $("#naslov").focus(); //Focus on search field
            $("#naslov").autocomplete({
                minLength: 0,
                delay:1,
                source: "/suggest.php?category=feature",
                focus: function( event, ui ) {
                    $('#imdb').val( ui.item.value );
                    return false;
                },
                select: function( event, ui ) {
                    $('#imdb').val( ui.item.value );
                    apiCall();
                    return false;
                }
            }).data("uiAutocomplete")._renderItem = function( ul, item ) {
                return $("<li></li>")
                    .data( "item.autocomplete", item )
                    .append( "<a>" + (item.img?"<img class='imdbImage' src='/imdbImage.php?url=" + item.img + "' />":"") + "<span class='imdbTitle'>" + item.label + "</span>  <span class='imdbYear'>("+ item.year +")</spane>"+ (item.cast?"<br /><span class='imdbCast'>" + item.cast + "</span>":"") + "<div class='clearfix'></div></a>" )
                    .appendTo( ul );
            };
        });
    </script>
@stop

@section('content_js_footer')
<!-- Autocomplite -->
<script src="/js/jquery-ui.js"></script>
<!-- Select2 -->
<script src="/plugins/select2/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>


<script>


  function apiCall(){
    $("#update").addClass('fa-spin'); 
    var imdb = $('#imdb').val();
    $('.alert').removeClass().addClass('alert fadeIn alert-warning').fadeIn( "slow" ).text('Podatke sinhonizujemo...');
    $.getJSON('https://www.omdbapi.com/?i='+imdb+'&apikey=e93b1ffa').then(function(imdb_api){
        // Ciscenje zandrova
        $("#Genre").select2().val(null).trigger("change");
        if(!imdb_api['Error']) {
        // Upisi u polja :D
        $('#naslov').val(imdb_api['Title']);
        $('#year').val(imdb_api['Year']);
//      $('#Plot').val(imdb_api['Plot']);
        $('#Director').val(imdb_api['Director']);
        $('#Writer').val(imdb_api['Writer']);
        $('#Actors').val(imdb_api['Actors']);
        $('#imdbRating').val(imdb_api['imdbRating']);
        $('#imdbVotes').val(imdb_api['imdbVotes']);
        $('#Poster').val(imdb_api['Poster']);
        // Zandrovi
        var sel_zandrovi = [];
        if (imdb_api['Genre'].indexOf('Action') != -1) { sel_zandrovi.push('1'); }
        if (imdb_api['Genre'].indexOf('Animation') != -1) { sel_zandrovi.push('2'); }
        if (imdb_api['Genre'].indexOf('Adventure') != -1) { sel_zandrovi.push('3'); }
        if (imdb_api['Genre'].indexOf('Biography') != -1) { sel_zandrovi.push('4'); }
        if (imdb_api['Genre'].indexOf('Documentary') != -1) { sel_zandrovi.push('5'); }
        if (imdb_api['Genre'].indexOf('Drama') != -1) { sel_zandrovi.push('6'); }
        if (imdb_api['Genre'].indexOf('Fantasy') != -1) { sel_zandrovi.push('7'); }
        if (imdb_api['Genre'].indexOf('Horror') != -1) { sel_zandrovi.push('8'); }
        if (imdb_api['Genre'].indexOf('History') != -1) { sel_zandrovi.push('9'); }
        if (imdb_api['Genre'].indexOf('Comedy') != -1) { sel_zandrovi.push('10'); }
        if (imdb_api['Genre'].indexOf('Crime') != -1) { sel_zandrovi.push('11'); }
        if (imdb_api['Genre'].indexOf('Mystery') != -1) { sel_zandrovi.push('12'); }
        if (imdb_api['Genre'].indexOf('Musical') != -1) { sel_zandrovi.push('13'); }
        if (imdb_api['Genre'].indexOf('Sci-Fi') != -1) { sel_zandrovi.push('14'); }
        if (imdb_api['Genre'].indexOf('War') != -1) { sel_zandrovi.push('15'); }
        if (imdb_api['Genre'].indexOf('Romance') != -1) { sel_zandrovi.push('16'); }
        if (imdb_api['Genre'].indexOf('Sport') != -1) { sel_zandrovi.push('17'); }
        if (imdb_api['Genre'].indexOf('Thriller') != -1) { sel_zandrovi.push('18'); }
        if (imdb_api['Genre'].indexOf('Western') != -1) { sel_zandrovi.push('19'); }
        if (imdb_api['Genre'].indexOf('Family') != -1) { sel_zandrovi.push('20'); }

        $("#Genre").select2().val(sel_zandrovi).trigger("change");
}
        if(imdb_api['Error']) { $('.alert').removeClass().addClass('alert fadeIn alert-danger').fadeIn( "slow" ).text(imdb_api['Error']);  }
        else { $('.alert').removeClass().addClass('alert fadeIn alert-success').fadeIn( "slow" ).text('Uspjesno ste sinhronizovali podatke sa imdb-om :D');}

      console.log(imdb_api);
    })
    // Ako se pojavi greska prilikom sinhronizacije na serveru tipa 503 i 404..
    .fail(function() {
        $('.alert').removeClass().addClass('alert fadeIn alert-danger').fadeIn( "slow" ).html("Greška prilikom sinhonizacije, otvorite ovaj link <a href='https://www.omdbapi.com/?i="+imdb+"&apikey=e93b1ffa' target='_blank'>https://www.omdbapi.com/?i="+imdb+"&apikey=e93b1ffa</a> i pokušajte ponovo");
    })
    // Brisanje klase za rotiranje dugmeta
    .always(function() {
        $("#update").removeClass('fa-spin');
    })
  }

$('#update').click(function () { apiCall(); });

</script>

@stop