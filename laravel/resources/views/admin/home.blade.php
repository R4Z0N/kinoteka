@extends('admin.templates.admin')

@section('title', trans('admin.podesavanja'))

@section('navigacija')
@include ('admin.navigacija')
@stop

@section('naslov')
<h1>@lang('admin.podesavanja')</h1>
<p>@lang('admin.podesavanja_opis')</p>
@stop

@section('content')

<ul class="nav nav-tabs" role="tablist">
<li role="presentation" class="active"><a href="#opsta" aria-controls="opsta" role="tab" data-toggle="tab">@lang('admin.opsta')</a></li>
<li role="presentation"><a href="#webmaster" aria-controls="webmaster" role="tab" data-toggle="tab">@lang('admin.webmaster')</a></li>
<li role="presentation"><a href="#social" aria-controls="social" role="tab" data-toggle="tab">@lang('admin.social')</a></li>
</ul>

<form action="{{ url('admin', $podesavanja->id) }}" method="POST">
<input type="hidden" name="_method" value="PATCH">
<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
<div class="tab-content">

      <div role="tabpanel" class="tab-pane active" id="opsta">

      <h2>@lang('admin.naslov')</h2>
      <input type="text" name="naslov" value="{{ $podesavanja->naslov }}" class="form-control">

      <h2>@lang('admin.podnaslov')</h2>
      <input type="text" name="podnaslov" value="{{ $podesavanja->podnaslov }}" class="form-control">

      <h2>@lang('admin.opisvebmjesta')</h2>
      <input type="text" name="description" value="{{ $podesavanja->description }}" class="form-control">

      <h2>@lang('admin.keywords')</h2>
      <input type="text" name="keywords" value="{{ $podesavanja->keywords }}" class="form-control">

      <h2>@lang('admin.favicon')</h2>
      <input type="text" name="favicon" value="{{ $podesavanja->favicon }}" class="form-control">
      </div>


      <div role="tabpanel" class="tab-pane" id="webmaster">
      <h2><a href="https://www.google.com/webmasters/verification/verification?hl=en&siteUrl={{ url('/') }}" title="@lang('admin.google')">@lang('admin.google')</a></h2>
      <input type="text" name="google" value="{{ $podesavanja->google }}" class="form-control">

      <h2><a href="http://www.bing.com/webmaster/?rfp=1#/Dashboard/?url={{ url('/') }}" title="@lang('admin.bing')">@lang('admin.bing')</a></h2>
      <input type="text" name="bing" value="{{ $podesavanja->bing }}" class="form-control">
      </div>


      <div role="tabpanel" class="tab-pane" id="social">
      <h2>@lang('admin.googleplus')</h2>
      <input type="text" name="googleplus" value="{{ $podesavanja->googleplus }}" class="form-control">

      <h2>@lang('admin.twitter')</h2>
      <input type="text" name="twitter" value="{{ $podesavanja->twitter }}" class="form-control">

      <h2>@lang('admin.facebook')</h2>
      <input type="text" name="facebook" value="{{ $podesavanja->facebook }}" class="form-control">

      <h2>@lang('admin.slika')</h2>
      <input type="text" name="slika" value="{{ $podesavanja->slika }}" class="form-control">
      </div>


</div>
</div>

<div class="form-group">
<button class="btn btn-success pull-right" title="@lang('admin.sacuvaj')" type="submit">@lang('admin.sacuvaj')</button>
</div>
</form>




@stop
