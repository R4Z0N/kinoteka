<div class="form-group">
<input type="text" name="naslov" class="form-control" placeholder="@lang('admin.naslov')">
</div>

<div class="form-group">
  <input type="text" name="opis" class="form-control" placeholder="@lang('admin.opis')">
</div>

<div class="form-group">
	<input type="text" name="boja" class="form-control"  placeholder="@lang('admin.boja')">
</div>

<div class="form-group  pull-right">
    <button class="btn btn-success" title="@lang('admin.dodaj')" type="submit">@lang('admin.dodaj')</button>
    <button class="btn btn-default" id="idinavrh" data-toggle="tooltip" data-placement="right" title="Idi na vrh"><i class="fa fa-angle-double-up"></i></button>
</div>
