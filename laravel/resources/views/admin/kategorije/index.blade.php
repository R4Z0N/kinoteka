@extends('admin.templates.admin')

@section('title', trans('admin.kategorije'))

@section('navigacija')
@include ('admin.navigacija')
<div class="btn-group navbar-btn navbar-right">
<a href="#kateogrija" class="btn btn-danger kateogrija" title="@lang('admin.dodaj_kategoriju')"><i class="fa fa-plus"></i> @lang('admin.dodaj_kategoriju')</a>
</div>
@stop

@section('naslov')
<h1>@lang('admin.kategorije')</h1>
<p>@lang('admin.kategorije_opis')</p>
@stop

@section('content')
<table class="table table-striped">
<thead>

<tr>
<th width="8%"><h2>@lang('admin.pozicija')</h2></th>
<th width="20%"><h2>@lang('admin.naslov')</h2></th>
<th width="50%"><h2>@lang('admin.description')</h2></th>
<th width="20%"><h2>@lang('admin.boja')</h2></th>
<th width="1%"></th>
<th width="1%"></th>
</tr>
</thead>
<tbody>

@if ($kategorije->count())
@foreach($kategorije as $kategorija)
<form action="{{ url('admin/kategorije', $kategorija->id) }}" method="POST">
<input type="hidden" name="_method" value="PATCH">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<tr>
<td><input type="text" name="pozicija" value="{{ $kategorija->pozicija }}" class="form-control"></td>
<td><input type="text" name="naslov" value="{{ $kategorija->naslov }}" class="form-control"></td>
<td><input type="text" name="opis" value="{{ $kategorija->opis }}" class="form-control"></td>
<td><input type="text" name="boja" value="{{ $kategorija->boja }}" class="form-control"></td>

<td class="text-right">
<button class="dugme" title="@lang('admin.izmeni')" type="submit"><i class="fa fa-pencil"></i></button>
</form>
</td>
<td class="text-right">
<form action="{{ url('admin/kategorije', $kategorija->id) }}" method="POST">
<input type="hidden" name="_method" value="DELETE">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button onclick="return confirm('Da li ste sigurni da želite da izbrišete?');" class="dugme" title="@lang('admin.izbrisi')" type="submit"><i class="fa fa-trash-o"></i></button>
</form>
</td>

</tr>
@endforeach
@endif
</tbody>
</table>

{!! $kategorije->render() !!}

</div>
<div id="kateogrija">
<div class="container">
<form action="{{ url('admin/kategorije') }}" method="POST">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
@include ('admin.kategorije.form')
</form>
</div>
</div>

<div class="container">

@stop
