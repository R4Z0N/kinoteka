<div class="form-group">
	<input type="text" name="name" class="form-control" placeholder="  @lang('admin.korisnicko_ime')">
</div>

<div class="form-group">
	<input type="email" name="email" class="form-control" placeholder="  @lang('admin.email_adresa')">
</div>

<div class="form-group">
	<input type="password" name="password" class="form-control" placeholder="@lang('admin.lozinka')">
</div>

<div class="form-group  pull-right">
    <button class="btn btn-success" title="@lang('admin.dodaj')" type="submit">@lang('admin.dodaj')</button>
    <button class="btn btn-default" id="idinavrh" data-toggle="tooltip" data-placement="right" title="Idi na vrh"><i class="fa fa-angle-double-up"></i></button>
</div>
