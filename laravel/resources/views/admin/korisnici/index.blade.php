@extends('admin.templates.admin')

@section('title', trans('admin.korisnici'))

@section('navigacija')
@include ('admin.navigacija')
<div class="btn-group navbar-btn navbar-right">
<a href="#dodaj" class="btn btn-danger dodaj" title="@lang('admin.dodaj_korisnika')"><i class="fa fa-plus"></i> @lang('admin.dodaj_korisnika')</a>
</div>
@stop

@section('naslov')
<h1>@lang('admin.korisnici')</h1>
<p>@lang('admin.korisnici_opis')</p>
@stop

@section('content')
<table class="table table-striped">
<thead>

<tr>
<th width="30%"><h2>@lang('admin.korisnicko_ime')</h2></th>
<th width="10%"><h2>@lang('admin.slug')</h2></th>
<th width="37%"><h2>@lang('admin.email_adresa')</h2></th>
<th width="20%"><h2>@lang('admin.funkcija')</h2></th>
<th width="1%"></th>
<th width="1%"></th>
<th width="1%"></th>
</tr>
</thead>
<tbody>

@if ($users->count())
@foreach($users as $user)
<form action="{{ url('admin/korisnici', $user->id) }}" method="POST">
<input type="hidden" name="_method" value="PATCH">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<tr>
<td><input type="text" name="name" placeholder="{{ $user->name }}" class="form-control"></td>
<td><input type="text" name="slug" placeholder="{{ $user->slug }}" class="form-control"></td>
<td><input type="email" name="email" placeholder="{{ $user->email }}" class="form-control"></td>
<td>

<select name="admin" class="form-control">
@if ($user->admin == 1)
<option value="1">Admin</option>
<option value="0">Korisnik</option>
@else
<option value="0">Korisnik</option>
<option value="1">Admin</option>
@endif
</select>
</td>

<td class="text-right">
<button class="dugme" title="@lang('admin.izmeni')" type="submit"><i class="fa fa-pencil"></i></button>
</form>
</td>

<td class="text-right">
<a href="/korisnik/{{ $user->slug }}" title="@lang('admin.pogledaj')"><button class="dugme" ><i class="fa fa-eye"></i></button>
</a>
</td>

<td class="text-right">
<form action="{{ url('admin/korisnici', $user->id) }}" method="POST">
<input type="hidden" name="_method" value="DELETE">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
<button onclick="return confirm('Da li ste sigurni da želite da izbrišete?');" class="dugme" title="@lang('admin.izbrisi')" type="submit"><i class="fa fa-trash-o"></i></button>
</form>
</td>

</tr>
@endforeach
@endif
</tbody>
</table>

{!! $users->render() !!}

</div>
<div id="dodaj">
<div class="container">
<form action="{{ url('admin/korisnici') }}" method="POST">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
@include ('admin.korisnici.form')
</form>
</div>
</div>
<div class="container">

@stop
