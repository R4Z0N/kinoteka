<div class="btn-group navbar-btn">
<button type="button" style="margin-right:10px;" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i></button>
<ul class="dropdown-menu">
<li {!! Request::is('admin') ? 'class="active"' : '' !!}><a href="/admin" data-toggle="tooltip" data-placement="bottom" title="@lang('admin.podesavanja')">@lang('admin.podesavanja')</a></li>
<li class="divider"></li>
<li {!! Request::is('admin/kategorije') ? 'class="active"' : '' !!}><a href="/admin/kategorije" title="@lang('admin.kategorije')">@lang('admin.kategorije')</a></li>
<li {!! Request::is('admin/oznake') ? 'class="active"' : '' !!}><a href="/admin/oznake" title="@lang('admin.oznake')">@lang('admin.oznake')</a></li>
<li {!! Request::is('admin/korisnici') ? 'class="active"' : '' !!}><a href="/admin/korisnici" title="@lang('admin.korisnici')">@lang('admin.korisnici')</a></li>
</ul>
</div>

<div class="btn-group navbar-btn">
<a href="/admin/clanci" class="btn btn-default kateogrija {!! Request::is('admin/clanci') ? 'disabled' : '' !!}"><i class="fa fa-film"></i> @lang('admin.clanci')</a>
<a href="/admin/serije" class="btn btn-default kateogrija {!! Request::is('admin/serije') ? 'disabled' : '' !!}"><i class="fa fa-file-video-o"></i> @lang('admin.serije')</a>
</div>

<ul class="nav navbar-nav navbar-right">
<li class="dropdown">
<a href="" class="navbar-gravatar dropdown-toggle" data-toggle="dropdown">
@if (Auth::user()->avatar)
<img width="40" height="40" alt="{{ auth()->user()->name }}" src="/img/avatar/{{ auth()->user()->avatar }}"> {{ auth()->user()->name }}
@else
<img width="40" height="40" alt="{{ auth()->user()->name }}" src="/img/avatar/avatar.jpg"> {{ auth()->user()->name }}
@endif
</a>
<ul class="dropdown-menu">
<li><a href="/" title="@lang('admin.sajt')">@lang('admin.sajt')</a></li>
<li><a href="/korisnik/{{ auth()->user()->slug }}/edit" title="@lang('admin.izmeni_profil')">@lang('admin.izmeni_profil')</a></li>
<li class="divider"></li>
<li><a href="/logout" title="@lang('admin.odjava')">@lang('admin.odjava')</a></li>
</ul>
</li>
</ul>
