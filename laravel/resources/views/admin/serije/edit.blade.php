@extends('admin.templates.admin')

@section('title', trans('admin.serial'))
@section('content_css_head')
<!-- Select2 -->
<link rel="stylesheet" href="/plugins/select2/select2.min.css">
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Filmovi
        <small>Izmjena filma</small>
      </h1>
    </section>

    <section class="content">


<div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $serial->naslov }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
 

<form action="{{ url('admin/serial', $serial->id) }}" method="POST" files="true" enctype="multipart/form-data">
{{ csrf_field() }}
{{ method_field('PATCH') }}
        <input type="hidden" id="imdbRating" name="imdbRating" value="{{ $serial->imdbRating }}">
        <input type="hidden" id="imdbVotes" name="imdbVotes" value="{{ $serial->imdbVotes }}">
        <input type="hidden" id="Poster" name="Poster">
        <div class="row" >
            <div class="alert fadeIn" style="margin: 0px 15px 10px 15px; padding: 10px; display: none;">
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" id="imdb" name="imdb" class="form-control" value="{{ $serial->imdb }}" placeholder="@lang('admin.imdb')">
                    <div id="update" class="glyphicon glyphicon-refresh " style="position: absolute; cursor: pointer; z-index: 0; width: 15px; height: 18px; color: rgb(170, 170, 170); top: 10px; right: 25px; display: block;"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input type="text" id="naslov" name="naslov" class="form-control" value="{{ $serial->naslov }}" placeholder="@lang('admin.naslov')">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" id="year" name="godina" class="form-control" value="{{ $serial->godina }}" placeholder="Godina">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="kategorije_id" class="form-control">
                        @foreach($kategorije as $kategorija)
                            <option value="{{ $kategorija->id }}" {{ ($serial->kategorije_id == $kategorija->id) ? 'selected':'' }}>{{ $kategorija->naslov }}</option>
                        @endforeach
                    </select>
                </div>
           </div>
        </div>
        <div class="form-group">
            <input type="text" name="tags" class="form-control" value="{{ $serial->tags }}" placeholder="Tagovi (Ovdje obicno idu drugi nazivi za isti film)">
        </div>
        <div class="form-group">
            <textarea id="Plot" name="opis" class="form-control">{{ $serial->opis }}</textarea>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="Director" name="Director" class="form-control" value="{{ $serial->Director }}"  placeholder="Direktor">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="Writer" name="Writer" class="form-control" value="{{ $serial->Writer }}"  placeholder="Pisci">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="Actors" name="Actors" class="form-control" value="{{ $serial->Actors }}"  placeholder="Glumci">
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" name="trailer_1" class="form-control" value="{{ $serial->trailer_1 }}"  placeholder="Treiler #1 (Embed kod)">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" name="trailer_2" class="form-control" value="{{ $serial->trailer_2 }}"  placeholder="Treiler #2 (Embed kod)">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" name="trailer_3" class="form-control" value="{{ $serial->trailer_3 }}"  placeholder="Treiler #3 (Embed kod)">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="text" name="embed" class="form-control" value="{{ $serial->embed }}" placeholder="@lang('admin.embed')">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="embed_q" class="form-control">
                        <option value="">Izaberi kvalitet</option>
                        <option @if($serial->embed_q=='1080p') selected @endif value="1080p">Full HD (1080p)</option>
                        <option @if($serial->embed_q=='720p') selected @endif value="720p">HD (720p)</option>
                        <option @if($serial->embed_q=='SD') selected @endif value="SD">SD</option>
                        <option @if($serial->embed_q=='CAM') selected @endif value="CAM">CAM</option>
                        <option @if($serial->embed_q=='3D') selected @endif value="3D">3D</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="text" name="embed_2" class="form-control" value="{{ $serial->embed_2 }}" placeholder="@lang('admin.embed') #2">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="embed_q_2" class="form-control">
                        <option value="">Izaberi kvalitet</option>
                        <option @if($serial->embed_q_2=='1080p') selected @endif value="1080p">Full HD (1080p)</option>
                        <option @if($serial->embed_q_2=='720p') selected @endif value="720p">HD (720p)</option>
                        <option @if($serial->embed_q_2=='SD') selected @endif value="SD">SD</option>
                        <option @if($serial->embed_q_2=='CAM') selected @endif value="CAM">CAM</option>
                        <option @if($serial->embed_q_2=='3D') selected @endif value="3D">3D</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <input type="text" name="embed_3" class="form-control" value="{{ $serial->embed_3 }}" placeholder="@lang('admin.embed') #3">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <select name="embed_q_3" class="form-control">
                        <option value="">Izaberi kvalitet</option>
                        <option @if($serial->embed_q_3=='1080p') selected @endif value="1080p">Full HD (1080p)</option>
                        <option @if($serial->embed_q_3=='720p') selected @endif value="720p">HD (720p)</option>
                        <option @if($serial->embed_q_3=='SD') selected @endif value="SD">SD</option>
                        <option @if($serial->embed_q_3=='CAM') selected @endif value="CAM">CAM</option>
                        <option @if($serial->embed_q_3=='3D') selected @endif value="3D">3D</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Zandrovi</label>
                    <select id="Genre" name="oznake[]" class="oznake form-control select2" multiple="multiple">

                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Prevodi</label>
                    <select name="subtitles[]" class="subtitles oznake form-control select2" multiple="multiple">

                    </select>
                </div>
            </div>
        </div>
       <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>@lang('admin.objavljen')</label>
                    <select name="objavljen" class="form-control">
                        @if ($serial->objavljen == 1)
                        <option value="1">Da</option>
                        <option value="0">Ne</option>
                        @else
                        <option value="0">Ne</option>
                        <option value="1">Da</option>
                        @endif
                    </select>
               </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>@lang('admin.slika')</label>
                    <input type="file" name="slika" accept="image/*" class="form-control">
                </div>
            </div>
        </div>

        <div class="form-group  pull-right">
            <button class="btn btn-success" title="@lang('admin.izmeni')" type="submit">@lang('admin.izmeni')</button>
        </div>
    </form>

            </div>
            <!-- /.box-body -->
          </div>

</section>
@stop

@section('content_js_footer')
<!-- Select2 -->
<script src="/plugins/select2/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>


<script type="text/javascript">
    $(".oznake").select2({
        placeholder: 'Izaberite žanr',
    });
</script>
<script type="text/javascript">
    $(".subtitles").select2({
        placeholder: 'Ubaceni prevodi?',
    });
</script>
<script>

$('#update').click(function () {

$("#update").addClass('fa-spin'); 
/*
SetTimeout(function(){
    $("#update").removeClass('fa-spin'); 
},2000);
*/
  function apiCall(){
    var imdb = $('#imdb').val();
    $('.alert').removeClass().addClass('alert fadeIn alert-warning').fadeIn( "slow" ).text('Podatke sinhonizujemo...');
    $.getJSON('https://www.omdbapi.com/?i='+imdb+'&apikey=e93b1ffa').then(function(imdb_api){
        console.log(imdb_api);
        // Ciscenje zandrova
        $("#Genre").select2().val(null).trigger("change");
if(!imdb_api['Error']) {
        // Upisi u polja :D
        $('#naslov').val(imdb_api['Title']);
        $('#year').val(imdb_api['Year']);
//      $('#Plot').val(imdb_api['Plot']);
        $('#Director').val(imdb_api['Director']);
        $('#Writer').val(imdb_api['Writer']);
        $('#Actors').val(imdb_api['Actors']);
        $('#imdbRating').val(imdb_api['imdbRating']);
        $('#imdbVotes').val(imdb_api['imdbVotes']);
        $('#Poster').val(imdb_api['Poster']);
        // Zandrovi
        var sel_zandrovi = [];
        if (imdb_api['Genre'].indexOf('Action') != -1) { sel_zandrovi.push('1'); }
        if (imdb_api['Genre'].indexOf('Animation') != -1) { sel_zandrovi.push('2'); }
        if (imdb_api['Genre'].indexOf('Adventure') != -1) { sel_zandrovi.push('3'); }
        if (imdb_api['Genre'].indexOf('Biography') != -1) { sel_zandrovi.push('4'); }
        if (imdb_api['Genre'].indexOf('Documentary') != -1) { sel_zandrovi.push('5'); }
        if (imdb_api['Genre'].indexOf('Drama') != -1) { sel_zandrovi.push('6'); }
        if (imdb_api['Genre'].indexOf('Fantasy') != -1) { sel_zandrovi.push('7'); }
        if (imdb_api['Genre'].indexOf('Horror') != -1) { sel_zandrovi.push('8'); }
        if (imdb_api['Genre'].indexOf('History') != -1) { sel_zandrovi.push('9'); }
        if (imdb_api['Genre'].indexOf('Comedy') != -1) { sel_zandrovi.push('10'); }
        if (imdb_api['Genre'].indexOf('Crime') != -1) { sel_zandrovi.push('11'); }
        if (imdb_api['Genre'].indexOf('Mystery') != -1) { sel_zandrovi.push('12'); }
        if (imdb_api['Genre'].indexOf('Musical') != -1) { sel_zandrovi.push('13'); }
        if (imdb_api['Genre'].indexOf('Sci-Fi') != -1) { sel_zandrovi.push('14'); }
        if (imdb_api['Genre'].indexOf('War') != -1) { sel_zandrovi.push('15'); }
        if (imdb_api['Genre'].indexOf('Romance') != -1) { sel_zandrovi.push('16'); }
        if (imdb_api['Genre'].indexOf('Sport') != -1) { sel_zandrovi.push('17'); }
        if (imdb_api['Genre'].indexOf('Thriller') != -1) { sel_zandrovi.push('18'); }
        if (imdb_api['Genre'].indexOf('Western') != -1) { sel_zandrovi.push('19'); }
        if (imdb_api['Genre'].indexOf('Family') != -1) { sel_zandrovi.push('20'); }

        $("#Genre").select2().val(sel_zandrovi).trigger("change");
}
        if(imdb_api['Error']) { $('.alert').removeClass().addClass('alert fadeIn alert-danger').fadeIn( "slow" ).text(imdb_api['Error']);  }
        else { $('.alert').removeClass().addClass('alert fadeIn alert-success').fadeIn( "slow" ).text('Uspjesno ste sinhronizovali podatke sa imdb-om :D');}

      console.log(imdb_api);
    })
    // Ako se pojavi greska prilikom sinhronizacije na serveru tipa 503 i 404..
    .fail(function() {
        $('.alert').removeClass().addClass('alert fadeIn alert-danger').fadeIn( "slow" ).html("Greška prilikom sinhonizacije, pokušajte ponovo. Otvori ovaj link <a href='https://www.omdbapi.com/?i="+imdb+"&apikey=e93b1ffa' target='_blank'>https://www.omdbapi.com/?i="+imdb+"&apikey=e93b1ffa</a> i pokusaj ponovo.");
    })
    // Brisanje klase za rotiranje dugmeta
    .always(function() {
        $("#update").removeClass('fa-spin');
    })
  }
    apiCall();


    });

</script>

@stop