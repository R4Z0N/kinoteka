@extends('admin.templates.admin')

@section('title', 'Lista serija')

@section('naslov')
<h1>Serije</h1>
<p>@lang('admin.clanci_opis')</p>
@stop

@section('content_css_head')
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Serije
        <small>Kontrolna ploca</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
     <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">U bazi</span>
              <span class="info-box-number">{{ $series->all }} filmova</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Objavljenih</span>
              <span class="info-box-number">{{ $series->online }} filmova</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Na čekanju</span>
              <span class="info-box-number">{{ $series->padding }} filmova</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Bez streama</span>
              <span class="info-box-number">{{ $series->danger }} filmova</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

<style> 
td a {
  font-size: 16px;
  color: black;
}
</style>

<div class="row">
   <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista serija</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="movies" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width: 14px">ID</th>
                <th>Naslov</th>
                <th>Pregleda</th>
                <th>Korisnik</th>
                <th>Status</th>
                <th style="width: 50px">Opcije</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th style="width: 14px">ID</th>
                <th>Naslov</th>
                <th>Pregleda</th>
                <th>Korisnik</th>
                <th>Status</th>
                <th style="width: 50px">Opcije</th>
              </tr>
            </tfoot>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
    </section>
    <!-- /.content -->

@stop


@section('content_js_footer')

<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
  $(function () {
    $('#movies').DataTable({
      "order": [[ 5, 'asc' ], [ 2, 'desc' ]],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "data": [

@foreach($series as $serial)
    [
      "{{ $serial->id }}",
      "<a href=\"/serial/{{ $serial->id }}/{{ $serial->slug }}\" title=\"{{ $serial->name }}\">{{ $serial->name }}</a>",
      "{{ $serial->views }}",
      "<a href=\"/korisnik/{{ $serial->slug }}\" title=\"{{ $serial->name }}\">{{ $serial->name }}</a>",

    @if ($serial->objavljen)
      "@if (!$serial->embed) <span class=\"label label-danger\">Nema stream</span> @endif <span class=\"label label-success\">Objavljen</span>",
    @else
      "@if (!$serial->embed) <span class=\"label label-danger\">Nema stream</span> @endif <span class=\"label label-warning\">Na cekanju</span>",
    @endif
      "<a href=\"{{ url('admin/serije/'. $serial->id .'/edit') }}\" class=\"btn btn-social-icon\" style=\"float: left;\" title=\"@lang('admin.izmeni')\"><i class=\"fa fa-pencil\"></i></a><form  action=\"{{ url('admin/serije', $serial->id) }}\" method=\"POST\" style=\"float: left;\"><input type=\"hidden\" name=\"_method\" value=\"DELETE\"><input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token() }}\"><button onclick=\"return confirm('Da li ste sigurni da želite da izbrišete?');\" class=\"btn\" title=\"@lang('admin.izbrisi')\" type=\"submit\"><i class=\"fa fa-trash-o\"></i></button></form>"
    ],
@endforeach   
] 
    });
  });
</script>
@stop