  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        @if (Auth::user()->avatar)
          <img src="/img/avatar/{{ auth()->user()->avatar }}" class="img-circle" alt="User Image">
        @else
          <img src="/img/avatar/avatar.jpg" class="img-circle" alt="User Image">
        @endif
        </div>
        <div class="pull-left info">
          <p>{{ auth()->user()->name }} - Administrator</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">NAVIGACIJA</li>
        <li class="{!! Request::is('admin/clanci') ? 'active' : '' !!}{!! Request::is('admin/clanci/create') ? 'active' : '' !!} {!! Request::is('admin/clanci/nosubtitle') ? 'active' : '' !!} treeview">
          <a href="#">
            <i class="fa fa-film"></i> <span>Filmovi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{!! Request::is('admin/clanci') ? 'active' : '' !!}"><a href="/admin/clanci"><i class="fa fa-circle-o"></i> Lista filmova</a></li>
            <li class="{!! Request::is('admin/clanci/nosubtitle') ? 'active' : '' !!}"><a href="/admin/clanci/nosubtitle"><i class="fa fa-circle-o"></i> Lista filmova bez prevoda</a></li>
            <li class="{!! Request::is('admin/clanci/create') ? 'active' : '' !!}"><a href="/admin/clanci/create"><i class="fa fa-circle-o"></i> Dodaj film</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-video-camera"></i>
            <span>Serije</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/serije"><i class="fa fa-circle-o"></i> Lista serija</a></li>
            <li><a href="/admin/serije/create"><i class="fa fa-circle-o"></i> Dodaj seriju</a></li>
          </ul>
        </li>
        <li {!! Request::is('admin/stream-index') ? 'class="active"' : '' !!} >
          <a href="/admin/stream-index">
            <i class="fa fa-link"></i> <span>Stream index</span>
          </a>
        </li>
        <li {!! Request::is('admin/torrent') ? 'class="active"' : '' !!} >
          <a href="/admin/torrent">
            <i class="fa fa-rss-square"></i> <span>Torrent</span>
          </a>
        </li>
        <li {!! Request::is('admin/kategorije') ? 'class="active"' : '' !!} >
          <a href="/admin/kategorije">
            <i class="fa fa-folder-open"></i> <span>Kategorije</span>
          </a>
        </li>
        <li {!! Request::is('admin/oznake') ? 'class="active"' : '' !!} >
          <a href="/admin/oznake">
            <i class="fa fa-tag"></i> <span>Oznake</span>
          </a>
        </li>
        <li {!! Request::is('admin/korisnici') ? 'class="active"' : '' !!} >
          <a href="/admin/korisnici">
            <i class="fa fa-users"></i> <span>Korisnici</span>
          </a>
        </li>
        <li {!! Request::is('admin/podesavanje') ? 'class="active"' : '' !!} >
          <a href="/admin/podesavanja">
            <i class="fa fa-gear"></i> <span>Podesavanja</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>