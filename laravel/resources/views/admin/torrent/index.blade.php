@extends('admin.templates.admin')

@section('title', trans('admin.clanci'))

@section('naslov')
<h1>@lang('admin.clanci')</h1>
<p>@lang('admin.clanci_opis')</p>
@stop

@section('content_css_head')
  <!-- DataTables -->
  <link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">
@stop
@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Torrenti
        <small>Kontrolna ploca</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
     <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Ukupno</span>
              <span class="info-box-number">{{$ukupno}} torenata</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Objavljenih</span>
              <span class="info-box-number">num torenata</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Preostalo</span>
              <span class="info-box-number">num torenata</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-film"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Novih</span>
              <span class="info-box-number">num torenata</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

<style> 
td a, td {
  font-size: 16px;
  color: black;
}
</style>

<?php
// converts pure string into a trimmed keyed array
function string2KeyedArray($string, $delimiter = ',', $kv = '=>') {
  if ($a = explode($delimiter, $string)) { // create parts
    foreach ($a as $s) { // each part
      if ($s) {
        if ($pos = strpos($s, $kv)) { // key/value delimiter
          $ka[trim(substr($s, 0, $pos))] = trim(substr($s, $pos + strlen($kv)));
        } else { // key delimiter not found
          $ka[] = trim($s);
        }
      }
    }
    return $ka;
  }
} // string2KeyedArray
?>

<div class="row">
   <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Torrenti</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="torrents" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="width: 14px">ID</th>
                <th>Naslov</th>
                <th>Godina</th>
                <th>Torrent</th>
                <th>Zandrovi</th>
                <th>imdbID</th>
                <th>Ocjena</th>
                <th>Dodato</th>
                <th style="width: 50px">Opcije</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th style="width: 14px">ID</th>
                <th>Naslov</th>
                <th>Godina</th>
                <th>Torrent</th>
                <th>Zandrovi</th>
                <th>imdbID</th>
                <th>Ocjena</th>
                <th>Dodato</th>
                <th style="width: 50px">Opcije</th>
              </tr>
            </tfoot>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>


    </section>
    <!-- /.content -->

@stop


@section('content_js_footer')

<!-- DataTables -->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
  $(function () {
    $('#torrents').DataTable({


      "order": [[ 8, 'asc' ], [ 2, 'desc' ], [ 6, 'desc' ]],
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true,
      "data": [

@foreach($torrents as $torrent)
    [
      "{{ $torrent->id }}",
      "<a target=\"_blank\" href=\"{{ $torrent->source }}}\" metod title=\"Izvor: {{ $torrent->name }}\">{{ $torrent->name }}</a>",
      "{{ $torrent->year }}",
      "<?php


$niz = explode(';', $torrent->data, -1);

foreach ($niz as $movie) {
  $movie = string2KeyedArray($movie);

  echo '<a style=\'float: left; margin: 2px\' target=\'_blank\' href=\''.$movie['torrent_url'].'\'><button type=\'button\' class=\'btn btn-info btn-xs\'>'.$movie['torrent_q'].'</button></a>';
}

?>",
      "{{ $torrent->Genre }}",
      "{{ $torrent->imdbID }}",
      "{{ $torrent->imdbRating }} / 10",
      "{{ $torrent->created_at }}",
      "@if(!$torrent->clanci)<a href=\"../admin/clanci/create?name={{$torrent->name}}&imdbID={{$torrent->imdbID}}\" class=\"btn btn-social-icon\" title=\"Dodaj\"><i class=\"fa fa-share-square-o\"></i></a> @else ima @endif"
    ],
@endforeach   
] 
    });
  });
</script>
@stop