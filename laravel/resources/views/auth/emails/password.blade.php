Poštovani,
<p>
Zatraženo je resetovanje vaše lozinke, ukoliko želite da promjenite lozinku koju ste zaboravili <a href="{{ url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">kliknite ovde!</a>
</p>
Vaša Kinoteka.biz