@extends('admin.templates.login')

@section('title', 'Prijavi se')

@section('content')
<div class="row">

<div class="col-md-4 col-md-offset-4">
<a href="/" class="logo"></a>
</div>

<form role="form" method="POST" action="{{ url('/login') }}">
{!! csrf_field() !!}

<div class="col-md-4 col-md-offset-4">
<div class="login">

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left">
<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail adresa">
<i class="fa fa-envelope-o form-control-feedback"></i>
@if ($errors->has('email'))
<span class="help-block">
{{ $errors->first('email') }}
</span>
@endif
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback has-feedback-left">
<input type="password" class="form-control" name="password" placeholder="Lozinka">
<i class="fa fa-lock form-control-feedback"></i>
@if ($errors->has('password'))
<span class="help-block">
{{ $errors->first('password') }}
</span>
@endif
</div>

<div class="form-group">
<div class="checkbox small">
<label><input type="checkbox" name="remember" checked> Zapamti me</label>
</div>
</div>

</div>
</div>

<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<button type="submit" class="btn btn-sm btn-success pull-right">Prijavi se</button>

<a class="btn btn-sm btn-link" href="{{ url('/password/reset') }}">Zaboravili ste lozinku?</a>
</div>
</div>

</form>

</div>
@endsection
