@extends('admin.templates.login')

@section('title', 'Novu lozinka')

<!-- Main Content -->
@section('content')
<div class="row">

<div class="col-md-4 col-md-offset-4">
<a href="/" class="logo"></a>
</div>

<div class="col-md-4 col-md-offset-4">
<div class="login">

@if (session('status'))
<div class="alert alert-success">
{{ session('status') }}
</div>
@endif

<form role="form" method="POST" action="{{ url('/password/email') }}">
{!! csrf_field() !!}

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left">
<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail adresa">
<i class="fa fa-envelope-o form-control-feedback"></i>
@if ($errors->has('email'))
<span class="help-block">
{{ $errors->first('email') }}
</span>
@endif
</div>

</div>
</div>

<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<button type="submit" class="btn btn-sm btn-success pull-right">Pošalji novu lozinke</button>
<a class="btn btn-sm btn-link" href="/login">Prijavi se</a>
</div>
</div>

</form>

</div>
@endsection
