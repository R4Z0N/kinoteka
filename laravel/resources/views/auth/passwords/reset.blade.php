@extends('admin.templates.login')

@section('title', 'Promenite lozinku')

@section('content')
<div class="row">

<div class="col-md-4 col-md-offset-4">
<a href="/" class="logo"></a>
</div>

<form role="form" method="POST" action="{{ url('/password/reset') }}">
{!! csrf_field() !!}
<input type="hidden" name="token" value="{{ $token }}">

<div class="col-md-4 col-md-offset-4">
<div class="login">
<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
<input type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="E-mail adresa">
@if ($errors->has('email'))
<span class="help-block">
{{ $errors->first('email') }}
</span>
@endif
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
<input type="password" class="form-control" name="password" placeholder="Lozinka">
@if ($errors->has('password'))
<span class="help-block">
{{ $errors->first('password') }}
</span>
@endif
</div>

<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
<input type="password" class="form-control" name="password_confirmation" placeholder="Potvrdite lozinku">
@if ($errors->has('password_confirmation'))
<span class="help-block">
{{ $errors->first('password_confirmation') }}
</span>
@endif
</div>

</div>
</div>

<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<button type="submit" class="btn btn-sm btn-success pull-right">Promeni lozinku</button>
<a class="btn btn-sm btn-link" href="/auth/login">Prijavi se</a>
</div>
</div>

</form>

</div>
@endsection
