@extends('admin.templates.login')

@section('title', 'Registruj se')

@section('content')
<div class="row">

<div class="col-md-4 col-md-offset-4">
<a href="/" class="logo"></a>
</div>

<form role="form" method="POST" action="{{ url('/register') }}">
{!! csrf_field() !!}

<div class="col-md-4 col-md-offset-4">
<div class="login">

<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback has-feedback-left">
<input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Korisničko ime">
<i class="fa fa-user form-control-feedback"></i>
@if ($errors->has('name'))
<span class="help-block">
{{ $errors->first('name') }}
</span>
@endif
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left">
<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail adresa">
<i class="fa fa-envelope-o form-control-feedback"></i>
@if ($errors->has('email'))
<span class="help-block">
{{ $errors->first('email') }}
</span>
@endif
</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback has-feedback-left">
<input type="password" class="form-control" name="password" placeholder="Lozinka">
<i class="fa fa-lock form-control-feedback"></i>
@if ($errors->has('password'))
<span class="help-block">
{{ $errors->first('password') }}
</span>
@endif
</div>

<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} has-feedback has-feedback-left">
<input type="password" class="form-control" name="password_confirmation" placeholder="Potvrdite lozinku">
<i class="fa fa-lock form-control-feedback"></i>
@if ($errors->has('password_confirmation'))
<span class="help-block">
{{ $errors->first('password_confirmation') }}
</span>
@endif
</div>

</div>
</div>

<div class="col-md-4 col-md-offset-4">
<div class="form-group">
<button type="submit" class="btn btn-sm btn-success pull-right">Registruj se</button>
</div>
</div>
</form>

</div>
@endsection
