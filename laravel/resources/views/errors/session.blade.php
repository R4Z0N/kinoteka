@if(Session::has('message'))

    <div class="alert alert-info" style="margin: 0px 15px 0px 15px; padding: 10px;">

        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        {!! Session::get('message') !!}

    </div>

@endif



@if(Session::has('dodaj'))

    <div class="alert alert-success" style="margin: 0px 15px 0px 15px; padding: 10px;">

        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        {!! Session::get('dodaj') !!}

    </div>

@endif



@if(Session::has('izbrisi'))

    <div class="alert alert-danger" style="margin: 0px 15px 0px 15px; padding: 10px;">

        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        {{ Session::get('izbrisi') }}

    </div>

@endif

