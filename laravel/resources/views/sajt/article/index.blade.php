<div class="container sezone">

		@for ($i=1; $i<=$article->seasons; $i++)
			<a href="/filmovi-i-serije-sa-prevodom/{{ $article->slug }}/s{{ sprintf("%'.02d", $i) }}/e01" class="box {{ ($article->action === 'series' && $se== $i) ? 'active-pozadina' : '' }} left kocka">
				@php
					if ($article->action === 'series' && $se == $i) 
						echo '<i class="fa fa-play" aria-hidden="true"></i>&nbsp; Sezona ' . $i;
					else
						echo 'S' . sprintf("%'.02d", $i);
				@endphp 
			</a>
		@endfor		
		
		@if ($article->trailer !== '')
			<a href="/filmovi-i-serije-sa-prevodom/{{ $article->slug }}" class="box left trejler-kocka {{ ($article->action === 'trailer') ? 'active-pozadina' : '' }}">
				Trejler
			</a>
		@endif
</div>

	<div class="container">
		<div class="col-lg-9 frejm">
			<iframe id="link-place" src="{{ $article->action === 'trailer' ? $article->trailer : $episode->stream }}" scrolling="no" frameborder="0" width="100%" height="480px" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>
		</div>
	<div class="lista col-lg-3">

		@if ($article->action === 'series')

			<div class="list-group lista-epizoda">
				@foreach ($season as $sezona)
					<a id="{{ $sezona->episode }}" href="/filmovi-i-serije-sa-prevodom/{{ $article->slug }}/s{{ sprintf("%'.02d", $sezona->season) }}/e{{ sprintf("%'.02d", $sezona->episode) }}" class="list-group-item {{ ($ep == $sezona->episode) ? 'active' : '' }}"> 
						
						@if ($ep == $sezona->episode)
							<h2 class="epizoda-naslov">
								<i class="fa fa-play" aria-hidden="true"></i> &nbsp; {{ $sezona->name }}
							</h2>
						@else
							<h2 class="epizoda-naslov">{{ $sezona->episode . ". " . $sezona->name }}</h2>
						@endif

					</a>
				@endforeach
			</div>

		@else

			<div>
				<p class="trenutno-trejler">
					Trenutno je odabran trejler.
					Da biste pustili epizodu prvo odaberite jednu od ponudjenih sezona.
					Nakon toga ovde ce vam se pojaviti epizode.
				</p>
			</div>

		@endif
		
	</div>
</div>