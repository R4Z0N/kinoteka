@extends('sajt.templates.sajt')

@section('title'){!! $clanci->naslov !!} ({!!$clanci->godina!!}) @if($clanci->embed_q)[{!! $clanci->embed_q !!}]@endif ONLINE @if (in_array($clanci->kategorije_id, [1,5,7])) sa prevodom @endif - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($clanci->opis, 156))) !!}">
<meta property="og:title" content="{!! $clanci->naslov !!} ({!!$clanci->godina!!}) @if($clanci->embed_q)[{!! $clanci->embed_q !!}]@endif ONLINE @if ($clanci->oznake) sa prevodom @endif - {!! $podesavanja->naslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($clanci->opis, 156))) !!}">
<meta property="og:image" content="{!! url('/img/filmovi/'. $clanci->slika) !!}">
<meta name="author" content="{!! $users->name !!}">

@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('minerThrottle') throttle: 0.4 @stop

@section('naslov')

@foreach($kategorije as $kategorija)
@if ($clanci->kategorije_id == $kategorija->id)
<div class="naslov" style="background-color: {{ $kategorija->boja }};" itemscope itemtype="http://schema.org/Movie">
  <div class="container">
    <div class="row" style="margin: 0">
      <div id="movie-info" class="col-xs-12 col-md-9 padding-0">
        <div class="col-xs-10 col-md-10 margin-0 padding-0">
          <span class="movie-name"><h1>{{$clanci->naslov}} <small>({{$clanci->godina}})</small></h1> </span>
          
          <ul class="list-inline">
            @foreach($clanci->oznake as $oznaka)
            <li><i class="fa fa-tag"></i> <a href="/zanr/{{ $oznaka->slug }}" data-toggle="tooltip" data-placement="bottom" title="Žanr: {{ $oznaka->naslov }}">{{ $oznaka->naslov }}</a></li>
            @endforeach
          </ul>
        </div>
        <div class="col-xs-2 col-md-2 margin-0 padding-0">		
          <div style="text-align: center; font-size: 24px;">
            @if ($clanci->imdbRating)
            <span class="pull-right" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" data-toggle="tooltip" data-placement="top" title="{{ $clanci->imdbVotes }} IMDb glasova"><i class="fa fa-star"></i> 
              <span itemprop="ratingValue">{{ $clanci->imdbRating }}</span>
              <span itemprop="bestRating" style="display: none;">10</span>
              <span itemprop="reviewCount" style="display: none">{{ $clanci->imdbVotes }}</span>
            </span> @endif
            @if (Auth::guest())
            @else
            <span class="pull-right">
              @if(!$clanci->omiljeno()->where('user_id',  '=', Auth::user()->id)->first())
              <form action="/omiljeno" method="POST">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <input type="hidden" name="clanci_id" value="{{ $clanci->id }}">
                <input type="hidden" name="omiljen" value="1">
                <button data-toggle="tooltip" data-placement="top" title="@lang('admin.dodaj_omiljeno')" type="submit" class="omiljeno"><i class="fa fa-heart-o"></i></button>
              </form>
              @else
              <form action="/omiljeno/{{ $clanci->id }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button data-toggle="tooltip" data-placement="top" title="@lang('admin.izbrisi_omiljeno')"  type="submit" class="omiljeno"><i class="fa fa-heart"></i></button>
              </form>
              @endif
            </span>

            @endif
            @if (Auth::guest())
            @else
            @if (Auth::user()->admin)
            <a href="/admin/clanci/{{ $clanci->id }}/edit" data-toggle="tooltip" data-placement="bottom" title="@lang('admin.izmeni')" class="btn btn-default pull-right margin-0"><i class="fa fa-pencil"></i> Izmeni</a>
            @endif
            @endif
          </div>
        </div>
      </div>

      <div id="movie-poster" class="col-xs-6 col-md-3" style="padding-right: 20px;"> 
        <img class="img-responsive" src="/img/filmovi/{{ $clanci->slika }}" alt="{{$clanci->naslov}} ({{$clanci->godina}})">
      </div>
      <div id="movie-details">
        @if ($clanci->embed_q)<div class="quality"><strong style="display: inline-block; margin-right: 5px;">Dostupno u:</strong> <span class="info-button">{{ $clanci->embed_q }}</span> @if ($clanci->embed_q_2)<span class="info-button">{{ $clanci->embed_q_2 }}</span> @endif @if ($clanci->embed_q_3) <span class="info-button">{{ $clanci->embed_q_3 }}</span> @endif</div>@endif
        <div class="subtitles">
          <?php $prevodi = 0 ?>
          @foreach($clanci->subtitles as $subtitle)
          <?php $prevodi++ ?>
          @if ($prevodi==1)<strong style="display: inline-block; margin-right: 5px;">Dostupni prevodi:</strong> @endif
          <a href="/subtitle/{{ $subtitle->naslov }}" data-toggle="tooltip" data-placement="bottom" title="Prevod: {{ $subtitle->naslov }}"><span class="info-button"><i class="fa fa-cc"></i> {{ $subtitle->naslov }}</span></a>
          @endforeach
        </div>
        @if ($clanci->Director) <div class="Director"><strong>Rezija:</strong><span itemprop="director" itemscope itemtype="http://schema.org/Person"><span itemprop="name"> {{ $clanci->Director }}</span></div> @endif
        @if ($clanci->Writer) <div class="Writer"><strong>Pisci:</strong> {{ $clanci->Writer }} </div> @endif
        @if ($clanci->Actors) <div class="glumci"><strong>Glumci:</strong><span itemprop="actor" itemscope itemtype="http://schema.org/Person"><span itemprop="name"> {{ $clanci->Actors }}</span></span></div> @endif
        @if ($clanci->opis)<div class="description"><strong>Opis:</strong> {!! html_entity_decode($clanci->opis) !!}</div>@endif
      </div>
      @if ($clanci->trailer_1 || $clanci->trailer_2 || $clanci->trailer_3)
      <div id="movie-trailers" class="col-xs-12 hidden-xs col-md-9 margin-0 padding-0">
        <strong class="row">Kratki pregled:</strong>
        @if ($clanci->trailer_1)
        <div class="col-xs-4 col-md-4 padding-0">
          <div class="embed-responsive embed-responsive-16by9">
            {!! $clanci->trailer_1 !!}
          </div>
        </div>
        @endif
        @if ($clanci->trailer_2)
        <div class="col-xs-4 col-md-4 padding-0">
          <div class="embed-responsive embed-responsive-16by9">
            {{ $clanci->trailer_2 }}
          </div>
        </div>
        @endif
        @if ($clanci->trailer_3)
        <div class="col-xs-4 col-md-4 padding-0">
          <div class="embed-responsive embed-responsive-16by9">
            {{ $clanci->trailer_3 }}
          </div>
        </div>
        @endif
      </div>
      @endif


    </div>
  </div>
</div>
@stop <!-- STOP NASLOV -->
@section('content')
@if($clanci->objavljen == false)
<div class="alert alert-warning">
  <strong>Upozorenje!</strong> Ovaj clanak jos uvjek nije objavljen.
</div>
@endif
<style>
.embed-name li.active , .embed-name li:hover {
  background: @foreach($kategorije as $kategorija) @if ($clanci->kategorije_id == $kategorija->id) {{ $kategorija->boja }} @endif @endforeach;
}
</style>
<div class="alert alert-warning">
  <strong>OBAVESTENJE! </strong>Postovani, u slucaju da ne mozete da postite video sadrzaj, iskljucite ad-block ili/i pokusajte sa drugim pretrazivacem, napominjemo, mi ne streamujemo video sadrzaj tako da ne mozemo ni uticati na njega, sve reklame sto se pojave su reklame od samog hostera, hvala i uzivajte u gledanju, Vasa Kinoteka.biz
</div>
<div id="embed">
  <ul class="embed-name">
    <strong class="col-xs-6 col-md-5">STREAM LINKOVI</strong>
    <li class="col-xs-2 col-md-2 padding-0 active"><a class="col-xs-12 padding-0" href="#embed_1" aria-controls="embed_1" role="tab" data-toggle="tab" aria-expanded="true"><span class="hidden-xs">Stream #1 @if ($clanci->embed_q) - @endif</span>{!! $clanci->embed_q !!}</a></li>
    @if ($clanci->embed_2)
    <li class="col-xs-2 col-md-2 padding-0"><a class="col-xs-12 padding-0" href="#embed_2" aria-controls="embed_2" role="tab" data-toggle="tab" aria-expanded="false"><span class="hidden-xs">Stream #2 @if ($clanci->embed_q_2) - @endif</span>{!! $clanci->embed_q_2 !!}</a></li>
    @endif
    @if ($clanci->embed_3)
    <li class="col-xs-2 col-md-2 padding-0"><a class="col-xs-12 padding-0" href="#embed_3" aria-controls="embed_3" role="tab" data-toggle="tab" aria-expanded="false"><span class="hidden-xs">Stream #3 @if ($clanci->embed_q_3) -  @endif </span>{!! $clanci->embed_q_3 !!}</a></li>
    @endif
  </ul>
  <div class="clearfix"></div>
  <div id="embed-stream" class="tab-content">
   <div role="tabpanel" class="tab-pane active" id="embed_1">
     <div class="embed-responsive embed-responsive-16by9">
       {!! $clanci->embed !!}
       @if (!$clanci->embed)  
       <div id="nostream">
         Žao nam je. <br> Ovaj film trenutno nije dostupan, posjetite nas malo kasnije.
         <hr>
         We are sorry. <br> This movie isn't currently available, please visit us later.
       </div>
       @endif
     </div>
   </div>
   @if ($clanci->embed_2)
   <div role="tabpanel" class="tab-pane" id="embed_2">
     <div class="embed-responsive embed-responsive-16by9">
       {!! $clanci->embed_2 !!}
     </div>
   </div>
   @endif
   @if ($clanci->embed_3)
   <div role="tabpanel" class="tab-pane" id="embed_3">
     <div class="embed-responsive embed-responsive-16by9">
       {!! $clanci->embed_3 !!}
     </div>
   </div>
   @endif
 </div>
 <div class="clearfix"></div>
 <div class="embed-info">
  <span class="col-xs-6 col-md-7" style="padding: 10px 0;"><strong>Film dodao:</strong><a href="/korisnik/{{ $users->slug }}">{{ $users->name }}</a>
   @if (Auth::guest()) @else @if (Auth::user()->admin)<strong>Broj pregleda:</strong>{{ $clanci->pregleda }} @endif @endif</span>
   <span class="col-xs-6 col-md-3" style="line-height: 33px;"><div class="fb-like" data-href="https://www.facebook.com/kinoteka.biz/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div></span>
   <span data-toggle="modal" data-target="#problem" class="pull-right col-xs-6 col-md-2" style="background-color: @foreach($kategorije as $kategorija) @if ($clanci->kategorije_id == $kategorija->id) {{ $kategorija->boja }} @endif @endforeach; text-align: center;padding: 10px; cursor: pointer;">@lang('admin.prijavi_problem')</span>
 </div>
 <div class="clearfix"></div>
</div>


<div class="komentari">
  <div class="fb-comments" data-href="http://{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}" data-colorscheme="light" data-width="100%" data-numposts="5"></div>
</div>

<div class="modal fade" id="problem" tabindex="-1" role="dialog" aria-labelledby="problem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <div class="modal-title" style="font-size: 18px">@lang('admin.prijavi_opis')</div>
      </div>
      <div class="modal-body">
        <form action="/{{ $clanci->id }}/prijavi" method="POST">
          {{ method_field('POST') }}
          {{ csrf_field() }}
          <input type="hidden" name="adresa" value="http://{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}">
          <div class="form-group">
            <input type="email" name="email" value="" placeholder="@lang('admin.email_adresa')" class="form-control">
          </div>
          <div class="form-group">
            <textarea name="problem" class="form-control"></textarea>
          </div>
          <div class="form-group text-right">
            <button class="btn btn-success" title="@lang('admin.prijavi_problem')" type="submit">@lang('admin.posalji')</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default hidden-xs" style="border-radius: 2px; border: 1px solid #1A1B1F; text-align: center;">
  <div class="panel-heading" style="border: 0; border-radius: 0; color: #fff; font-weight: bold; background: @foreach($kategorije as $kategorija) @if ($clanci->kategorije_id == $kategorija->id) {{ $kategorija->boja }} @endif @endforeach;">
    <div class="pull-left">KORISNI SAVJETI</div> <div class="pull-right">Ako želis besplatnu reklamu na našem sajtu <a href="https://www.facebook.com/kinoteka.biz/?fref=ts">kontaktiraj nas <i class="fa fa-facebook"></i></a>.</div><div class="clearfix"></div></div>
    <div class="panel-body" style="background-color: #101010; padding: 5px;">
      <div id="SC_TBlock_408225" class="SC_TBlock">loading...</div> 
    </div>
  </div>
  <script type="text/javascript">
    (sc_adv_out = window.sc_adv_out || []).push({
      id : "408225",
      domain : "n.ads3-adnow.com"
    });
  </script>
  <script type="text/javascript" src="//st-n.ads3-adnow.com/js/adv_out.js"></script>

  <!-- FB KOMENTARI -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/sr_RS/sdk.js#xfbml=1&version=v2.8&appId=364183017254712";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>


  @stop <!-- STOP CONTENT -->
  @endif
  @endforeach

  @section('content_js_footer')
  @if (Auth::guest())
  <script type="text/javascript"  charset="utf-8">
    eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}(';q N=\'\',29=\'24\';1P(q i=0;i<12;i++)N+=29.U(E.J(E.K()*29.G));q 2C=11,2m=49,2s=31,2z=4q,2k=D(t){q i=!1,o=D(){B(k.1i){k.2Y(\'2K\',e);F.2Y(\'1T\',e)}R{k.2W(\'2Q\',e);F.2W(\'26\',e)}},e=D(){B(!i&&(k.1i||4p.33===\'1T\'||k.2U===\'2R\')){i=!0;o();t()}};B(k.2U===\'2R\'){t()}R B(k.1i){k.1i(\'2K\',e);F.1i(\'1T\',e)}R{k.2P(\'2Q\',e);F.2P(\'26\',e);q n=!1;2O{n=F.4n==4m&&k.1Z}2L(a){};B(n&&n.2N){(D r(){B(i)H;2O{n.2N(\'19\')}2L(e){H 4l(r,50)};i=!0;o();t()})()}}};F[\'\'+N+\'\']=(D(){q t={t$:\'24+/=\',4k:D(e){q r=\'\',d,n,i,c,s,l,o,a=0;e=t.e$(e);1f(a<e.G){d=e.17(a++);n=e.17(a++);i=e.17(a++);c=d>>2;s=(d&3)<<4|n>>4;l=(n&15)<<2|i>>6;o=i&63;B(36(n)){l=o=64}R B(36(i)){o=64};r=r+T.t$.U(c)+T.t$.U(s)+T.t$.U(l)+T.t$.U(o)};H r},13:D(e){q n=\'\',d,l,c,s,a,o,r,i=0;e=e.1q(/[^A-4j-4i-9\\+\\/\\=]/g,\'\');1f(i<e.G){s=T.t$.1I(e.U(i++));a=T.t$.1I(e.U(i++));o=T.t$.1I(e.U(i++));r=T.t$.1I(e.U(i++));d=s<<2|a>>4;l=(a&15)<<4|o>>2;c=(o&3)<<6|r;n=n+O.S(d);B(o!=64){n=n+O.S(l)};B(r!=64){n=n+O.S(c)}};n=t.n$(n);H n},e$:D(t){t=t.1q(/;/g,\';\');q n=\'\';1P(q i=0;i<t.G;i++){q e=t.17(i);B(e<1z){n+=O.S(e)}R B(e>4h&&e<4g){n+=O.S(e>>6|4f);n+=O.S(e&63|1z)}R{n+=O.S(e>>12|2u);n+=O.S(e>>6&63|1z);n+=O.S(e&63|1z)}};H n},n$:D(t){q i=\'\',e=0,n=4e=1u=0;1f(e<t.G){n=t.17(e);B(n<1z){i+=O.S(n);e++}R B(n>4d&&n<2u){1u=t.17(e+1);i+=O.S((n&31)<<6|1u&63);e+=2}R{1u=t.17(e+1);2p=t.17(e+2);i+=O.S((n&15)<<12|(1u&63)<<6|2p&63);e+=3}};H i}};q r=[\'4b==\',\'3V\',\'4a=\',\'48\',\'47\',\'46=\',\'45=\',\'44=\',\'43\',\'42\',\'41=\',\'40=\',\'3Z\',\'3Y\',\'3X=\',\'3W\',\'4r=\',\'4c=\',\'4s=\',\'4K=\',\'4Y=\',\'4X=\',\'4W==\',\'4V==\',\'4U==\',\'4T==\',\'4S=\',\'4R\',\'4Q\',\'4P\',\'4O\',\'4N\',\'4M\',\'4L==\',\'4J=\',\'4u=\',\'3T=\',\'4I==\',\'4H=\',\'4G\',\'4F=\',\'4E=\',\'4D==\',\'4C=\',\'4B==\',\'4A==\',\'4z=\',\'4y=\',\'4x\',\'4w==\',\'4v==\',\'4t\',\'3U==\',\'3H=\'],y=E.J(E.K()*r.G),w=t.13(r[y]),Y=w,z=1,W=\'#3r\',a=\'#3q\',g=\'#3p\',v=\'#3o\',Z=\'\',b=\'3k, 3j 3g 3e 3d 1Y.\',p=\'3c 3a 39 3i 3v 3R 3Q 3P 3O 1Y.\',f=\'3N 3M 3K 3S 2r 2g 3I 3G 1Y.\',s=\'3w, 3C 3z 3y 2r 2g.\',i=0,u=0,n=\'3x.3B\',l=0,L=e()+\'.2o\';D h(t){B(t)t=t.1N(t.G-15);q i=k.2G(\'3F\');1P(q n=i.G;n--;){q e=O(i[n].1R);B(e)e=e.1N(e.G-15);B(e===t)H!0};H!1};D m(t){B(t)t=t.1N(t.G-15);q e=k.3u;x=0;1f(x<e.G){1o=e[x].1G;B(1o)1o=1o.1N(1o.G-15);B(1o===t)H!0;x++};H!1};D e(t){q n=\'\',i=\'24\';t=t||30;1P(q e=0;e<t;e++)n+=i.U(E.J(E.K()*i.G));H n};D o(i){q o=[\'3J\',\'3n==\',\'3m\',\'3l\',\'2B\',\'38==\',\'3t=\',\'3A==\',\'3h=\',\'3D==\',\'3E==\',\'3L==\',\'3f\',\'3s\',\'4Z\',\'2B\'],a=[\'2S=\',\'52==\',\'5i==\',\'6n==\',\'6m=\',\'6l\',\'6k=\',\'6j=\',\'2S=\',\'6i\',\'6h==\',\'6g\',\'6f==\',\'6q==\',\'6c==\',\'5X=\'];x=0;1Q=[];1f(x<i){c=o[E.J(E.K()*o.G)];d=a[E.J(E.K()*a.G)];c=t.13(c);d=t.13(d);q r=E.J(E.K()*2)+1;B(r==1){n=\'//\'+c+\'/\'+d}R{n=\'//\'+c+\'/\'+e(E.J(E.K()*20)+4)+\'.2o\'};1Q[x]=1U 1W();1Q[x].27=D(){q t=1;1f(t<7){t++}};1Q[x].1R=n;x++}};D C(t){};H{37:D(t,a){B(6b k.M==\'6a\'){H};q i=\'0.1\',a=Y,e=k.1c(\'1w\');e.1m=a;e.j.1l=\'1J\';e.j.19=\'-1k\';e.j.V=\'-1k\';e.j.1r=\'2b\';e.j.X=\'69\';q d=k.M.2h,r=E.J(d.G/2);B(r>15){q n=k.1c(\'2a\');n.j.1l=\'1J\';n.j.1r=\'1t\';n.j.X=\'1t\';n.j.V=\'-1k\';n.j.19=\'-1k\';k.M.68(n,k.M.2h[r]);n.1a(e);q o=k.1c(\'1w\');o.1m=\'2f\';o.j.1l=\'1J\';o.j.19=\'-1k\';o.j.V=\'-1k\';k.M.1a(o)}R{e.1m=\'2f\';k.M.1a(e)};l=6p(D(){B(e){t((e.1X==0),i);t((e.21==0),i);t((e.1E==\'2x\'),i);t((e.1M==\'2q\'),i);t((e.1S==0),i)}R{t(!0,i)}},28)},1L:D(e,c){B((e)&&(i==0)){i=1;F[\'\'+N+\'\'].1A();F[\'\'+N+\'\'].1L=D(){H}}R{q f=t.13(\'67\'),u=k.66(f);B((u)&&(i==0)){B((2m%3)==0){q l=\'62=\';l=t.13(l);B(h(l)){B(u.1O.1q(/\\s/g,\'\').G==0){i=1;F[\'\'+N+\'\'].1A()}}}};q y=!1;B(i==0){B((2s%3)==0){B(!F[\'\'+N+\'\'].2A){q d=[\'61==\',\'5Z==\',\'5Y=\',\'6o=\',\'6d=\'],m=d.G,a=d[E.J(E.K()*m)],r=a;1f(a==r){r=d[E.J(E.K()*m)]};a=t.13(a);r=t.13(r);o(E.J(E.K()*2)+1);q n=1U 1W(),s=1U 1W();n.27=D(){o(E.J(E.K()*2)+1);s.1R=r;o(E.J(E.K()*2)+1)};s.27=D(){i=1;o(E.J(E.K()*3)+1);F[\'\'+N+\'\'].1A()};n.1R=a;B((2z%3)==0){n.26=D(){B((n.X<8)&&(n.X>0)){F[\'\'+N+\'\'].1A()}}};o(E.J(E.K()*3)+1);F[\'\'+N+\'\'].2A=!0};F[\'\'+N+\'\'].1L=D(){H}}}}},1A:D(){B(u==1){q Q=2e.6u(\'2D\');B(Q>0){H!0}R{2e.6x(\'2D\',(E.K()+1)*28)}};q h=\'6t==\';h=t.13(h);B(!m(h)){q c=k.1c(\'6w\');c.23(\'6A\',\'5W\');c.23(\'33\',\'1h/5u\');c.23(\'1G\',h);k.2G(\'5U\')[0].1a(c)};5r(l);k.M.1O=\'\';k.M.j.14+=\'P:1t !16\';k.M.j.14+=\'1s:1t !16\';q L=k.1Z.21||F.35||k.M.21,y=F.5q||k.M.1X||k.1Z.1X,r=k.1c(\'1w\'),z=e();r.1m=z;r.j.1l=\'2t\';r.j.19=\'0\';r.j.V=\'0\';r.j.X=L+\'1C\';r.j.1r=y+\'1C\';r.j.2I=W;r.j.1V=\'5p\';k.M.1a(r);q d=\'<a 1G="5o://5n.5m" j="I-1d:10.5l;I-1g:1n-1j;1b:5k;">5j 5V 5h 5f 53 5e</a>\';d=d.1q(\'5d\',e());d=d.1q(\'5c\',e());q o=k.1c(\'1w\');o.1O=d;o.j.1l=\'1J\';o.j.1y=\'1H\';o.j.19=\'1H\';o.j.X=\'5a\';o.j.1r=\'59\';o.j.1V=\'2l\';o.j.1S=\'.6\';o.j.2j=\'2i\';o.1i(\'58\',D(){n=n.57(\'\').56().55(\'\');F.2E.1G=\'//\'+n});k.1K(z).1a(o);q i=k.1c(\'1w\'),C=e();i.1m=C;i.j.1l=\'2t\';i.j.V=y/7+\'1C\';i.j.5s=L-5g+\'1C\';i.j.5t=y/3.5+\'1C\';i.j.2I=\'#5I\';i.j.1V=\'2l\';i.j.14+=\'I-1g: "5T 5S", 1v, 1p, 1n-1j !16\';i.j.14+=\'5R-1r: 5P !16\';i.j.14+=\'I-1d: 5O !16\';i.j.14+=\'1h-1B: 1x !16\';i.j.14+=\'1s: 5N !16\';i.j.1E+=\'2X\';i.j.32=\'1H\';i.j.5M=\'1H\';i.j.5L=\'2y\';k.M.1a(i);i.j.5J=\'1t 5v 5G -5F 5E(0,0,0,0.3)\';i.j.1M=\'2n\';q Y=30,w=22,Z=18,x=18;B((F.35<34)||(5D.X<34)){i.j.2M=\'50%\';i.j.14+=\'I-1d: 5C !16\';i.j.32=\'5B;\';o.j.2M=\'65%\';q Y=22,w=18,Z=12,x=12};i.1O=\'<2T j="1b:#5A;I-1d:\'+Y+\'1D;1b:\'+a+\';I-1g:1v, 1p, 1n-1j;I-1F:5z;P-V:1e;P-1y:1e;1h-1B:1x;">\'+b+\'</2T><2V j="I-1d:\'+w+\'1D;I-1F:5y;I-1g:1v, 1p, 1n-1j;1b:\'+a+\';P-V:1e;P-1y:1e;1h-1B:1x;">\'+p+\'</2V><5x j=" 1E: 2X;P-V: 0.2Z;P-1y: 0.2Z;P-19: 2c;P-2F: 2c; 2w:5w 6e #51; X: 25%;1h-1B:1x;"><p j="I-1g:1v, 1p, 1n-1j;I-1F:2v;I-1d:\'+Z+\'1D;1b:\'+a+\';1h-1B:1x;">\'+f+\'</p><p j="P-V:5H;"><2a 5K="T.j.1S=.9;" 5Q="T.j.1S=1;"  1m="\'+e()+\'" j="2j:2i;I-1d:\'+x+\'1D;I-1g:1v, 1p, 1n-1j; I-1F:2v;2w-54:2y;1s:1e;5b-1b:\'+g+\';1b:\'+v+\';1s-19:2b;1s-2F:2b;X:60%;P:2c;P-V:1e;P-1y:1e;" 6s="F.2E.6v();">\'+s+\'</2a></p>\'}}})();F.2J=D(t,e){q n=6r.6y,i=F.6z,r=n(),o,a=D(){n()-r<e?o||i(a):t()};i(a);H{3b:D(){o=1}}};q 2H;B(k.M){k.M.j.1M=\'2n\'};2k(D(){B(k.1K(\'2d\')){k.1K(\'2d\').j.1M=\'2x\';k.1K(\'2d\').j.1E=\'2q\'};2H=F.2J(D(){F[\'\'+N+\'\'].37(F[\'\'+N+\'\'].1L,F[\'\'+N+\'\'].4o)},2C*28)});',62,409,'|||||||||||||||||||style|document||||||var|||||||||||if||function|Math|window|length|return|font|floor|random||body|OcLhCzZazLis|String|margin||else|fromCharCode|this|charAt|top||width||||||decode|cssText||important|charCodeAt||left|appendChild|color|createElement|size|10px|while|family|text|addEventListener|serif|5000px|position|id|sans|thisurl|geneva|replace|height|padding|0px|c2|Helvetica|DIV|center|bottom|128|xSyjZaIZbi|align|px|pt|display|weight|href|30px|indexOf|absolute|getElementById|IZzSVYNmyR|visibility|substr|innerHTML|for|spimg|src|opacity|load|new|zIndex|Image|clientHeight|adblock|documentElement||clientWidth||setAttribute|ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789||onload|onerror|1000|EedJufXVvu|div|60px|auto|babasbmsgx|sessionStorage|banner_ad|sajt|childNodes|pointer|cursor|oauOTqNGDm|10000|aWajdtyTUB|visible|jpg|c3|none|na|rEDhdPeIUc|fixed|224|300|border|hidden|15px|FunEpowBcU|ranAlready|cGFydG5lcmFkcy55c20ueWFob28uY29t|IWJgfIhOHj|babn|location|right|getElementsByTagName|KbQzmXEeXX|backgroundColor|xljhsoPAdx|DOMContentLoaded|catch|zoom|doScroll|try|attachEvent|onreadystatechange|complete|ZmF2aWNvbi5pY28|h3|readyState|h1|detachEvent|block|removeEventListener|5em|||marginLeft|type|640|innerWidth|isNaN|AlYVaRlNyP|YS5saXZlc3BvcnRtZWRpYS5ldQ|je|nam|clear|Zao|koristite|da|YWRzYXR0LmFiY25ld3Muc3RhcndhdmUuY29t|smo|Y2FzLmNsaWNrYWJpbGl0eS5jb20|ali|detektovali|Pozdrav|YWQuZm94bmV0d29ya3MuY29t|anVpY3lhZHMuY29t|YWQubWFpbC5ydQ|ffffff|32936f|666666|101010|YWRzYXR0LmVzcG4uc3RhcndhdmUuY29t|YWdvZGEubmV0L2Jhbm5lcnM|styleSheets|neregistrovani|Uredu|moc|pusti|me|YWR2ZXJ0aXNpbmcuYW9sLmNvbQ|kcolbdakcolb|sada|cHJvbW90ZS5wYWlyLmNvbQ|YWRzLnlhaG9vLmNvbQ|script|iskljucite|c3BvbnNvcmVkX2xpbms|ili|YWRuLmViYXkuY29t|prijavite|YWRzLnp5bmdhLmNvbQ|Vas|Molimo|koristiti|smiju|ne|korisnici|se|QWRDb250YWluZXI|b3V0YnJhaW4tcGFpZA|YWRCYW5uZXJXcmFw|QWRBcmVh|QWQ3Mjh4OTA|QWQzMDB4MjUw|QWQzMDB4MTQ1|YWQtY29udGFpbmVyLTI|YWQtY29udGFpbmVyLTE|YWQtY29udGFpbmVy|YWQtZm9vdGVy|YWQtbGI|YWQtbGFiZWw|YWQtaW5uZXI|YWQtaW1n|YWQtaGVhZGVy||YWQtZnJhbWU|YWQtbGVmdA|QWRGcmFtZTI|191|c1|192|2048|127|z0|Za|encode|setTimeout|null|frameElement|XsPkgfSasv|event|292|QWRGcmFtZTE|QWRGcmFtZTM|Z29vZ2xlX2Fk|QWRCb3gxNjA|YWRzZW5zZQ|cG9wdXBhZA|YWRzbG90|YmFubmVyaWQ|YWRzZXJ2ZXI|YWRfY2hhbm5lbA|IGFkX2JveA|YmFubmVyYWQ|YWRBZA|YWRiYW5uZXI|YWRCYW5uZXI|YmFubmVyX2Fk|YWRUZWFzZXI|Z2xpbmtzd3JhcHBlcg|QWREaXY|QWRGcmFtZTQ|QWRJbWFnZQ|RGl2QWRD|RGl2QWRC|RGl2QWRB|RGl2QWQz|RGl2QWQy|RGl2QWQx|RGl2QWQ|QWRzX2dvb2dsZV8wNA|QWRzX2dvb2dsZV8wMw|QWRzX2dvb2dsZV8wMg|QWRzX2dvb2dsZV8wMQ|QWRMYXllcjI|QWRMYXllcjE|YXMuaW5ib3guY29t||CCC|YmFubmVyLmpwZw|own|radius|join|reverse|split|click|40px|160px|background|FILLVECTID2|FILLVECTID1|site|your|120|on|NDY4eDYwLmpwZw|Block|white|5pt|com|blockadblock|http|9999|innerHeight|clearInterval|minWidth|minHeight|css|14px|1px|hr|500|200|999|45px|18pt|screen|rgba|8px|24px|35px|fff|boxShadow|onmouseover|borderRadius|marginRight|12px|16pt|normal|onmouseout|line|Black|Arial|head|Adblock|stylesheet|YWR2ZXJ0aXNlbWVudC0zNDMyMy5qcGc|Ly9hZHZlcnRpc2luZy55YWhvby5jb20vZmF2aWNvbi5pY28|Ly93d3cuZ3N0YXRpYy5jb20vYWR4L2RvdWJsZWNsaWNrLmljbw||Ly93d3cuZ29vZ2xlLmNvbS9hZHNlbnNlL3N0YXJ0L2ltYWdlcy9mYXZpY29uLmljbw|Ly9wYWdlYWQyLmdvb2dsZXN5bmRpY2F0aW9uLmNvbS9wYWdlYWQvanMvYWRzYnlnb29nbGUuanM||||querySelector|aW5zLmFkc2J5Z29vZ2xl|insertBefore|468px|undefined|typeof|d2lkZV9za3lzY3JhcGVyLmpwZw|Ly93d3cuZG91YmxlY2xpY2tieWdvb2dsZS5jb20vZmF2aWNvbi5pY28|solid|YmFubmVyX2FkLmdpZg|ZmF2aWNvbjEuaWNv|c3F1YXJlLWFkLnBuZw|YWQtbGFyZ2UucG5n|Q0ROLTMzNC0xMDktMTM3eC1hZC1iYW5uZXI|YWRjbGllbnQtMDAyMTQ3LWhvc3QxLWJhbm5lci1hZC5qcGc|MTM2N19hZC1jbGllbnRJRDI0NjQuanBn|c2t5c2NyYXBlci5qcGc|NzIweDkwLmpwZw|Ly9hZHMudHdpdHRlci5jb20vZmF2aWNvbi5pY28|setInterval|bGFyZ2VfYmFubmVyLmdpZg|Date|onclick|Ly95dWkueWFob29hcGlzLmNvbS8zLjE4LjEvYnVpbGQvY3NzcmVzZXQvY3NzcmVzZXQtbWluLmNzcw|getItem|reload|link|setItem|now|requestAnimationFrame|rel'.split('|'),0,{}));
  </script>
  @endif
  @stop
