@extends('sajt.templates.sajt')

@section('title')@lang('admin.dodajte_film') - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! str_limit(trans('admin.dodajte_film_opis'), 156) !!}">
<meta property="og:title" content="@lang('admin.dodajte_film') - {!! $podesavanja->podnaslov !!}">
<meta property="og:description" content="{!! str_limit(trans('admin.dodajte_film_opis'), 156) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov">
    <div class="container">
        <h1>@lang('admin.dodajte_film')</h1>
        <p>@lang('admin.dodajte_film_opis')</p>
    </div>
</div>
@stop

@section('content')
<form action="/dodaj" method="POST">
{{ csrf_field() }}
{{ method_field('POST') }}
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label>Naziv</label>
            <input type="text" name="naslov" id="naslov" class="form-control korisnik-input" placeholder="@lang('admin.naslov')">
            <input type="text" name="imdb" id="imdb" style="display: none!important;" class="form-control">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label>Kategorija</label>
            <select name="kategorije_id" class="form-control korisnik-input">
                @foreach($kategorije as $kategorija)
                <option value="{{ $kategorija->id }}">{{ $kategorija->naslov }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-group">
    <label>Opis</label>
    <textarea name="opis" class="form-control korisnik-input"></textarea>
</div>
<div class="form-group">
    <label>Embed kod:</label>
    <input type="text" name="embed" class="form-control korisnik-input" placeholder="@lang('admin.embed')">
</div>
<div class="form-group pull-right">
    <button class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="@lang('admin.dodaj')" type="submit">@lang('admin.dodaj')</button>
</div>
</form>

@stop


@section('content_js_footer_top')
<link rel="stylesheet"          href="/css/jquery-ui.css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script>

    $(function(){
        $("#naslov").focus(); //Focus on search field
        $("#naslov").autocomplete({
            minLength: 0,
            delay:1,
            source: "/suggest.php",
            focus: function( event, ui ) {
                $('#imdb').val( ui.item.value );
                $(this).val( ui.item.label );
                return false;
            },
            select: function( event, ui ) {
                $('#imdb').val( ui.item.value );
                $(this).val( ui.item.label );
                return false;
            }
        }).data("uiAutocomplete")._renderItem = function( ul, item ) {
            return $("<li></li>")
                .data( "item.autocomplete", item )
                 .append( "<a>" + (item.img?"<img class='imdbImage' src='/imdbImage.php?url=" + item.img + "' />":"") + "<span class='imdbTitle'>" + item.label + "</span>  <span class='imdbYear'>("+ item.year +")</spane>"+ (item.cast?"<br /><span class='imdbCast'>" + item.cast + "</span>":"") + "<div class='clearfix'></div></a>" )
                .appendTo( ul );
        };
    });
</script>
@stop

@section('content_js_footer')
<!-- Autocomplite -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
@stop