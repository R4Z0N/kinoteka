@extends('sajt.templates.sajt')

@section('title')@lang('admin.donacije') - {!! $podesavanja->podnaslov !!}@stop

@section('meta')
<meta name="description" content="{!! str_limit(trans('admin.donacije_opis'), 156) !!}">
<meta property="og:title" content="@lang('admin.donacije') - {!! $podesavanja->podnaslov !!}">
<meta property="og:description" content="{!! str_limit(trans('admin.donacije_opis'), 156) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov">
<div class="container">
<h1>@lang('admin.donacije')</h1>
<p>@lang('admin.donacije_opis')</p>
</div>
</div>
@stop

@section('content')

<div class="row">
<div class="col-md-12" style="margin-bottom:20px;">
<img src="/img/donacije/djordje.png" class="img-thumbnail">
</div>
<div class="col-md-12" style="margin-bottom:20px;">
<img src="/img/donacije/mladen.png" class="img-thumbnail">
</div>
</div>

<form class="pull-right" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
    <input type="hidden" name="cmd" value="_s-xclick">
    <input type="hidden" name="hosted_button_id" value="4LDSDET559YBA">
    <input type="hidden" name="on0" value="Doniraj">
    <select name="os0" class="form-control">
        <option value="Doniraj">Doniraj €1.00 EUR</option>
    </select>
    <input type="hidden" name="currency_code" value="EUR">
    <input type="image" src="/img/doniraj.png" class="img-responsive" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
@stop