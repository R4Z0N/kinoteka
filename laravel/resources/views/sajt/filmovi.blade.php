<div class="col-md-2 col-sm-4 col-xs-6 animacija animated fadeIn">
<div class="film">

<a itemprop="url" href="/watch/{{ $clanak->id }}/{{ $clanak->slug }}" title="{{ $clanak->naslov }}">
<figure>
<img itemprop="image" alt="{{ $clanak->naslov }}" width="165" height="250" class="img-responsive" style="" src="/img/filmovi/{{ $clanak->slika }}" >
@if ($clanak->embed_q  == '1080p' | $clanak->embed_q_2  == '1080p' | $clanak->embed_q_3 == '1080p')
<img class="quality-banner img-responsive" style="width: 70px;height: 70px;border: 0; opacity: 1;" src="/img/quality/1080p.png" alt="1080p">
@elseif ($clanak->embed_q  == '720p' | $clanak->embed_q_2  == '720p' | $clanak->embed_q_3 == '720p')
<img class="quality-banner img-responsive" style="width: 70px;height: 70px;border: 0; opacity: 1;" src="/img/quality/720p.png" alt="720p">
@endif

@if ($clanak->imdbRating)
<span class="rank" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" data-toggle="tooltip" data-placement="top" title="IMDb rank"><i class="fa fa-star"></i> <span itemprop="ratingValue">{{ $clanak->imdbRating }}</span> <span itemprop="reviewCount" style="display: none">{{ $clanak->imdbVotes }}</span></span>
@endif
<figcaption>	

<div class="kategorije">
@foreach($clanak->oznake as $oznaka)
<li>{{ $oznaka->naslov }}</li>
@endforeach
</div>


<span class="play animated bounceIn">
<i class="fa fa-play-circle-o fa-4x"></i></span>
</figcaption>
</figure>

</a>

<div class="opis">
<a href="/watch/{{ $clanak->id }}/{{ $clanak->slug }}" title="{{ $clanak->naslov }}"><h2 itemprop="name">{{ $clanak->naslov }}</h2></a>
<p>
<a href="/godina/{{ $clanak->godina }}" title="{{ $clanak->godina }}">{{ $clanak->godina }}</a>
</p>

</div>
</div>
</div>