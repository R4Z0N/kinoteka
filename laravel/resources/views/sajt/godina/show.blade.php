@extends('sajt.templates.sajt')

@section('title')@lang('admin.godina') - {!! $podesavanja->podnaslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($podesavanja->description, 156))) !!}">
<meta property="og:title" content="@lang('admin.pretraga') - {!! $podesavanja->podnaslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($podesavanja->description, 156))) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov">
<div class="container">
<h1>@lang('admin.godina') {{ $godina }}</h1>
</div>
</div>
@stop

@section('content')
<div id="content">
<div class="row">
@if (count($clanci) === 0)
<div class="col-md-12">
<div class="alert alert-danger">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<p>Ova godina nije pronađena, pokušajte ponovo...</p>
</div>
</div>
@elseif (count($clanci) >= 1)
@foreach($clanci as $clanak)
@if ($clanak->objavljen == 1)
@include('sajt.filmovi')
@endif
@endforeach
@endif
</div>
</div>

@stop
