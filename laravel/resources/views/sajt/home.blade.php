@extends('sajt.templates.sajt')

@section('title'){!! $podesavanja->naslov !!} - {!! $podesavanja->podnaslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($podesavanja->description, 156))) !!}">
<meta property="og:title" content="{!! $podesavanja->naslov !!} - {!! $podesavanja->podnaslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($podesavanja->description, 156))) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">

@stop

@section('minerThrottle') throttle: 0.8 @stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov alert">
<div class="container">
@if (Auth::guest())
<h1>@lang('admin.dobrodosli')</h1>
<p>@lang('admin.dobrodosli_opis')</p>
@else
<h1>Zdravo,</h1>
<p>ukoliko želite da se reklamirate na našoj stranici, primjetite kakvu grešku na sajtu ili zelite da pokrenete sajt kao ovaj <a href="http://www.facebook.com/kinoteka.biz" target="_blank">kontaktirajte nas</a></p>
@endif
</div>
</div>
@stop

@section('content')
<div itemscope itemtype="http://schema.org/Movie" id="content">
<div class="row">
@foreach($clanci as $clanak)
@include('sajt.filmovi')
@endforeach
</div>
</div>

{!! $clanci->links() !!}


@stop