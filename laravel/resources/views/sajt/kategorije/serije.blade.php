@extends('sajt.templates.sajt')

@section('title'){!! $kategorija->naslov !!} - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($kategorija->opis, 156))) !!}">
<meta property="og:title" content="{!! $kategorija->naslov !!} - {!! $podesavanja->naslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($kategorija->opis, 156))) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov" style="background-color: {{ $kategorija->boja }};">
<div class="container">
<h1>{!! $kategorija->naslov !!}</h1>
<p>{!! $kategorija->opis !!}</p>
</div>
</div>
@stop


@section('content')
<div id="content">
<div class="row">
@foreach($clanci as $clanak)
@include('sajt.serije')
@endforeach
</div>
</div>

{!! $clanci->render() !!}
@stop