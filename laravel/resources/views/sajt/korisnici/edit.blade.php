@extends('sajt.templates.sajt')

@section('title') {{ $users->name }} - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! $podesavanja->description !!}">
<meta property="og:title" content="{!! $users->name !!} - {!! $podesavanja->naslov !!}">
<meta property="og:description" content="{!! $podesavanja->description !!}">
<meta property="og:image" content="/img/avatar/{!! $users->avatar !!}">
<meta name="author" content="{!! $users->name !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="korisnik">
<div class="container">
<span class="pull-left">
@if ($users->avatar)
<span data-toggle="tooltip" data-placement="left" title="@lang('admin.izmeni_avatar')"><img class="avatar" data-toggle="modal" data-target="#avatar" width="100" height="100" alt="{{ $users->name }}" src="/img/avatar/{{ $users->avatar }}"></span>
@else
<span data-toggle="tooltip" data-placement="left" title="@lang('admin.izmeni_avatar')"><img class="avatar" data-toggle="modal" data-target="#avatar" width="100" height="100" alt="{{ $users->name }}" src="/img/avatar/avatar.jpg"></span>
@endif
</span>
<span class="ime">
<h1>{{ $users->name }}</h1>
</span>
<p>
<a data-toggle="tooltip" data-placement="bottom" class="email" title="Kontaktirajte me" href="mailto: {{ $users->email }}">{{ $users->email }}</a>
</p>
</div>
</div>
@stop

@section('content')
<div class="korisnik-info">

<div class="col-md-4 col-md-offset-4">
<form action="/korisnik/{{ $users->id }}" method="POST">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="form-group">
<h3>@lang('admin.korisnicko_ime')</h3>
<input type="text" name="name" value="" placeholder="{{ $users->name }}" class="form-control korisnik-input">
</div>

<div class="form-group">
<h3>@lang('admin.email_adresa')</h3>
<input type="email" name="email" value="" placeholder="{{ $users->email }}" class="form-control korisnik-input">
</div>
<div class="form-group">
<h3>@lang('admin.nova_lozinka')</h3>
<input type="password" name="password" value="{{ old('password') }}" class="form-control korisnik-input">
</div>
<div class="form-group">
<button class="btn btn-success pull-right" title="@lang('admin.sacuvaj')" type="submit">@lang('admin.sacuvaj')</button>
</div>
</form>
</div>

</div>
<div class="modal fade" id="avatar" tabindex="-1" role="dialog" aria-labelledby="avatar" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title">@lang('admin.izmeni_avatar')</h4>
</div>
<div class="modal-body">
<form action="/korisnik/{{ $users->id }}" method="POST" files="true" enctype="multipart/form-data">
{{ csrf_field() }}
{{ method_field('PATCH') }}
<div class="form-group">
<input type="file" name="avatar" accept="image/*" class="form-control">
</div>
<div class="form-group text-right">
<button class="btn btn-success" title="@lang('admin.sacuvaj')" type="submit">@lang('admin.sacuvaj')</button>
</div>
</form>
</div>
</div>
</div>
</div>
@stop