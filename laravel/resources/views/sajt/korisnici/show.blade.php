@extends('sajt.templates.sajt')

@section('title') {{ $users->name }} - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! $podesavanja->description !!}">
<meta property="og:title" content="{!! $users->name !!} - {!! $podesavanja->naslov !!}">
<meta property="og:description" content="{!! $podesavanja->description !!}">
<meta property="og:image" content="/img/avatar/{!! $users->avatar !!}">
<meta name="author" content="{!! $users->name !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="korisnik">
<div class="container">
<span class="pull-left">
@if ($users->avatar)
<img class="avatar" width="100" height="100" alt="{{ $users->name }}" src="/img/avatar/{{ $users->avatar }}">
@else
<img class="avatar" width="100" height="100" alt="{{ $users->name }}" src="/img/avatar/avatar.jpg">
@endif
</span>
<span class="ime">
<h1>{{ $users->name }}</h1>
</span>
<p>
<a data-toggle="tooltip" data-placement="bottom" class="email" title="Kontaktirajte me" href="mailto:{{ $users->email }}">{{ $users->email }}</a>
</p>
</div>
</div>
@stop

@section('content')

<div class="korisnik-info">
<div class="row">

<div class="col-md-8">
<h3>@lang('admin.dodati_filmova')</h3><hr>
<div id="content">
<div class="row">
@foreach($clanci as $clanak)
@if ($clanak->objavljen == 1)

<div class="col-md-3 col-xs-6 animacija animated fadeIn">
<div class="film">


<a itemprop="url" href="/watch/{{ $clanak->id }}/{{ $clanak->slug }}" title="{{ $clanak->naslov }}">
<figure>
<img itemprop="image" alt="{{ $clanak->naslov }}" width="165" height="250" class="img-responsive" style="" src="/img/filmovi/{{ $clanak->slika }}" >
@if ($clanak->embed_q  == '1080p' | $clanak->embed_q_2  == '1080p' | $clanak->embed_q_3 == '1080p')
<img class="quality-banner img-responsive" style="width: 70px;height: 70px;border: 0; opacity: 1;" src="/img/quality/1080p.png">
@elseif ($clanak->embed_q  == '760p' | $clanak->embed_q_2  == '760p' | $clanak->embed_q_3 == '760p')
<img class="quality-banner img-responsive" style="width: 70px;height: 70px;border: 0; opacity: 1;" src="/img/quality/720p.png">
@endif

@if ($clanak->imdbRating)
<span class="rank" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" data-toggle="tooltip" data-placement="top" title="IMDb rank"><i class="fa fa-star"></i> <span itemprop="ratingValue">{{ $clanak->imdbRating }}</span> <span itemprop="reviewCount" style="display: none">{{ $clanak->imdbVotes }}</span></span>
@endif
<figcaption>	

<div class="kategorije">
@foreach($clanak->oznake as $oznaka)
<li>{{ $oznaka->naslov }}</li>
@endforeach
</div>


<span class="play animated bounceIn">
<i class="fa fa-play-circle-o fa-4x"></i></span>
</figcaption>
</figure>

</a>


<div class="opis">
<a href="/watch/{{ $clanak->id }}/{{ $clanak->slug }}" title="{{ $clanak->naslov }}"><h2 itemprop="name">{{ $clanak->naslov }}</h2></a>
<p>
<a href="/godina/{{ $clanak->godina }}" title="{{ $clanak->godina }}">{{ $clanak->godina }}</a>
</p>

</div>
</div>
</div>
@endif
@endforeach

</div>
</div>
{!! $clanci->render() !!}
</div>


<div class="col-md-4">
<h3>@lang('admin.na_cekanje')</h3><hr>
<p>
@foreach($clanci as $clanak)
@if ($clanak->objavljen == 1)
@else
{{ $clanak->naslov }} <br>
@endif
@endforeach
</p><br>

<h3>@lang('admin.statistika')</h3><hr>
<p>@lang('admin.dodao_filmova'): {{ $users->dodao_filmova() }}</p>
<p>@lang('admin.omiljeno'): {{ $users->omiljeni_filmovi() }}</p>
</div>

</div>
</div>
@stop
