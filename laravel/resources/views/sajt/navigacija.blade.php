<ul class="nav navbar-nav">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Sortija po: <span class="badge hidden-xs" style="background-color: #32936F;"><i style="font-size:13px;" class="fa fa-arrow-down"></i></span></a>
		<ul class="dropdown-menu">
			<li {!! Request::is('/') ? 'class="active"' : '' !!}><a href="/" data-toggle="tooltip" data-placement="right" title="Najnoviji filmovi">@lang('admin.najnovije')</a></li>
			<li {!! Request::is('popularno') ? 'class="active"' : '' !!}><a href="/popularno" data-toggle="tooltip" data-placement="right" title="Popularni filmovi">@lang('admin.popularno')</a></li>
			<li {!! Request::is('best_rated') ? 'class="active"' : '' !!}><a href="/best_rated" data-toggle="tooltip" data-placement="right" title="Najbolje ocjenjeni filmovi">Najbolje ocenjeno</a></li>
		</ul>
	</li>


	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Filmovi <span class="caret hidden-xs"></span></a>
		<ul class="dropdown-menu">
			@foreach($kategorije as $kategorija)
			@if ($kategorija->slug == 'domaci-filmovi' || $kategorija->slug == 'strani-filmovi')
			<li {!! Request::is($kategorija->slug) ? 'class="active"' : '' !!}>
				<a href="/{{ $kategorija->slug }}" title="{{ $kategorija->naslov }} sa prevodom">{{ $kategorija->naslov }} <span class="badge hidden-xs" style="background-color: {{ $kategorija->boja }};">{{ $kategorija->clanci()->count() }}</span></a>
			</li>
			@endif
			@endforeach
		</ul>
	</li>

<!--
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Serije <span class="caret hidden-xs"></span></a>
<ul class="dropdown-menu">
@foreach($kategorije as $kategorija)
@if ($kategorija->slug == 'domace-serije' || $kategorija->slug == 'strane-serije')
<li {!! Request::is($kategorija->slug) ? 'class="active"' : '' !!}>
<a href="/{{ $kategorija->slug }}" title="{{ $kategorija->naslov }} sa prevodom">{{ $kategorija->naslov }} <span class="badge hidden-xs" style="background-color: {{ $kategorija->boja }};">{{ $kategorija->clanci()->count() }}</span></a>
</li>
@endif
@endforeach
</ul>
</li>
-->

<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Crtani <span class="caret hidden-xs"></span></a>
	<ul class="dropdown-menu">
		@foreach($kategorije as $kategorija)
		@if ($kategorija->slug == 'domaci-crtani' || $kategorija->slug == 'strani-crtani')
		<li {!! Request::is($kategorija->slug) ? 'class="active"' : '' !!}>
			<a href="/{{ $kategorija->slug }}" title="{{ $kategorija->naslov }} sa prevodom">{{ $kategorija->naslov }} <span class="badge hidden-xs" style="background-color: {{ $kategorija->boja }};">{{ $kategorija->clanci()->count() }}</span></a>
		</li>
		@endif
		@endforeach
	</ul>
</li>

<li class="dropdown ">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Žanrovi <span class="caret hidden-xs"></span></a>
	<ul class="dropdown-menu columns">
		@foreach($oznake as $oznaka)
		<li {!! Request::is('zanr/'. $oznaka->slug) ? 'class="active"' : '' !!}>
			<a href="/zanr/{{ $oznaka->slug }}" title="{{ $oznaka->naslov }}">{{ $oznaka->naslov }}</a>
		</li>
		@endforeach
	</ul>
</li>
</ul>

@if (Auth::guest())
<ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
		<a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
<!--			<img width="32" height="32" alt="Avatar" src="/img/avatar/avatar.jpg"> -->
			Dobrodosao Gost, <b>Prijava</b>
		</a>
		<ul class="dropdown-menu">
			<li><a href="/login" title="@lang('admin.prijava')">@lang('admin.prijava')</a></li>
			<li><a href="/register" title="@lang('admin.registracija')">@lang('admin.registracija')</a></li>
		</ul>
	</li>
</ul>
@else
<ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
		<a href="" class="navbar-gravatar dropdown-toggle" data-toggle="dropdown">
			@if (Auth::user()->avatar)
			<img width="32" height="32" alt="{{ auth()->user()->name }}" src="/img/avatar/{{ auth()->user()->avatar }}">
			@else
			<img width="32" height="32" alt="{{ auth()->user()->name }}" src="/img/avatar/avatar.jpg">
			@endif
		</a>
		<ul class="dropdown-menu">
			@if (Auth::user()->admin)
			<li><a href="/admin" title="@lang('admin.administracija')">@lang('admin.administracija')</a></li>
			@endif
			<li><a href="/korisnik/{{ auth()->user()->slug }}" title="@lang('admin.pogledaj_profil')">@lang('admin.pogledaj_profil')</a></li>
			<li><a href="/korisnik/{{ auth()->user()->slug }}/edit" title="@lang('admin.izmeni_profil')">@lang('admin.izmeni_profil')</a></li>
			<li class="divider"></li>
			<li><a href="/logout" title="@lang('admin.odjava')">@lang('admin.odjava')</a></li>
		</ul>
	</li>
</ul>
@endif

<ul class="nav navbar-nav navbar-right">
	@if (!Auth::guest())
	<li><a href="/dodaj" data-toggle="tooltip" data-placement="bottom" title="@lang('admin.dodajte_film')" style="color: #32936f;"><i class="fa fa-plus"></i></a></li>
	<li><a href="/omiljeno" data-toggle="tooltip" data-placement="bottom" title="@lang('admin.omiljeno')"><i style="color: red;" class="fa fa-heart"></i></a></li>
	@endif
</ul>

<form id="form-pretraga"class="navbar-form navbar-right" style="padding: 0;" action="/pretraga" method="POST">
	{{ method_field('POST') }}
	{{ csrf_field() }}
	<div class="form-group has-feedback">
		<input type="hidden" id="imdbID" name="imdbID">
		<input type="text" onkeyup="search(this.value)"  id="pretraga" name="pretraga" size="23" data-toggle="tooltip" data-placement="bottom" title="Pretraga" placeholder="Unesite naziv, godinu, žanr..." class="form-control pretraga" style="padding-right:25px">
		<i class="fa fa-search form-control-feedback fa-rotate-90"></i>
	</div>
</form>


<link rel="stylesheet" href="/css/site/jquery-ui.css">
<!-- JS za sugerisani sadrzaj pretrage -->
<script src="/js/pretraga.js"></script>
<!-- Autocomplite -->
<script src="/js/jquery-ui.js"></script>
<!-- Select2 -->
<script src="/plugins/select2/select2.full.min.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
});
</script>