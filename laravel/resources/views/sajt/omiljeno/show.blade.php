@extends('sajt.templates.sajt')

@section('title')@lang('admin.omiljeno') - {!! $podesavanja->podnaslov !!}@stop

@section('meta')
<meta name="description" content="{!! str_limit(trans('admin.omiljeno_opis'), 156) !!}">
<meta property="og:title" content="@lang('admin.omiljeno') - {!! $podesavanja->podnaslov !!}">
<meta property="og:description" content="{!! str_limit(trans('admin.omiljeno_opis'), 156) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov">
<div class="container">
<h1>@lang('admin.omiljeno')</h1>
<p>@lang('admin.omiljeno_opis')</p>
</div>
</div>
@stop

@section('content')
<div itemscope itemtype="http://schema.org/Movie" id="content">
<div class="row">
@foreach($clanci as $clanak)
@if($clanak->omiljeno()->where('user_id',  '=', Auth::user()->id)->first())
@include('sajt.filmovi')
@endif
@endforeach
</div>
</div>
@stop