@extends('sajt.templates.sajt')

@section('title'){!! $oznaka->naslov !!} - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($oznaka->opis, 156))) !!}">
<meta property="og:title" content="{!! $oznaka->naslov !!} - {!! $podesavanja->naslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($oznaka->opis, 156))) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov" style="background-color: {{ $oznaka->boja }};">
<div class="container">
<h1>{!! $oznaka->naslov !!}</h1>
<p>{!! $oznaka->opis !!}</p>
</div>
</div>
@stop


@section('content')
<div id="content">
<div class="row">
@foreach($clanci as $clanak)
@include('sajt.filmovi')
@endforeach
</div>
</div>
{!! $clanci->render() !!}

@stop