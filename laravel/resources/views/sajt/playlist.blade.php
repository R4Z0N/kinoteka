@extends('sajt.templates.sajt')

@section('title'){{$article->name}}: {{$episode->name}} ({!!$article->year!!}) ONLINE sa prevodom - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($article->description, 312))) !!}">
<meta property="og:title" content="{{$article->name}}: {{$episode->name}} ({!!$article->year!!}) ONLINE sa prevodom - {!! $podesavanja->naslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($article->description, 312))) !!}">
<meta property="og:image" content="{!! url('/img/filmovi/'. $article->image) !!}">
<meta name="author" content="Vaso">

@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')

<div class="naslov" style="background-color: #1B98E0;" itemscope itemtype="http://schema.org/Movie">
  <div class="container">
    <div class="row" style="margin: 0">
        <div id="movie-info" class="col-xs-12 col-md-9 padding-0">
        <div class="col-xs-10 col-md-10 margin-0 padding-0">
          <span class="movie-name"><h1>{{$article->name}}: {{$episode->name}} <small>({{$article->year}})</small></h1> </span>
          
          <ul class="list-inline">

          <li><i class="fa fa-tag"></i> <a href="" data-toggle="tooltip" data-placement="bottom" title="Žanr: {{ $article->genre }}">{{ $article->genre }}</a></li>
          </ul>
        </div>
        <div class="col-xs-2 col-md-2 margin-0 padding-0">
        <h1 style="text-align: center;">
        @if ($article->rating)
<span class="pull-right" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" data-toggle="tooltip" data-placement="top" title="{{ rand(10000, 40000) }} IMDb glasova"><i class="fa fa-star"></i> 
<span itemprop="ratingValue">{{ $article->rating }}</span>
<span itemprop="bestRating" style="display: none;">10</span>
<span itemprop="reviewCount" style="display: none">{{ rand(10000, 40000) }}</span>
</span> @endif
@if (Auth::guest())
@else
@if (Auth::user()->admin)
<a href="/admin/clanci/{{ $article->id }}/edit" data-toggle="tooltip" data-placement="bottom" title="@lang('admin.izmeni')" class="btn btn-default pull-right margin-0"><i class="fa fa-pencil"></i> Izmeni</a>
@endif
@endif
</h1>
</div>
        </div>

        <div id="movie-poster" class="pull-left col-md-3" style="padding-right: 20px;"> 
          <img class="img-responsive" src="http://www.gledaj-online.com/posters/{{ $article->image }}" alt="">
        </div>
        <div id="movie-details">
        
          @if ($article->actor) <div class="glumci"><strong>Glumci:</strong><span itemprop="actor" itemscope itemtype="http://schema.org/Person"><span itemprop="name"> {{ $article->actor }}</span></span></div> @endif
          @if ($article->description)<div class="description"><strong>Opis:</strong> {!! html_entity_decode($article->description) !!}</div>@endif
        </div>

    </div>
  </div>
</div>
@stop <!-- STOP NASLOV -->
@section('content')
@if($article->published == false)
<div class="alert alert-warning">
  <strong>Upozorenje!</strong> Ovaj clanak jos uvjek nije objavljen.
</div>
@endif
<style>
  .embed-name li.active , .embed-name li:hover {
    background: #1B98E0;
}
</style>
<div class="row">
  <div id="embed">

    <div id="embed-stream" class="tab-content col-xs-12 col-md-9 padding-0">
     <div role="tabpanel" class="tab-pane active" id="embed_1">
       <div class="embed-responsive embed-responsive-16by9">
          <iframe id="link-place" src="{{ $article->action === 'trailer' ? $article->trailer : $episode->stream }}" scrolling="no" frameborder="0" width="100%" height="480px" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"></iframe>
       </div>
     </div>
     <div class="clearfix"></div>
    </div>
<div class="list-group playlist col-xs-12 col-md-3 padding-0 margin-0">
<ul class="playlist-menu padding-0 margin-0">


  

    @foreach ($seasons as $season)

      <li>
        <a class="list-group-item list @if ($se == $season->season) active @endif" data-toggle="collapse" data-target="#demo{{ $season->season }}">
          <i class="fa fa-film"></i> <span> Sezona {{ $season->season }} </span>
        </a>
        <ul class="padding-0 collapse @if ($se == $season->season)in @endif" id="demo{{ $season->season }}">

          @foreach ($article->seasons as $seasonEpisodes)

            @if ($seasonEpisodes->season === $season->season)
              <li class="">
                <a class="list-group-item item" href="/serija/{{ $article->slug }}/s{{ sprintf("%'.02d", $season->season) }}/e{{ sprintf("%'.02d", $seasonEpisodes->episode) }}">
                  {{$seasonEpisodes->episode}}. @if ($ep == $seasonEpisodes->episode && $se === $season->season) <i class="fa fa-play-circle" style="color: green"></i>  @endif {{$seasonEpisodes->name}}
                </a>
              </li>
            @endif

          @endforeach

        </ul>
      </li>
    @endforeach
  


</ul>
</div>
 <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>

<script>
$( "a" ).on('click', function() {
  $(this).toggleClass('active');
});
</script>
    <div class="clearfix"></div>
    <div class="embed-info">
      <span class="col-xs-6 col-md-6" style="padding: 10px 0;"><strong>Stream dodao:</strong><a href="">Vaso</a>



     @if (Auth::guest()) @else @if (Auth::user()->admin)<strong>Broj pregleda:</strong>{{ $article->views }} @endif @endif</span>
      <span data-toggle="modal" data-target="#problem" class="pull-right col-xs-6 col-md-2" style="background-color: #1B98E0; text-align: center;padding: 10px; cursor: pointer;">@lang('admin.prijavi_problem')</span>
    </div>
    <div class="clearfix"></div>
  </div>
</div>

<div class="row">
  
<div class="komentari">
<div class="fb-comments" data-href="http://{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}" data-colorscheme="light" data-width="100%" data-numposts="5"></div>

</div>

<div class="modal fade" id="problem" tabindex="-1" role="dialog" aria-labelledby="problem" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title">@lang('admin.prijavi_opis')</h4>
</div>
<div class="modal-body">
<form action="/{{ $article->id }}/prijavi" method="POST">
{{ method_field('POST') }}
{{ csrf_field() }}
<input type="hidden" name="adresa" value="http://{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}">
<div class="form-group">
<input type="email" name="email" value="" placeholder="@lang('admin.email_adresa')" class="form-control">
</div>
<div class="form-group">
<textarea name="problem" class="form-control"></textarea>
</div>
<div class="form-group text-right">
<button class="btn btn-success" title="@lang('admin.prijavi_problem')" type="submit">@lang('admin.posalji')</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default hidden-xs" style="border-radius: 2px; border: 1px solid #1A1B1F; text-align: center;">
  <div class="panel-heading" style="border: 0; border-radius: 0; color: #fff; font-weight: bold; background: #1B98E0;">
  <div class="pull-left">RAZMJENA BANNERA</div> <div class="pull-right">Ako želis besplatnu reklamu na našem sajtu <a href="https://www.facebook.com/kinoteka.biz/?fref=ts">kontaktiraj nas <i class="fa fa-facebook"></i></a>.</div><div class="clearfix"></div></div>
  <div class="panel-body" style="background-color: #101010; padding: 5px;">
    <a class="img-responsive" href="http://zapocni.com" target="_BLANK"><img src="http://csget.me/cs/zapocni.jpg"></a>
  </div>
</div>

@stop <!-- STOP CONTENT -->
