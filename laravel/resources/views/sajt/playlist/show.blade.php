@extends('sajt.templates.sajt')

@section('title'){!! $clanci->naslov !!} ({!!$clanci->godina!!}) ONLINE @if ($clanci->oznake) sa prevodom @endif - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($clanci->opis, 312))) !!}">
<meta property="og:title" content="{!! $clanci->naslov !!} - {!! $podesavanja->naslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($clanci->opis, 312))) !!}">
<meta property="og:image" content="{!! url('/img/filmovi/'. $clanci->slika) !!}">
<meta name="author" content="{!! $users->name !!}">

<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')

@foreach($kategorije as $kategorija)
@if ($clanci->kategorije_id == $kategorija->id)
<div class="naslov" style="background-color: {{ $kategorija->boja }};" itemscope itemtype="http://schema.org/Movie">
  <div class="container">
    <div class="row" style="margin: 0">
        <div id="movie-info" class="col-xs-12 col-md-9 padding-0">
        <div class="col-xs-10 col-md-10 margin-0 padding-0">
          <span class="movie-name"><h1>{{$clanci->naslov}} <small>({{$clanci->godina}})</small></h1> </span>
          
          <ul class="list-inline">
          @foreach($clanci->oznake as $oznaka)
          <li><i class="fa fa-tag"></i> <a href="/zanr/{{ $oznaka->slug }}" data-toggle="tooltip" data-placement="bottom" title="Žanr: {{ $oznaka->naslov }}">{{ $oznaka->naslov }}</a></li>
          @endforeach
          </ul>
        </div>
        <div class="col-xs-2 col-md-2 margin-0 padding-0">
        <h1 style="text-align: center;">
        @if ($clanci->imdbRating)
<span class="pull-right" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" data-toggle="tooltip" data-placement="top" title="{{ $clanci->imdbVotes }} IMDb glasova"><i class="fa fa-star"></i> 
<span itemprop="ratingValue">{{ $clanci->imdbRating }}</span>
<span itemprop="bestRating" style="display: none;">10</span>
<span itemprop="reviewCount" style="display: none">{{ $clanci->imdbVotes }}</span>
</span> @endif
@if (Auth::guest())
@else
<span class="pull-right">
@if(!$clanci->omiljeno()->where('user_id',  '=', Auth::user()->id)->first())
<form action="/omiljeno" method="POST">
{{ method_field('POST') }}
{{ csrf_field() }}
<input type="hidden" name="clanci_id" value="{{ $clanci->id }}">
<input type="hidden" name="omiljen" value="1">
<button data-toggle="tooltip" data-placement="top" title="@lang('admin.dodaj_omiljeno')" type="submit" class="omiljeno"><i class="fa fa-heart-o"></i></button>
</form>
@else
<form action="/omiljeno/{{ $clanci->id }}" method="POST">
{{ method_field('DELETE') }}
{{ csrf_field() }}
<button data-toggle="tooltip" data-placement="top" title="@lang('admin.izbrisi_omiljeno')"  type="submit" class="omiljeno"><i class="fa fa-heart"></i></button>
</form>
@endif
</span>

@endif
@if (Auth::guest())
@else
@if (Auth::user()->admin)
<a href="/admin/clanci/{{ $clanci->id }}/edit" data-toggle="tooltip" data-placement="bottom" title="@lang('admin.izmeni')" class="btn btn-default pull-right margin-0"><i class="fa fa-pencil"></i> Izmeni</a>
@endif
@endif
</h1>
</div>
        </div>

        <div id="movie-poster" class="col-xs-6 col-md-3" style="padding-right: 20px;"> 
          <img class="img-responsive" src="/img/filmovi/{{ $clanci->slika }}" alt="">
        </div>
        <div id="movie-details">
        <!--
          @if ($clanci->embed_q)<div class="quality"><strong style="display: inline-block; margin-right: 5px;">Dostupno u:</strong> <span class="info-button">{!! $clanci->embed_q !!}</span> {!! $clanci->embed_q_2 !!} {!! $clanci->embed_q_3 !!}</div>@endif
        -->
          <div class="subtitles">
          <?php $prevodi = 0 ?>
          @foreach($clanci->subtitles as $subtitle)
          <?php $prevodi++ ?>
          @if ($prevodi==1)<strong style="display: inline-block; margin-right: 5px;">Dostupni prevodi:</strong> @endif
          <a href="/subtitle/{{ $subtitle->naslov }}" data-toggle="tooltip" data-placement="bottom" title="Prevod: {{ $subtitle->naslov }}"><span class="info-button"><i class="fa fa-cc"></i> {{ $subtitle->naslov }}</span></a>
          @endforeach
          </div>
          @if ($clanci->Director) <div class="Director"><strong>Direktor:</strong><span itemprop="director" itemscope itemtype="http://schema.org/Person"><span itemprop="name"> {{ $clanci->Director }}</span></div> @endif
          @if ($clanci->Writer) <div class="Writer"><strong>Pisci:</strong> {{ $clanci->Writer }} </div> @endif
          @if ($clanci->Actors) <div class="glumci"><strong>Glumci:</strong><span itemprop="actor" itemscope itemtype="http://schema.org/Person"><span itemprop="name"> {{ $clanci->Actors }}</span></span></div> @endif
          @if ($clanci->opis)<div class="description"><strong>Opis:</strong> {!! html_entity_decode($clanci->opis) !!}</div>@endif
        </div>
        <!--
        @if ($clanci->trailer_1 || $clanci->trailer_2 || $clanci->trailer_3)
        <div id="movie-trailers" class="col-xs-12 hidden-xs col-md-9 margin-0 padding-0">
          <strong class="row">Kratki pregled:</strong>
          @if ($clanci->trailer_1)
          <div class="col-xs-4 col-md-4 padding-0">
            <div class="embed-responsive embed-responsive-16by9">
              {!! $clanci->trailer_1 !!}
            </div>
          </div>
          @endif
          @if ($clanci->trailer_2)
          <div class="col-xs-4 col-md-4 padding-0">
            <div class="embed-responsive embed-responsive-16by9">
              {{ $clanci->trailer_2 }}
            </div>
          </div>
          @endif
          @if ($clanci->trailer_3)
          <div class="col-xs-4 col-md-4 padding-0">
            <div class="embed-responsive embed-responsive-16by9">
              {{ $clanci->trailer_3 }}
            </div>
          </div>
          @endif
        </div>
        @endif
        -->


    </div>
  </div>
</div>
@stop <!-- STOP NASLOV -->
@section('content')
@if($clanci->objavljen == false)
<div class="alert alert-warning">
  <strong>Upozorenje!</strong> Ovaj clanak jos uvjek nije objavljen.
</div>
@endif
<style>
  .embed-name li.active , .embed-name li:hover {
    background: @foreach($kategorije as $kategorija) @if ($clanci->kategorije_id == $kategorija->id) {{ $kategorija->boja }} @endif @endforeach;
}
</style>
<div class="row">
  <div id="embed">
    <ul class="embed-name">
      <strong class="col-xs-6 col-md-5">STREAM LINKOVI</strong>
      <li class="col-xs-2 col-md-2 padding-0 active"><a class="col-xs-12 padding-0" href="#embed_1" aria-controls="embed_1" role="tab" data-toggle="tab" aria-expanded="true"><span class="hidden-xs">Stream #1</span></a></li>
      @if ($clanci->embed_2)
      <li class="col-xs-2 col-md-2 padding-0"><a class="col-xs-12 padding-0" href="#embed_2" aria-controls="embed_2" role="tab" data-toggle="tab" aria-expanded="false"><span class="hidden-xs">Stream #2</span>{</a></li>
      @endif
      @if ($clanci->embed_3)
      <li class="col-xs-2 col-md-2 padding-0"><a class="col-xs-12 padding-0" href="#embed_3" aria-controls="embed_3" role="tab" data-toggle="tab" aria-expanded="false"><span class="hidden-xs">Stream #3</span></a></li>
      @endif
    </ul>
    <div class="clearfix"></div>
    <div id="embed-stream" class="tab-content">
     <div role="tabpanel" class="tab-pane active" id="embed_1">
       <div class="embed-responsive embed-responsive-16by9">
         {!! $clanci->embed !!}
       </div>
     </div>
     @if ($clanci->embed_2)
     <div role="tabpanel" class="tab-pane" id="embed_2">
       <div class="embed-responsive embed-responsive-16by9">
         {!! $clanci->embed_2 !!}
       </div>
     </div>
     @endif
     @if ($clanci->embed_3)
     <div role="tabpanel" class="tab-pane" id="embed_3">
       <div class="embed-responsive embed-responsive-16by9">
         {!! $clanci->embed_3 !!}
       </div>
     </div>
     @endif
    </div>
    <div class="clearfix"></div>
    <div class="embed-info">
      <span class="col-xs-6 col-md-6" style="padding: 10px 0;"><strong>Film dodao:</strong><a href="/korisnik/{{ $users->slug }}">{{ $users->name }}</a>



     @if (Auth::guest()) @else @if (Auth::user()->admin)<strong>Broj pregleda:</strong>{{ $clanci->pregleda }} @endif @endif</span>
      <span data-toggle="modal" data-target="#problem" class="pull-right col-xs-6 col-md-2" style="background-color: @foreach($kategorije as $kategorija) @if ($clanci->kategorije_id == $kategorija->id) {{ $kategorija->boja }} @endif @endforeach; text-align: center;padding: 10px; cursor: pointer;">@lang('admin.prijavi_problem')</span>
    </div>
    <div class="clearfix"></div>
  </div>
</div>

<div class="row">
  
<div class="komentari">
<div class="fb-comments" data-href="http://{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}" data-colorscheme="light" data-width="100%" data-numposts="5"></div>

</div>

<div class="modal fade" id="problem" tabindex="-1" role="dialog" aria-labelledby="problem" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h4 class="modal-title">@lang('admin.prijavi_opis')</h4>
</div>
<div class="modal-body">
<form action="/{{ $clanci->id }}/prijavi" method="POST">
{{ method_field('POST') }}
{{ csrf_field() }}
<input type="hidden" name="adresa" value="http://{{ $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] }}">
<div class="form-group">
<input type="email" name="email" value="" placeholder="@lang('admin.email_adresa')" class="form-control">
</div>
<div class="form-group">
<textarea name="problem" class="form-control"></textarea>
</div>
<div class="form-group text-right">
<button class="btn btn-success" title="@lang('admin.prijavi_problem')" type="submit">@lang('admin.posalji')</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<div class="panel panel-default hidden-xs" style="border-radius: 2px; border: 1px solid #1A1B1F; text-align: center;">
  <div class="panel-heading" style="border: 0; border-radius: 0; color: #fff; font-weight: bold; background: @foreach($kategorije as $kategorija) @if ($clanci->kategorije_id == $kategorija->id) {{ $kategorija->boja }} @endif @endforeach;">
  <div class="pull-left">RAZMJENA BANNERA</div> <div class="pull-right">Ako želis besplatnu reklamu na našem sajtu <a href="https://www.facebook.com/kinoteka.biz/?fref=ts">kontaktiraj nas <i class="fa fa-facebook"></i></a>.</div><div class="clearfix"></div></div>
  <div class="panel-body" style="background-color: #101010; padding: 5px;">
    <a class="img-responsive" href="http://zapocni.com" target="_BLANK"><img src="http://csget.me/cs/zapocni.jpg"></a>
  </div>
</div>

@stop <!-- STOP CONTENT -->
@endif
@endforeach