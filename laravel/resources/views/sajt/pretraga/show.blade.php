@extends('sajt.templates.sajt')

@section('title')@lang('admin.pretraga') - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta name="description" content="{!! strip_tags(html_entity_decode(str_limit($podesavanja->description, 156))) !!}">
<meta property="og:title" content="@lang('admin.pretraga') - {!! $podesavanja->podnaslov !!}">
<meta property="og:description" content="{!! strip_tags(html_entity_decode(str_limit($podesavanja->description, 156))) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov" @if (count($clanci) === 0) style="background: #e63946;" @endif>
	<div class="container">
		<h1>@lang('admin.pretraga') za "{{ $pretraga }}"</h1>
		@if (count($clanci) === 0) 
		<p>Zao nam je, nemamo ni jedan rezulata sa pojmom "{{ $pretraga }}", stoga pogledajte nasu preporuku samo za Vas.</p> 
		@else
		<p>@lang('admin.pretraga_opis')</p>
		@endif
	</div>
</div>
@stop

@section('content')
<div id="content">
	<div class="row">
		@if (count($clanci) === 0)
			@if (count($preporuka))
		<div class="col-md-12">
			<h3>Lista od 12 filmova preporucenih samo za Vas</h3>
		</div>
				@foreach($preporuka as $clanak)
					@if ($clanak->objavljen == 1)
						@include('sajt.filmovi')
					@endif
				@endforeach
			@endif
		@elseif (count($clanci))
			@foreach($clanci as $clanak)
				@if ($clanak->objavljen == 1)
					@include('sajt.filmovi')
				@endif
			@endforeach
		@endif
	</div>
</div>
@stop
