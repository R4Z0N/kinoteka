<div class="col-md-2 col-sm-4 col-xs-6 animacija animated fadeIn">
<div class="film">

<a itemprop="url" href="/serija/{{ $clanak->slug }}" title="{{ $clanak->naslov }}">
<figure>
<img itemprop="image" alt="{{ $clanak->naslov }}" width="165" height="250" class="img-responsive" style="" src="http://www.gledaj-online.com/posters/{{ $clanak->image }}" >
@if ($clanak->embed_q  == '1080p' | $clanak->embed_q_2  == '1080p' | $clanak->embed_q_3 == '1080p')
<img class="quality-banner img-responsive" style="width: 70px;height: 70px;border: 0; opacity: 1;" src="/img/quality/1080p.png">
@elseif ($clanak->embed_q  == '760p' | $clanak->embed_q_2  == '760p' | $clanak->embed_q_3 == '760p')
<img class="quality-banner img-responsive" style="width: 70px;height: 70px;border: 0; opacity: 1;" src="/img/quality/720p.png">
@endif

@if ($clanak->rating)
<span class="rank" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" data-toggle="tooltip" data-placement="top" title="IMDb rank"><i class="fa fa-star"></i> <span itemprop="ratingValue">{{$clanak->rating}}</span> <span itemprop="reviewCount" style="display: none">{{ rand(10000, 40000) }}</span></span>
@endif
<figcaption>	

<div class="kategorije">
<li>{{ $clanak->genre }}</li>
</div>


<span class="play animated bounceIn">
<i class="fa fa-play-circle-o fa-4x"></i></span>
</figcaption>
</figure>

</a>

<div class="opis">
<a href="/{{ $clanak->id }}/{{ $clanak->slug }}" title="{{ $clanak->naslov }}"><h2 itemprop="name">{{ $clanak->name }}</h2></a>
<p>
<a href="/godina/{{ substr($clanak->naslov, -5, -1) }}" title="{{ $clanak->godina }}">{{ $clanak->year }}</a>
</p>

</div>
</div>
</div>