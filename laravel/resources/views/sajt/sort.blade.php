@extends('sajt.templates.sajt')

@section('title')@lang('admin.popularni') - {!! $podesavanja->podnaslov !!}@stop

@section('meta')
<meta name="description" content="{!! str_limit(trans('admin.popularni_opis'), 156) !!}">
<meta property="og:title" content="@lang('admin.popularni') - {!! $podesavanja->podnaslov !!}">
<meta property="og:description" content="{!! str_limit(trans('admin.popularni_opis'), 156) !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop

@section('naslov')
<div class="naslov">
<div class="container">
<h1>{!! $sort_name !!}</h1>
<p>{!! $sort_description !!}</p>
</div>
</div>
@stop

@section('content')
<div itemscope itemtype="http://schema.org/Movie" id="content">
<div class="row">
@foreach($clanci as $clanak)
@include('sajt.filmovi')
@endforeach
</div>
</div>
{!! $clanci->render() !!}


@stop