@extends('sajt.templates.sajt')

@section('title'){!! $Subtitle->naslov !!} - {!! $podesavanja->naslov !!}@stop

@section('meta')
<meta property="og:title" content="{!! $Subtitle->naslov !!} - {!! $podesavanja->naslov !!}">
<meta property="og:image" content="{!! $podesavanja->slika !!}">
<meta name="author" content="{!! $podesavanja->naslov !!}">
@stop

@section('navigacija')
@include ('sajt.navigacija')
@stop


@section('naslov')
<div class="naslov" style="background-color: {!! $Subtitle->bg_color !!};">
<div class="container">
<h1>Svi filmovi sa prevodom na {!! $Subtitle->naslov !!}</h1>
<p>{!! $Subtitle->opis !!}</p>
</div>
</div>
@stop

@section('content')
<div id="content">
<div class="row">
@foreach($clanci as $clanak)
@include('sajt.filmovi')
@endforeach
</div>
</div>

{!! $clanci->links() !!}
@stop