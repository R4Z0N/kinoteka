$(function () { $('[data-toggle="tooltip"]').tooltip() });

tinymce.init({
    selector: "textarea",
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify",
    menubar:false,
    statusbar: false,
    height: 100
});

$(document).ready(function() {
      if(window.location.href.indexOf("#dodaj") > -1) {
      $("#dodaj").fadeToggle();
   }
      $(".dodaj").click(function() {
      $("#dodaj").fadeToggle();
      });
    $("body").mouseup(function(e) {
        var subject = $("#dodaj");
        if(e.target.id != subject.attr('id') && !subject.has(e.target).length) { subject.fadeOut(); }
    });
});

$(document).ready(function() {
      if(window.location.href.indexOf("#kateogrija") > -1) {
      $("#kateogrija").fadeToggle();
   }
      $(".kateogrija").click(function() {
      $("#kateogrija").fadeToggle();
   });
   $("body").mouseup(function(e) {
   var subject = $("#kateogrija");
   if(e.target.id != subject.attr('id') && !subject.has(e.target).length) { subject.fadeOut(); }
});
});


$(document).ready(function() {
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#idinavrh').fadeIn();
        } else {
            $('#idinavrh').fadeOut();
        }
    });
    $('#idinavrh').click(function() {
        $("html, body").animate({ scrollTop: 0 }, 1000);
        return false;
    });
    $('#ididolje').click(function() {
        $('html, body').animate({scrollTop: $(document).height()}, 2000);
        return false;
    });
});
