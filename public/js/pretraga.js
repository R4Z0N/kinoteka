$(function(){
	$('#pretraga').on('input', function() {
    	$('#imdbID').removeAttr('value');
	});
    $("#pretraga").focus(); //Focus on search field
    $("#pretraga").autocomplete({
        minLength: 0,
        delay:1,
        source: "/suggest.php",
        focus: function( event, ui ) {
        	$('#imdbID').val( ui.item.value );
            $('#pretraga').val( ui.item.label );
            $("#pretraga").css("'border-radius', '0px'");
            return false;
        },
        select: function( event, ui ) {
            $('#imdbID').val( ui.item.value );
            $('#form-pretraga').submit();
            return false;
        }
    }).data("uiAutocomplete")._renderItem = function( ul, item ) {
        return $("<li></li>")
        .data( "item.autocomplete", item )
        .append( "<a>" + (item.img?"<img class='imdbImage' src='/imdbImage.php?url=" + item.img + "' />":"") + "<span class='imdbTitle'>" + item.label + "</span>  <span class='imdbYear'>("+ item.year +")</spane>"+ (item.cast?"<br /><span class='imdbCast'>" + item.cast + "</span>":"") + "<div class='clearfix'></div></a>" )
        .appendTo( ul );
    };
});