<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sinhronizacija</title>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
.tg .tg-4eph{background-color:#f9f9f9}
</style>
</head>
<body>
	

<?php 
include "sync_db.php";

// Now, let's fetch five random articless and output their names to a list.
// We'll add less error handling here as you can do that on your own now
$sql = "SELECT * FROM articles WHERE published = true AND type='series'";
if (!$result = $mysqli->query($sql)) {
    echo "Sorry, the website is experiencing problems.";
    exit;
}
 ?>





<table class="tg">
  <tr>
    <th class="tg-031e">ID</th>
    <th class="tg-031e">IME</th>
    <th class="tg-031e">IMDBTT</th>
    <th class="tg-031e">KATEGORIJA</th>
    <th class="tg-031e">OPCIJE</th>
    <th class="tg-031e">STATUS</th>
  </tr>
  <?php 
// Print our 5 random articless in a list, and link to each articles
while ($articles = $result->fetch_assoc()) {
?>

  <tr>
    <td class="tg-031e"><?php echo $articles['id'] ?></td>
    <td class="tg-031e"><?php echo $articles['name'] ?></td>
    <td class="tg-031e"><?php echo $articles['imdbTT'] ?></td>
    <td class="tg-031e"><?php echo $articles['type'] ?></td>
    <td class="tg-031e"></td>
    <td class="tg-031e"></td>
  </tr>

<?php 	
		
}
 ?>

</table>

</body>
</html>